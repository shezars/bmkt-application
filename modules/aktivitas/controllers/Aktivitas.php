<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktivitas extends CI_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model("M_aktivitas","aktivitas");
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->add_package_path( APPPATH . 'third_party/fpdf');
        // $this->load->library('pdf');
        $this->load->library('pdfhtml');
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$this->load->view('v_core');
		}else{
			$this->load->view('v_login');
		}
	}
	
	function data_baru(){
		if($this->session->userdata('isLogin')){
			$data['lifting_area']	= $this->aktivitas->lifting_area();
			$data['category']		= $this->aktivitas->getCategory();
			$data['material']		= $this->aktivitas->getMaterial();
			$this->load->view('v_data_baru',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function detail_temuan($category_name){
		$id = $this->input->get('id');
		$data['data']	= $this->aktivitas->data_master_temuan_by_id($id);
		$data['category_name'] = $category_name;
		if($category_name=='coin'){
			$data['dataMeasure']	= $this->aktivitas->data_master_pengukuran_coin_by_id($id);
		}else if($category_name=='bottle'){
			$data['dataMeasure']	= $this->aktivitas->data_master_pengukuran_bottle_by_id($id);
		}else{
			$data['dataMeasure']	= $this->aktivitas->data_master_pengukuran_other_by_id($id);
		}
		
		if($this->session->userdata('isLogin')){
			$this->load->view('v_detail_temuan',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function import_data_baru(){
		if($this->session->userdata('isLogin')){
			$data['lifting_area']	= $this->aktivitas->lifting_area();
			$data['category']		= $this->aktivitas->getCategory();
			$data['material']		= $this->aktivitas->getMaterial();
			$this->load->view('v_import_data_baru',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function kodok2(){
		// $this->newNumber();
		// print_r($format_barcode['id']);
		$this->load->view('v_kodok2');
	}
	
	function import_tambah_data_baru(){
		$user 			= $this->session->userdata('user_id');
		$config['upload_path']          = './uploads/excel_import/';
		$config['allowed_types']        = 'xls|xlsx';
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('userfile'))
		{
			echo json_encode(array('errorMsg'=>$this->upload->display_errors()));
		}
		else
		{
			$data_upload 	= $this->upload->data();
			$inputFileName 	= './uploads/excel_import/'.$data_upload['file_name'];
			$data=null;
			
			//Truncate Dulu
			$this->db->truncate('temp_upload');
			
			try {
                $inputFileType	= IOFactory::identify($inputFileName);
                $objReader 		= IOFactory::createReader($inputFileType);
                $objPHPExcel 	= $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 3; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                          
				
				//UPDATE KETIKA KE TEMP TANPA BARCODE AGAR TIDAK KOTOR
                //Sesuaikan sama nama kolom tabel di database                                
                $data = array(
                    "category"		=> $rowData[0][0],
                    "subcategory"	=> $rowData[0][1],
                    "material"		=> $rowData[0][2],
                    "submaterial"	=> $rowData[0][3],
                    "tipe"			=> $rowData[0][4],
                    "dimensi_t"		=> $rowData[0][5],
                    "dimensi_p"		=> $rowData[0][6],
                    "dimensi_l"		=> $rowData[0][7],
                    "dimensi_dm_atas"	=> $rowData[0][8],
                    "dimensi_dm_bawah"	=> $rowData[0][9],
                    "kondisi"		=> $rowData[0][10]==''?'0':'1',
                    "glasir_korosi"	=> $rowData[0][11],
                    "tulisan"		=> $rowData[0][12]==''?'0':'1',
                    "pecah_seribu"	=> $rowData[0][13]==''?'0':'1',
                    "kupingan"	=> $rowData[0][14]==''?'0':'1',
                    "pegangan"	=> $rowData[0][15]==''?'0':'1',
                    "karang"	=> $rowData[0][16]==''?'0':'1',
                    "catatan_tambahan"	=> $rowData[0][17],
                    "user_created"	=> $user,
                    "date_created"	=> date('Y-m-d H:m:s')
                );
                
                //sesuaikan nama dengan nama tabel
				if(!empty($rowData[0][0])){
					$insert = $this->db->insert("temp_upload",$data);
				}
                
                delete_files($data_upload['file_path']);
			
            }
			echo json_encode(array(
				'status' => 1,
				'messages' => 'success'
			));
		}
	}
	
	function get_data_import_temp(){
		$data = $this->aktivitas->get_data_import_temp();
		echo json_encode($data);
	}
	
	function kodok3(){
		echo $this->aktivitas->getRefferensi('19')['other'];
		die;
		
		$lifting_area = '33';
		$category = '10';
		$subcategory = '11';
		$number = '99';
			
		$format_barcode = $this->aktivitas->getBarcodeFormat();
		$format_barcode = explode("-",$format_barcode['value']);
		
		print_r($format_barcode);
		
		echo $zzz;
		
	}
	
	function proses_masukan_data_import(){
		$lifting_area 	= $this->input->post('lifting');
		$lifting_area 	= explode("-",$lifting_area);
		$get_data 		= $this->aktivitas->get_data_import_temp();
		$user 			= $this->session->userdata('user_id');
		
		foreach($get_data as $row){
			$id_temp = $row['id'];
			$format_barcode = $this->aktivitas->getBarcodeFormat();
			$format_barcode = explode("-",$format_barcode['value']);
			
			$number = $this->aktivitas->newNumber()+1;
			
			$temp_key = array_keys($format_barcode,"lifting_area")[0];
			$temp[$temp_key] = $lifting_area[1];
			$temp_key = array_keys($format_barcode,"category")[0];
			$temp[$temp_key] = $this->aktivitas->getRefferensi('',$row['category'],'category')['other'];
			$id_category = $this->aktivitas->getRefferensi('',$row['category'],'category')['id'];
			$temp_key = array_keys($format_barcode,"subcategory")[0];
			$temp[$temp_key] = $this->aktivitas->getRefferensi('',$row['subcategory'],null,$id_category)['other'];
			$temp_key = array_keys($format_barcode,"material")[0];
			$temp[$temp_key] = $this->aktivitas->getRefferensi('',$row['material'],'material')['other'];
			$id_material = $this->aktivitas->getRefferensi('',$row['material'],'material')['id'];
			$temp_key = array_keys($format_barcode,"submaterial")[0];
			$temp[$temp_key] = $this->aktivitas->getRefferensi('',$row['submaterial'],null,$id_material)['other'];
			$temp_key = array_keys($format_barcode,"autonum_4")[0];
			$temp[$temp_key] = sprintf("%06d", $number);
			
			$barcode = null;
			for($x=0;$x<count($temp);$x++){
				$barcode .= $temp[$x];
			}
			
			//Generate Barcode
			$this->_generate_barcode($barcode);
			$this->_generate_qrcode($barcode);
			
			//Insert to main table
			$data = array(
				'barcode'		=> $barcode,
				'lifting_area'	=> $lifting_area[0],
				'category'		=> $this->aktivitas->getRefferensi('',$row['category'],'category')['id']==''?'0':$this->aktivitas->getRefferensi('',$row['category'],'category')['id'],
				'subcategory'	=> $this->aktivitas->getRefferensi('',$row['subcategory'],null,$id_category)['id']==''?'0':$this->aktivitas->getRefferensi('',$row['subcategory'],null,$id_category)['id'],
				'material'		=> $this->aktivitas->getRefferensi('',$row['material'],'material')['id']==''?'0':$this->aktivitas->getRefferensi('',$row['material'],'material')['id'],
				'submaterial'	=> $this->aktivitas->getRefferensi('',$row['submaterial'],null,$id_material)['id']==''?'0':$this->aktivitas->getRefferensi('',$row['submaterial'],null,$id_material)['id'],
				'user_created'	=> $user,
				'date_created'	=> date('Y-m-d H:m:s')
			);
			$id = $this->aktivitas->tambah_data_baru($data);
			
			//Filter Category First
			if($row['category'] == 'coin'){
				
			}else if($row['category'] == 'bottle'){
				
			}else{
				$data_ukur = array(
					'master_item_id'	=> $id,
					'diameter_top'		=> $row['dimensi_dm_atas'],
					'diameter_bottom'	=> $row['dimensi_dm_bawah'],
					'height'			=> $row['dimensi_t'],
					'long'				=> $row['dimensi_p'],
					'width'				=> $row['dimensi_l'],
					'description'		=> $row['catatan_tambahan'],
					'condition'			=> $row['kondisi'],
					'glasir_korosi'		=> $row['glasir_korosi'],
					'tulisan'			=> $row['tulisan'],
					'pecah_seribu'		=> $row['pecah_seribu'],
					'kupingan'			=> $row['kupingan'],
					'pegangan'			=> $row['pegangan'],
					'karang'			=> $row['karang']
				);
				
				$this->aktivitas->tambah_data_baru_ukur_other($data_ukur);
			}
			
			//Update Status di TEMP
			$data2 = array(
				'status'			=> 1,
				'user_modified'		=> $user,
				'date_modified'		=> date('Y-m-d H:m:s')
			);
			$this->aktivitas->update_temp_upload($id_temp,$data2);
			
		}
		echo 1;
	}
	
	function tambah_data_baru(){
		$lifting_area		= explode('-',$this->input->post('lifting_area'));
		$category			= explode('-',$this->input->post('category'));
		$subcategory		= $this->input->post('subcategory');
		$material			= $this->input->post('material');
		$submaterial		= $this->input->post('submaterial');
		$newNumber			= $this->input->post('newNumber');
		
		$user 				= $this->session->userdata('user_id');
		
		$data 	= array(
			'barcode'		=> $newNumber,
			'lifting_area'	=> $lifting_area[0],
			'category'		=> $category[0],
			'subcategory'	=> $subcategory,
			'material'		=> $material,
			'submaterial'	=> $submaterial,
			'user_created'	=> $user,
			'date_created'	=> date('Y-m-d H:m:s')
		);
		
		//Generate Barcode
		$this->_generate_barcode($newNumber);
		$this->_generate_qrcode($newNumber);
		
		//Insert
		$id = $this->aktivitas->tambah_data_baru($data);
		$data = array(
			'master_item_id'	=> $id,
			'user_created'		=> $user,
			'date_created'		=> date('Y-m-d H:m:s')
		);
		
		//Insert to Measure
		if($category[0]=='45'){
			//COIN MEASURE
			$this->db->insert('measure_coin',$data);
		}else if($category[0]=='46'){
			//BOTTLE MEASURE
			$this->db->insert('measure_bottle',$data);
		}else{
			//OTHER MEASURE
			$this->db->insert('measure_other',$data);
		}
		
		redirect(base_url('aktivitas/data_baru'));
	}
	
	function ubah_data_baru(){
		$lifting_area		= explode('-',$this->input->post('lifting_area'));
		$category			= explode('-',$this->input->post('category'));
		$subcategory		= $this->input->post('subcategory');
		$material			= $this->input->post('material');
		$submaterial		= $this->input->post('submaterial');
		$newNumber			= $this->input->post('newNumber');
		
		$user 				= $this->session->userdata('user_id');
		
		$data 	= array(
			'barcode'		=> $newNumber,
			'lifting_area'	=> $lifting_area[0],
			'category'		=> $category[0],
			'subcategory'	=> $subcategory,
			'material'		=> $material,
			'submaterial'	=> $submaterial,
			'user_created'	=> $user,
			'date_created'	=> date('Y-m-d H:m:s')
		);
		
		//Generate Barcode & QRCode
		$this->_generate_barcode($newNumber);
		$this->_generate_qrcode($newNumber);
		
		//Insert
		$this->aktivitas->tambah_data_baru($data);
		
		redirect(base_url('aktivitas/data_baru'));
	}
	
	function hapus_data_baru(){
		
		$id = $this->input->post('id');
		$category = $this->input->post('id');
		
		$status = $this->aktivitas->hapus_data_baru($id);

		//Hapus Measure
		$this->aktivitas->hapus_data_baru_ukur($category,$id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_super_data_baru(){
		
		$data = $this->input->post('data');
		foreach($data as $index => $row){
			$this->aktivitas->hapus_data_baru($row['id']);
			
			//Hapus Measure
			$this->aktivitas->hapus_data_baru_ukur($row['category'],$row['id']);
		}
		
		echo json_encode(array(
			'status' => 1,
			'qty'	=> count($data)
		));
	}
	
	function newNumber(){
		echo $this->aktivitas->newNumber()+1;
	}
	
	function getDigit(){
		echo json_encode($this->aktivitas->getDigit());
	}
	
	function getBarcodeFormat(){
		echo json_encode($this->aktivitas->getBarcodeFormat());
	}
	
	function getSubCategory($id){
		echo json_encode($this->aktivitas->getSubCategory($id));
	}
	
	function getSubMaterial($id){
		echo json_encode($this->aktivitas->getSubMaterial($id));
	}
	
	function getRefferensi($id){
		echo json_encode($this->aktivitas->getRefferensi($id));
	}
	
	function laporan_temuan_baru_add(){		
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$this->load->library('upload', $config);
		
		$name			= $this->input->post('name');
		$alamat			= $this->input->post('alamat');
		$no_hp			= $this->input->post('no_hp');
		$email			= $this->input->post('email');
		$lokasi			= $this->input->post('lokasi');
		$captcha		= $this->input->post('g-recaptcha-response');
		
		$secretKey = "6Lc6dE4UAAAAABsdv0IYDqKIeO2OSnUtJw3BlIaO";
        $ip = $_SERVER['REMOTE_ADDR'];
		
		$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip));
		
		if($response->success == '' || $response->success == null){
			echo json_encode(array(
				'status' => 0,
				'messages' => 'Captcha Failed'
			));	
			die;
		}
		
		
		if ( ! $this->upload->do_upload('userfile'))
		{
			echo json_encode(array('errorMsg'=>$this->upload->display_errors()));
		}
		else
		{
			$data_upload = $this->upload->data();
			$data = array(
					'name'			=> $name,
					'address'		=> $alamat,
					'no_telp'		=> $no_hp,
					'email'			=> $email,
					'location'		=> $lokasi,
					'attachment'	=> $data_upload['file_name'],
					'upload_date'	=> date('Y-m-d H:m:s')
				);
			$result = $this->aktivitas->laporan_temuan_baru_add($data);	
			echo json_encode(array(
				'status' => 1,
				'messages' => $result
			));
		}
				
	}
	
	function master_temuan($param=null){
		$data['refferensi_status_cleaning_aksi']	= $this->aktivitas->getRefferensiByTag('refferensi_aksi_cleaning',1);
		$data['refferensi_status_moving_aksi']		= $this->aktivitas->getRefferensiByTag('refferensi_aksi_moving',1);
		$data['data_warehouse'] 					= $this->aktivitas->getRefferensiByTag('storage_warehouse',1);
		$data['data_users'] 						= $this->aktivitas->get_users(3);
		if($this->session->userdata('isLogin')){
			$this->load->view('v_temuan',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function data_master_temuan($param1=null,$param2=null){
		$where=null;
		
		if($param1 != null){
			$where = "c.group_id=".$param1;
		}
		
		if($param1!=null && $param2 != null){
			$where = "$param1='$param2'";
		}
		
		//Filtering
		$filterParam	= null;
		if($this->input->post('filterRules')){
			if($this->input->post('filterRules') != '[]'){
				$filter			= json_decode($this->input->post('filterRules'));
				$filter_count	= count($filter);
				if($filter_count>1){
					for($x=0;$x<$filter_count;$x++){
						if($x == $filter_count-1){
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%"';
						}else{
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%" AND ';
						}
						$filterParam .= $temp;
					}
				}else{
					$filterParam = $filter[0]->field.' LIKE "%'.$filter[0]->value.'%"';
				}
			}
		}
		
		$page 			= $this->input->post('page') ? intval($this->input->post('page')) : 1;
		$rows 			= $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
		$dataRows 		= $this->aktivitas->data_master_temuan($page,$rows,$filterParam,$where);
		$dataRowsAll 	= $this->aktivitas->data_master_temuan_all($where);
		$data["total"] 	= ''.count($dataRowsAll).'';
		$data["rows_total"] 	= $dataRowsAll;
		$data["rows"] 	= $dataRows;
		
		echo json_encode($data);
	}
	
	function getRackFromWarehouse($id){
		echo json_encode($this->aktivitas->getRackFromWarehouse($id));
	}
	
	function getBoxFromWarehouse($id){
		echo json_encode($this->aktivitas->getBoxFromWarehouse($id));
	}
	
	function export_master_temuan(){
		$data['data'] 	= $this->aktivitas->data_master_temuan_all();
		$this->load->view('excel/master_temuan',$data);
		$date = date('Y-m-d');	
		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=master-temuan-".$date.".xls");  //File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
	}
	
	function item_cleaning($id){
		$user		= $this->session->userdata('user_id');
		$qty		= $this->input->post('qty');
		$action		= $this->input->post('action');
		$status		= $this->input->post('status');
		$notes		= $this->input->post('notes');
		$data = array(
			'id_master_item'	=> $id,
			'qty_artefact'		=> $qty,
			'action'			=> $action,
			'notes'				=> $notes,
			'pic_user'			=> $user,
			'date_created'		=> date('Y-m-d H:m:s')
		);
		
		
		if($status!=0){
			//Check first
			$checkData = $this->db->get_where('cleaning',array('id_master_item'=>$id))->result_array();
			if(empty($checkData)){
				//Insert cleaning table
				$this->db->insert('cleaning',$data);
			}else{
				//Update cleaning table
				$this->db->where('id',$checkData[0]['id']);
				$this->db->update('cleaning',$data);
			}
		}else{
			$this->db->where('id_master_item',$id);
			$this->db->delete('cleaning');
		}
		
		//Update status to master_item
		$data2 = array(
			'status_cleaning'	=> $status
		);
		$this->db->where('id',$id);
		$status = $this->db->update('master_item',$data2);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function item_moving($id){
		$user		= $this->session->userdata('user_id');
		$qty		= $this->input->post('qty');
		$action		= $this->input->post('action');
		$status		= $this->input->post('status');
		$notes		= $this->input->post('notes');
		$data = array(
			'id_master_item'	=> $id,
			'qty_artefact'		=> $qty,
			'action'			=> $action,
			'notes'				=> $notes,
			'pic_user'			=> $user,
			'date_created'		=> date('Y-m-d H:m:s')
		);
		
		
		if($status!=0){
			//Check first
			$checkData = $this->db->get_where('moving',array('id_master_item'=>$id))->result_array();
			if(empty($checkData)){
				//Insert cleaning table
				$this->db->insert('moving',$data);
			}else{
				//Update cleaning table
				$this->db->where('id',$checkData[0]['id']);
				$this->db->update('moving',$data);
			}
		}else{
			$this->db->where('id_master_item',$id);
			$this->db->delete('moving');
		}
		
		//Update status to master_item
		$data2 = array(
			'status_moving'	=> $status
		);
		$this->db->where('id',$id);
		$status = $this->db->update('master_item',$data2);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function item_storage($id){
		$user		= $this->session->userdata('user_id');
		
		$box_warehouse		= $this->input->post('box_warehouse');
		$box_rack			= $this->input->post('box_rack');
		$box				= $this->input->post('box');
		
		$notes		= $this->input->post('notes');
		
		// $data = array(
			// 'id_master_item'	=> $id,
			// 'qty_artefact'		=> $qty,
			// 'action'			=> $action,
			// 'notes'				=> $notes,
			// 'pic_user'			=> $user,
			// 'date_created'		=> date('Y-m-d H:m:s')
		// );
		
		
		// if($status!=0){
			// //Check first
			// $checkData = $this->db->get_where('moving',array('id_master_item'=>$id))->result_array();
			// if(empty($checkData)){
				// //Insert cleaning table
				// $this->db->insert('moving',$data);
			// }else{
				// //Update cleaning table
				// $this->db->where('id',$checkData[0]['id']);
				// $this->db->update('moving',$data);
			// }
		// }else{
			// $this->db->where('id_master_item',$id);
			// $this->db->delete('moving');
		// }
		
		//Update status to master_item
		$data2 = array(
			'box_id'				=> $box,
			'status_final_storage'	=> 1
		);
		$this->db->where('id',$id);
		$status = $this->db->update('master_item',$data2);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function item_measure($category_name=null){
		
		$id = $this->input->get('id');
		$data['data']	= $this->aktivitas->data_master_temuan_by_id($id);
		
		if($this->session->userdata('isLogin')){
			if($category_name=='coin'){
				$data['symbol_coin'] 	= $this->aktivitas->getRefferensiByTag('measure_coin_symbol_reff',1);
				$data['mint_mark'] 		= $this->aktivitas->getRefferensiByTag('measure_mint_mark_reff',1);
				$data['symbol_reverse']	= $this->aktivitas->getRefferensiByTag('measure_symbol_reverse_reff',1);
				$data['denomination']	= $this->aktivitas->getRefferensiByTag('measure_denomination_reff',1);
				$data['form_rim']		= $this->aktivitas->getRefferensiByTag('measure_form_rim_reff',1);
				$data['form_edge']		= $this->aktivitas->getRefferensiByTag('measure_form_edge_reff',1);
				$data['text_aroundtoprim'] = $this->aktivitas->getRefferensiByTag('measure_text_aroundtoprim',1);
				$data['kings_name'] 	= $this->aktivitas->getRefferensiByTag('measure_kings_name_reff',1);
				$data['after_kings_name'] = $this->aktivitas->getRefferensiByTag('measure_after_kings_name_reff',1);
				$data['assayers_name_between'] = $this->aktivitas->getRefferensiByTag('measure_assayers_name_between_reff',1);
				$data['left_of_year'] 	= $this->aktivitas->getRefferensiByTag('measure_left_of_year_reff',1);
				$data['right_of_year'] 	= $this->aktivitas->getRefferensiByTag('measure_right_of_year_reff',1);
				$data['right_toleftbelow'] 	= $this->aktivitas->getRefferensiByTag('measure_right_toleftbelow_reff',1);
				// print_r($data['symbol_coin']);die;
				$this->load->view('v_measure_coin',$data);
			}else if($category_name=='bottle'){
				$data['bottle_shape'] 	= $this->aktivitas->getRefferensiByTag('measure_bottle_shape_reff',1);
				$data['bottle_lips'] 	= $this->aktivitas->getRefferensiByTag('measure_bottle_lips_reff',1);
				$data['pontil_scars'] 	= $this->aktivitas->getRefferensiByTag('measure_pontil_scars_reff',1);
				
				$data['symbol_coin'] 	= $this->aktivitas->getRefferensiByTag('measure_coin_symbol_reff',1);
				$data['mint_mark'] 		= $this->aktivitas->getRefferensiByTag('measure_mint_mark_reff',1);
				$data['symbol_reverse']	= $this->aktivitas->getRefferensiByTag('measure_symbol_reverse_reff',1);
				$data['denomination']	= $this->aktivitas->getRefferensiByTag('measure_denomination_reff',1);
				$data['form_rim']		= $this->aktivitas->getRefferensiByTag('measure_form_rim_reff',1);
				$data['form_edge']		= $this->aktivitas->getRefferensiByTag('measure_form_edge_reff',1);
				$data['text_aroundtoprim'] = $this->aktivitas->getRefferensiByTag('measure_text_aroundtoprim',1);
				$data['kings_name'] 	= $this->aktivitas->getRefferensiByTag('measure_kings_name_reff',1);
				$data['after_kings_name'] = $this->aktivitas->getRefferensiByTag('measure_after_kings_name_reff',1);
				$data['assayers_name_between'] = $this->aktivitas->getRefferensiByTag('measure_assayers_name_between_reff',1);
				$data['left_of_year'] 	= $this->aktivitas->getRefferensiByTag('measure_left_of_year_reff',1);
				$data['right_of_year'] 	= $this->aktivitas->getRefferensiByTag('measure_right_of_year_reff',1);
				$data['right_toleftbelow'] 	= $this->aktivitas->getRefferensiByTag('measure_right_toleftbelow_reff',1);
				
				$this->load->view('v_measure_bottle',$data);
			}else{
				$this->load->view('v_measure_other',$data);
			}
		}else{
			redirect(base_url());
		}
	}
	
	function tambah_item_measure(){
		
		$id_master_item	= $this->input->post('id');
		$user			= $this->session->userdata('user_id');
		
		$diameter_top	= $this->input->post('diameter_top');
		$diameter_bottom= $this->input->post('diameter_bottom');
		$height			= $this->input->post('height');
		$description	= $this->input->post('description');
		
		$data = array(
			'diameter_top'		=> $diameter_top,
			'diameter_bottom'	=> $diameter_bottom,
			'height'			=> $height,
			'description'		=> $description,
			'user_modified'		=> $user,
			'date_modified'		=> date('Y-m-d H:m:s')
		);
		
		//UPDATE MEASURE DETAIL
		$this->db->where('master_item_id',$id_master_item);
		$this->db->update('measure_other',$data);
		
		$uploads_dir 	= './uploads/';
		$year			= date('Y');
		$month			= date('m');
		$day			= date('d');
		$uploads_dir	= $uploads_dir.'/'.$year.'/'.$month.'/'.$day;
		
		
		if (!file_exists($uploads_dir)) {
			mkdir($uploads_dir, 0777, true);
		}
		
		$data;
		if(array_count_values($_FILES["files"]["error"])[0]>1){
			for($x=0;$x<array_count_values($_FILES["files"]["error"])[0];$x++){
				$y=$x+1;
				$name = basename($_FILES["files"]["name"][$x]);
				$temp['image_'.$y]	= $name;
				$data['images']		= $temp;
				
				move_uploaded_file($_FILES['files']['tmp_name'][$x], "$uploads_dir/$name");
			}
		}else{
			$name = basename($_FILES["files"]["name"][0]);
			$temp['image_1']	= $name;
			$data['images'] 	= $temp;
		}
		
		//UPDATE FOLDER IMAGE LOCATION TO MEASURE OTHER
		$data_folder = array(
			'folder_location'	=> substr($uploads_dir,1),
			'status_measure'	=> '1'
		);
		$this->db->where('id',$id_master_item);
		$this->db->update('master_item',$data_folder);
		
		//UPDATE IMAGE TO MEASURE OTHER
		$this->db->where('master_item_id',$id_master_item);
		$this->db->update('measure_other',$data['images']);
		
		redirect(base_url('aktivitas/master_temuan'));
	}
	
	function tambah_item_measure_coin(){
		
		$id_master_item			= $this->input->post('id');
		$user					= $this->session->userdata('user_id');
		
		$diameter				= $this->input->post('diameter');
		$year					= $this->input->post('year');
		$thickness				= $this->input->post('thickness');
		$mint_mark				= $this->input->post('mint_mark');
		$weight					= $this->input->post('weight');
		$denomination			= $this->input->post('denomination');
		$coin_type				= $this->input->post('coin_type');
		$symbol_coin			= $this->input->post('symbol_coin');
		
		//Coin Type 1
		$text_around_top_rim	= $this->input->post('text_around_top_rim');
		$text_1_kings_name		= $this->input->post('text_1_kings_name');
		$text_2_after_kings_name = $this->input->post('text_2_after_kings_name');
		$assayers_name_in_between = $this->input->post('assayers_name_in_between');
		//Coin Type 2
		$text_1_left_of_year	= $this->input->post('text_1_left_of_year');
		$text_2_right_of_year	= $this->input->post('text_2_right_of_year');
		$right_above_to_left_below = $this->input->post('right_above_to_left_below');
		
		$symbol_reverse			= $this->input->post('symbol_reverse');
		$assayers_initial		= $this->input->post('assayers_initial');
		$form_rim				= $this->input->post('form_rim');
		$form_edge				= $this->input->post('form_edge');
		$description			= $this->input->post('description');
		
		
		$data = array(
			'diameter'			=> $diameter,
			'year'				=> $year,
			'thickness'			=> $thickness,
			'weight'			=> $weight,
			'mint_mark'			=> $mint_mark,
			'denomination'		=> $denomination,
			'coin_type'			=> $coin_type,
			'symbol_coin'		=> $symbol_coin,
			'text_around_top_rim' => $text_around_top_rim,
			'king_name'	=> $text_1_kings_name,
			'after_name_of_king' => $text_2_after_kings_name,
			'assayer_name_between' => $assayers_name_in_between,
			'left_of_year' => $text_1_left_of_year,
			'right_of_year' => $text_2_right_of_year,
			'right_above_to_left_below'	=> $right_above_to_left_below,
			'symbol_reverse'	=> $symbol_reverse,
			'assayer_initial'	=> $assayers_initial,
			'form_of_rim'			=> $form_rim,
			'form_of_edge'			=> $form_edge,
			'description'		=> $description,
			'user_modified'		=> $user,
			'date_modified'		=> date('Y-m-d H:m:s')
		);
		
		//UPDATE MEASURE DETAIL
		$this->db->where('master_item_id',$id_master_item);
		$this->db->update('measure_coin',$data);
		
		$uploads_dir 	= './uploads/';
		$year			= date('Y');
		$month			= date('m');
		$day			= date('d');
		$uploads_dir	= $uploads_dir.'/'.$year.'/'.$month.'/'.$day;
		
		if (!file_exists($uploads_dir)) {
			mkdir($uploads_dir, 0777, true);
		}
		
		$data;
		if(array_count_values($_FILES["files"]["error"])[0]>1){
			for($x=0;$x<array_count_values($_FILES["files"]["error"])[0];$x++){
				$y=$x+1;
				$name = basename($_FILES["files"]["name"][$x]);
				$temp['image_'.$y]	= $name;
				$data['images']		= $temp;
				
				move_uploaded_file($_FILES['files']['tmp_name'][$x], "$uploads_dir/$name");
			}
		}else{
			$name = basename($_FILES["files"]["name"][0]);
			$temp['image_1']	= $name;
			$data['images'] 	= $temp;
		}
		
		//UPDATE FOLDER IMAGE LOCATION TO MEASURE OTHER
		$data_folder = array(
			'folder_location'	=> substr($uploads_dir,1),
			'status_measure'	=> '1'
		);
		$this->db->where('id',$id_master_item);
		$this->db->update('master_item',$data_folder);
		
		//UPDATE IMAGE TO MEASURE OTHER
		$this->db->where('master_item_id',$id_master_item);
		$this->db->update('measure_coin',$data['images']);
		
		redirect(base_url('aktivitas/master_temuan'));
	}
	
	function tambah_item_measure_bottle(){
		
		$id_master_item			= $this->input->post('id');
		$user					= $this->session->userdata('user_id');
		
		$bottle_shape			= $this->input->post('bottle_shape');
		$height					= $this->input->post('height');
		$bottle_lips			= $this->input->post('bottle_lips');
		$lip_diameter			= $this->input->post('lip_diameter');
		$mouth_diameter			= $this->input->post('mouth_diameter');
		$lip_thickness			= $this->input->post('lip_thickness');
		$lip_height				= $this->input->post('lip_height');
		$neck_smallest_diameter	= $this->input->post('neck_smallest_diameter');
		$measured_artifact		= $this->input->post('measured_artifact');
		$neck_height			= $this->input->post('neck_height');
		$body_height			= $this->input->post('body_height');
		$artefact_shape			= $this->input->post('artefact_shape');
		$pontil_scars			= $this->input->post('pontil_scars');
		$body_diameter			= $this->input->post('body_diameter');
		$base_diameter			= $this->input->post('base_diameter');
		$base_inside_diameter	= $this->input->post('base_inside_diameter');
		$four_largest_sides_a	= $this->input->post('four_largest_sides_a');
		$four_largest_sides_b	= $this->input->post('four_largest_sides_b');
		$four_largest_sides_c	= $this->input->post('four_largest_sides_c');
		$four_largest_sides_d	= $this->input->post('four_largest_sides_d');
		$four_smallest_sides_a	= $this->input->post('four_smallest_sides_a');
		$four_smallest_sides_b	= $this->input->post('four_smallest_sides_b');
		$four_smallest_sides_c	= $this->input->post('four_smallest_sides_c');
		$four_smallest_sides_d	= $this->input->post('four_smallest_sides_d');
		$pontil_scar_diameter	= $this->input->post('pontil_scar_diameter');
		$pontil_scar_depth		= $this->input->post('pontil_scar_depth');
		$description			= $this->input->post('description');
		
		
		$data = array(
			'bottle_shape'				=> $bottle_shape,
			'height'					=> $height,
			'bottle_lips'				=> $bottle_lips,
			'lip_diameter'				=> $lip_diameter,
			'mouth_diameter'			=> $mouth_diameter,
			'lip_thickness'				=> $lip_thickness,
			'lip_height'				=> $lip_height,
			'neck_smallest_diameter'	=> $neck_smallest_diameter,
			'is_neck_body_measured' 	=> $measured_artifact,
			'neck_height'				=> $neck_height,
			'body_height' 				=> $body_height,
			'artefact_shape' 			=> $artefact_shape,
			'pontil_scars_type'			=> $pontil_scars,
			'body_diameter' 			=> $body_diameter,
			'base_diameter'				=> $base_diameter,
			'base_inside_diameter'		=> $base_inside_diameter,
			'four_largest_sides_1'		=> $four_largest_sides_a,
			'four_largest_sides_2'		=> $four_largest_sides_b,
			'four_largest_sides_3'		=> $four_largest_sides_c,
			'four_largest_sides_4'		=> $four_largest_sides_d,
			'four_smallest_sides_1'		=> $four_smallest_sides_a,
			'four_smallest_sides_2'		=> $four_smallest_sides_b,
			'four_smallest_sides_3'		=> $four_smallest_sides_c,
			'four_smallest_sides_4'		=> $four_smallest_sides_d,
			'pontil_scar_diameter'		=> $pontil_scar_diameter,
			'pontil_scar_depth'			=> $pontil_scar_depth,
			'description'				=> $description,
			'user_modified'				=> $user,
			'date_modified'				=> date('Y-m-d H:m:s')
		);
		
		//UPDATE MEASURE DETAIL
		$this->db->where('master_item_id',$id_master_item);
		$this->db->update('measure_bottle',$data);
		
		
		$uploads_dir 	= './uploads/';
		$year			= date('Y');
		$month			= date('m');
		$day			= date('d');
		$uploads_dir	= $uploads_dir.'/'.$year.'/'.$month.'/'.$day;
		
		if (!file_exists($uploads_dir)) {
			mkdir($uploads_dir, 0777, true);
		}
		
		$data;
		if(array_count_values($_FILES["files"]["error"])[0]>1){
			for($x=0;$x<array_count_values($_FILES["files"]["error"])[0];$x++){
				$y=$x+1;
				$name = basename($_FILES["files"]["name"][$x]);
				$temp['image_'.$y]	= $name;
				$data['images']		= $temp;
				
				move_uploaded_file($_FILES['files']['tmp_name'][$x], "$uploads_dir/$name");
			}
		}else{
			$name = basename($_FILES["files"]["name"][0]);
			$temp['image_1']	= $name;
			$data['images'] 	= $temp;
		}
		
		//UPDATE FOLDER IMAGE LOCATION TO MEASURE OTHER
		$data_folder = array(
			'folder_location'	=> substr($uploads_dir,1),
			'status_measure'	=> '1'
		);
		$this->db->where('id',$id_master_item);
		$this->db->update('master_item',$data_folder);
		
		//UPDATE IMAGE TO MEASURE OTHER
		$this->db->where('master_item_id',$id_master_item);
		$this->db->update('measure_bottle',$data['images']);
		
		redirect(base_url('aktivitas/master_temuan'));
	}
	
	/*
	PEMINDAHAN
	*/
	function pemindahan(){
		if($this->session->userdata('isLogin')){
			$this->load->view('v_pemindahan');
		}else{
			redirect(base_url());
		}
	}
	
	function tambah_pemindahan_baru(){
		$user				= $this->session->userdata('user_id');
		$tujuan				= $this->input->post('tujuan');
		$deskripsi			= $this->input->post('deskripsi');
		$pic_nama_lengkap	= $this->input->post('pic_nama_lengkap');
		$pic_handphone		= $this->input->post('pic_handphone');
		$tgl_kembali		= $this->input->post('tgl_kembali');
		$tgl_kembali		= explode("/",$tgl_kembali);
		$tgl_kembali		= $tgl_kembali[2].'-'.$tgl_kembali[0].'-'.$tgl_kembali[1];
		
		$data = array(
			'tujuan'			=> $tujuan,
			'description'		=> $deskripsi,
			'pic_name'			=> $pic_nama_lengkap,
			'pic_phone'			=> $pic_handphone,
			'date_created'		=> date('Y-m-d H:m:s'),
			'date_return'		=> $tgl_kembali,
			'user_created'		=> $user
		);
		
		$status = $this->aktivitas->tambah_pemindahan_baru($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function detail_pemindahan($id){
		$data['getDetailPemindahan']	= $this->aktivitas->getDetailPemindahan($id);
		$data['id_pemindahan']			= $id;
		if($this->session->userdata('isLogin')){
			$this->load->view('v_detail_pemindahan',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function add_item_pemindahan(){
		$id_item		= $this->input->post('id');
		$id_pemindahan	= $this->input->post('id_pemindahan');
		$data = array(
			'id_pemindahan'		=> $id_pemindahan,
			'id_master_item'	=> $id_item
		);
		//Insert to pemindahan detail
		$this->db->insert('pemindahan_detail',$data);
		
		//Update status to master item
		$data2 = array(
			'status'	=> '1'
		);
		$this->db->where('id',$id_item);
		$this->db->update('master_item',$data2);
	}
	
	function data_master_pemindahan(){
		$data = $this->aktivitas->data_master_pemindahan();
		echo json_encode($data);
	}
	
	function data_master_detail_pemindahan($id){
		$data = $this->aktivitas->data_master_detail_pemindahan($id);
		echo json_encode($data);
	}
	
	function export_master_pemindahan(){
		$data['data'] = $this->aktivitas->data_master_pemindahan();
		$this->load->view('excel/master_pemindahan',$data);
		$date = date('Y-m-d');	
		header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=master-pemindahan-".$date.".xls");  //File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
	}
	
	function lokasi_temuan_baru(){
		if($this->session->userdata('isLogin')){
			$this->load->view('v_lokasi_temuan_baru');
		}else{
			redirect(base_url());
		}
	}
	
	function data_lokasi_temuan_baru(){
		$data = $this->aktivitas->data_lokasi_temuan_baru();
		echo json_encode($data);
	}
	
	function tambah_lokasi_temuan_baru(){
		$nama		= $this->input->post('nama');
		$wilayah	= $this->input->post('wilayah');
		$long		= $this->input->post('long');
		$lat		= $this->input->post('lat');
		$kode		= $this->input->post('kode');
		
		$data 		= array(
			'nama'			=> $nama,
			'wilayah'		=> $wilayah,
			'long'			=> $long,
			'lat'			=> $lat,
			'kode_wilayah'	=> $kode,
			'user_created'	=> $this->session->userdata('user_id'),
			'date_created'	=> date('Y-m-d H:m:s')
		);
		
		$status = $this->aktivitas->tambah_lokasi_temuan_baru($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
		
	}
	
	function ubah_lokasi_temuan_baru($id){
		$nama		= $this->input->post('nama');
		$wilayah	= $this->input->post('wilayah');
		$long		= $this->input->post('long');
		$lat		= $this->input->post('lat');
		$kode		= $this->input->post('kode');
		
		$data 		= array(
			'nama'			=> $nama,
			'wilayah'		=> $wilayah,
			'long'			=> $long,
			'lat'			=> $lat,
			'kode_wilayah'	=> $kode,
			'user_modified'	=> $this->session->userdata('user_id'),
			'date_modified'	=> date('Y-m-d H:m:s')
		);
		
		$status = $this->aktivitas->ubah_lokasi_temuan_baru($id,$data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_lokasi_temuan_baru(){
		
		$id = $this->input->post('id');
		
		$status = $this->aktivitas->hapus_lokasi_temuan_baru($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	/*
	####################################################################################################	
	*/
	
	function master_laporan_temuan(){
		$this->load->view('v_laporan_temuan');
	}
	
	function data_master_laporan_temuan(){
		$data = $this->aktivitas->data_master_laporan_temuan();
		echo json_encode($data);
	}
	
	function ubah_status_laporan_temuan($id){
		$status = $this->input->post('status_laporan');
		$user	= $this->session->userdata('user_id');
		$date	= date('Y-m-d H:m:s');
		
		$data = array(
			'status'		=> $status,
			'user_checking'	=> $user,
			'date_checking'	=> $date
		);
		
		$status = $this->aktivitas->ubah_status_laporan_temuan($id,$data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_status_laporan_temuan(){
		
		$id = $this->input->post('id');
		
		$status = $this->aktivitas->hapus_status_laporan_temuan($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function kodok(){
		$data = $this->input->post('data');
		// print_r($data[0]['barcode']); 
	}
	
	function export_bulk(){
		$data 	= $this->input->post('data');
		$count 	= count($data); 
		$jenis 	= $this->input->post('jenis');
		
		echo $this->parsingPDF($jenis,$count,$data);
		// print_r($jenis);
	}
	
	function parsingPDF($jenis=null,$count=null,$data=null){
		$username 	= $this->session->userdata('username');
		
		$this->pdf = new PDFHTML();
        $this->pdf->Add_Page('L','A4',0);
        // $this->pdf->SetAutoPageBreak(true, 15);
        $this->pdf->AliasNbPages();
		$this->pdf->SetFont('Arial','',12);
		
		if($jenis==1){
			//barcode
			$this->template_pdf_barcode($count,$data);
		}else{
			//qrcode
			$this->template_pdf_qrcode($count,$data);
		}
		
        // $this->pdf->Output( 'page.pdf' , 'I' );
		$namafile 	= $username.'-'.date('Ymd').'-'.date('Hms').'.pdf';
		$filename	= FCPATH .'export_bulk/'.$namafile;
        $this->pdf->Output( $filename , 'F' );
		
		return $namafile;
	}
	
	// Simple table
	function template_pdf_barcode($count,$data)
	{
		
		$border=0;
		$heightCell=25;
		$widthCode=-410;
		$start=0;
		$max_column=2; //index dari 0 ~ jadinya 2
		$max_item_page=21;
		
		$html = '<table>';
		
		if($count==1){
			$html .= '<tr>
						<td>'.$this->pdf->Cell(95,$heightCell,$this->pdf->Image(base_url()."barcode/".$data[0]['barcode'].".png",$this->pdf->GetX(), $this->pdf->GetY(),$widthCode),$border,0,'C').'</td>
					  </tr>';
		}else{
			foreach($data as $key=>$row){
				
				if($key == $max_item_page){
					$this->pdf->Add_Page('L','A4',0);
					$max_item_page += 5;
				}
				
				if($key == $start){
					$html .= '<tr>';
				}
				
				if($key == $max_column){
					$html .= '<td>'.$this->pdf->Cell(95,$heightCell,$this->pdf->Image(base_url()."barcode/".$data[$key]['barcode'].".png",$this->pdf->GetX(), $this->pdf->GetY(),$widthCode),$border,1,'C').'</td>';
					$max_column += 3;
				}else{
					$html .= '<td>'.$this->pdf->Cell(95,$heightCell,$this->pdf->Image(base_url()."barcode/".$data[$key]['barcode'].".png",$this->pdf->GetX(), $this->pdf->GetY(),$widthCode),$border,0,'C').'</td>';
				}
				
				if($key == $max_column){
					$html .= '</tr>';
				}
			}
		}
				
		$html .= '</table>';
		$this->pdf->WriteHTML($html);
	}
	
	function kodokk(){
		echo round(7/6);
	}
	
	function template_pdf_qrcode($count,$data)
	{
		$border=0;
		$heightCell=37;
		$widthCode=-200;
		$start=0;
		$max_column=7; //index dari 0 ~ jadinya 6
		$max_item_page=32;
		
		$html = '<table>';
		
		if($count==1){
			$html .= '<tr>
						<td>'.$this->pdf->Cell(5,$heightCell,$this->pdf->Image(base_url()."qrcode/".$data[0]['barcode'].".png",$this->pdf->GetX(), $this->pdf->GetY(),$widthCode).$this->pdf->SetFont('','',9).$this->pdf->Write(68,$data[0]['barcode']),$border,0,'C').'</td>
					  </tr>';
		}else{
			foreach($data as $key=>$row){
				
				if($key == $max_item_page){
					$this->pdf->Add_Page('L','A4',0);
					$max_item_page += 32;
				}
				
				if($key == $start){
					$html .= '<tr>';
				}
				
				if($key == $max_column){
					$html .= '<td>'.$this->pdf->Cell(5,$heightCell,$this->pdf->Image(base_url()."qrcode/".$data[$key]['barcode'].".png",$this->pdf->GetX(), $this->pdf->GetY(),$widthCode).$this->pdf->SetFont('','',9).$this->pdf->Write(68,$data[$key]['barcode']),$border,1,'C').'</td>';
					$max_column += 8;
				}else{
					$html .= '<td>'.$this->pdf->Cell(5,$heightCell,$this->pdf->Image(base_url()."qrcode/".$data[$key]['barcode'].".png",$this->pdf->GetX(), $this->pdf->GetY(),$widthCode).$this->pdf->SetFont('','',9).$this->pdf->Write(68,$data[$key]['barcode']),$border,0,'C').'</td>';
				}
				
				if($key == $max_column){
					$html .= '</tr>';
				}
			}
		}
				
		$html .= '</table>';
		$this->pdf->WriteHTML($html);
	}
	
	function downloadBarcode($barcode){
		$url = FCPATH .'barcode/'.$barcode.'.png';
		force_download($url, NULL);
	}
	
	function downloadQrcode($barcode){
		$url = FCPATH .'qrcode/'.$barcode.'.png';
		force_download($url, NULL);
	}
	
	function downloadExportBulk($namafile){
		$url = FCPATH .'export_bulk/'.$namafile;
		force_download($url, NULL);
	}
	
	private function _generate_barcode($sparepart_code, $scale=6, $fontsize=18, $thickness=30,$dpi=72) {
		// CREATE BARCODE GENERATOR
		// Including all required classes
		require_once( APPPATH . 'libraries/barcodegen/BCGFontFile.php');
		require_once( APPPATH . 'libraries/barcodegen/BCGColor.php');
		require_once( APPPATH . 'libraries/barcodegen/BCGDrawing.php');

		// Including the barcode technology
		// Ini bisa diganti-ganti mau yang 39, ato 128, dll, liat di folder barcodegen
		require_once( APPPATH . 'libraries/barcodegen/BCGcode39.barcode.php');

		// Loading Font
		// kalo mau ganti font, jangan lupa tambahin dulu ke folder font, baru loadnya di sini
		$font = new BCGFontFile(APPPATH . 'libraries/font/Arial.ttf', $fontsize);
		
		// Text apa yang mau dijadiin barcode, biasanya kode produk
		$text = $sparepart_code;

		// The arguments are R, G, B for color.
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255);

		$drawException = null;
		try {
			$code = new BCGcode39(); // kalo pake yg code39, klo yg lain mesti disesuaikan
			$code->setScale($scale); // Resolution
			$code->setThickness($thickness); // Thickness
			$code->setForegroundColor($color_black); // Color of bars
			$code->setBackgroundColor($color_white); // Color of spaces
			$code->setFont($font); // Font (or 0)
			$code->parse($text); // Text
		} catch(Exception $exception) {
			$drawException = $exception;
		}

		/* Here is the list of the arguments
		1 - Filename (empty : display on screen)
		2 - Background color */
		$drawing = new BCGDrawing('', $color_white);
		if($drawException) {
			$drawing->drawException($drawException);
		} else {
			$drawing->setDPI($dpi);
			$drawing->setBarcode($code);
			$drawing->draw();
		}
		// ini cuma labeling dari sisi aplikasi saya, penamaan file menjadi png barcode.
		$filename_img_barcode = $sparepart_code .'.png';
		// folder untuk menyimpan barcode
		$drawing->setFilename( FCPATH .'barcode/'. $filename_img_barcode);
		// proses penyimpanan barcode hasil generate
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

		return $filename_img_barcode;
	}
	
	function _generate_qrcode($text){
		$params['data'] = $text;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'qrcode/'.$text.'.png';
		$this->ciqrcode->generate($params);
	}
	
	function get_lokasi_penemuan(){
		$id = $this->input->post('id');
		echo json_encode($this->aktivitas->get_lokasi_penemuan($id));
	}
	
	function lokasi_geografis($id){
		$geografis = $this->input->post('geografis');
		$data = array(
			'lokasi_geografis'	=> $geografis
		);
		$status = $this->aktivitas->ubah_lokasi_temuan_baru($id,$data);
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function lokasi_kedalaman($id){
		$kedalaman = $this->input->post('kedalaman');
		$data = array(
			'lokasi_kedalaman'	=> $kedalaman
		);
		$status = $this->aktivitas->ubah_lokasi_temuan_baru($id,$data);
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function lokasi_penemuan($id){
		$penemuan = $this->input->post('penemuan');
		$data = array(
			'penemuan'	=> $penemuan
		);
		$status = $this->aktivitas->ubah_lokasi_temuan_baru($id,$data);
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function lokasi_periode($id){
		$periode = $this->input->post('periode');
		$data = array(
			'dinasti_periode'	=> $periode
		);
		$status = $this->aktivitas->ubah_lokasi_temuan_baru($id,$data);
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
}
