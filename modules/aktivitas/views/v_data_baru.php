<?php
	$this->load->view('core/v_header');
?>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Aktivitas Data Baru" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						<div class="easyui-panel" title="Informasi Data Baru" style="width:100%;padding:10px;">
							<form id="ff" action="<?=base_url();?>aktivitas/tambah_data_baru" method="post" enctype="multipart/form-data">
								<table>
									<tr>
										<td>Lifting Area:</td>
										<td>
										<select name="lifting_area" class="easyui-combobox"  id="idLiftingArea" required="true" data-options="onSelect:function(rec){generateBarcode(rec,'lifting_area');}">
											<?php
											foreach($lifting_area as $row){
												echo '<option value="'.$row['id'].'-'.$row['kode_wilayah'].'">'.$row['nama'].'</option>';
											}
											?>
										</select>	
										</td>
									</tr>
									<tr>
										<td>Category:</td>
										<td>
										<select name="category" class="easyui-combobox"  id="idCategory" required="true" data-options="onSelect:function(rec){changeCategory(rec);generateBarcode(rec,'category');}">
											<?php
											foreach($category as $row){
												echo '<option value="'.$row['id'].'-'.$row['other'].'">'.$row['value'].'</option>';
											}
											?>
										</select>
										</td>
									</tr>
									<tr>
										<td>SubCategory:</td>
										<td>
											<input name="subcategory" id="cc2" class="easyui-combobox" data-options="onSelect:function(rec){generateBarcode(rec,'subcategory');}">
										</td>
									</tr>
									<tr>
										<td>Material:</td>
										<td>
											<select name="material" class="easyui-combobox"  id="idMaterial" required="true" data-options="onSelect:function(rec){changeMaterial(rec);generateBarcode(rec,'material');}">
											<?php
											foreach($material as $row){
												echo '<option value="'.$row['id'].'-'.$row['other'].'">'.$row['value'].'</option>';
											}
											?>
										</select>
										</td>
									</tr>
									<tr>
										<td>SubMaterial:</td>
										<td>
											<input name="submaterial" id="cc3" class="easyui-combobox" data-options="onSelect:function(rec){generateBarcode(rec,'submaterial');}">
										</td>
									</tr>
									<!--
									<tr>
										<td>File:</td>
										<td><input name="file" class="f1 easyui-filebox"></input></td>
									</tr>
									-->
									<tr>
										<td>Barcode:</td>
										<td><input name="barcode" id="idBarcode" class="f1 easyui-textbox"></input></td>
										<input type="hidden" name="newNumber" id="idNewNumber">
									</tr>
									<tr>
										<td></td>
										<td style="padding-top:10px;text-align:right;"><input type="submit" value="Submit"></input></td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
	<script>
		
		var temp2 	= new Array();
		var xyz 	= null;
		var digit 	= null;
		var other_temp = null;
		
		newNumber();
		function newNumber(){
			$.get("<?=base_url();?>aktivitas/newNumber",function(result){
				xyz = result;
			})
		}
		
		getDigit();
		function getDigit(){
			$.get("<?=base_url();?>aktivitas/getDigit",function(result){
				var obj = jQuery.parseJSON(result);
				digit = obj['value'];
			})
		}
		
		function generateBarcode(sel,param){
			
			var formatJoin;
		
			var getFormat = $.get("<?=base_url();?>aktivitas/getBarcodeFormat",function(result){
				var obj = jQuery.parseJSON(result);
				var split = obj['value'].split("-");
				var originalValue = $("#idBarcode").val();
				
				if(param=='subcategory' || param=='submaterial'){
					temp = sel.other;
				}else{
					temp = sel.value.split('-');
					temp = temp[1];
				}
				
				for(x=0;x<split.length;x++){
					if(split[x] == param){
						
						var index = split.indexOf(param);
						if (index !== -1) {
							temp2[x] = temp;
						}else{
							temp2.push(temp);
						}
					}
				}
				
				formatJoin = temp2.join('')+lpad(xyz,parseInt(digit));
				$("#idNewNumber").val(formatJoin);
				
				$("#idBarcode").textbox({
					disabled	:true,
					value		: formatJoin
				});
				
			})
		}
		
		function lpad(value, padding) {
			var zeroes = new Array(padding+1).join("0");
			return (zeroes + value).slice(-padding);
		}
		
		function changeCategory(rec){
			var value = rec.value.split("-");
			var temp='';
			var url = '<?=base_url();?>aktivitas/getSubCategory/'+value[0];
			
			$('#cc2').combobox({
				url:url,
				valueField:'id',
				textField:'value'
			});
		}
		
		function changeMaterial(rec){
			var value = rec.value.split("-");
			var temp='';
			var url = '<?=base_url();?>aktivitas/getSubMaterial/'+value[0];
			
			$('#cc3').combobox({
				url:url,
				valueField:'other',
				textField:'value'
			});
		}
	</script>
<?php
	$this->load->view('core/v_footer');
?>	
</body>
</html>