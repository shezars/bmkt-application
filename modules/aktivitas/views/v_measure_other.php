<?php
	$this->load->view('core/v_header');
?>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Detail Measure Other" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						<div class="easyui-panel" title="Measure Other" style="width:100%;padding:10px;">
							<form id="ff" action="<?=base_url();?>aktivitas/tambah_item_measure" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
								<table width="100%">
									<tr>
										<td width="50%">
											<table width="100%">
												<tr>
													<td width="20%">Barcode</td>
													<td width="1%">:</td>
													<td><?=$data['barcode'];?></td>
												</tr>
												<tr>
													<td width="20%">Lifting Area</td>
													<td width="1%">:</td>
													<td><?=$data['lifting_area_name'];?></td>
												</tr>
												<tr>
													<td>Category</td>
													<td>:</td>
													<td><?=$data['category_name'];?></td>
												</tr>
												<tr>
													<td>Sub Category</td>
													<td>:</td>
													<td><?=$data['sub_category_name'];?></td>
												</tr>
												<tr>
													<td>Material</td>
													<td>:</td>
													<td><?=$data['material_name'];?></td>
												</tr>
												<tr>
													<td>Sub Material</td>
													<td>:</td>
													<td><?=$data['sub_material_name'];?></td>
												</tr>
												<tr>
													<td colspan="3" align="right">
															<input type="file" name="files">
														</form>
													</td>
												</tr>
												<tr>
													<td>Diameter Top</td>
													<td>:</td>
													<td><input name="diameter_top" class="easyui-textbox"> CM</td>
												</tr>
												<tr>
													<td>Diameter Bottom</td>
													<td>:</td>
													<td><input name="diameter_bottom" class="easyui-textbox"> CM</td>
												</tr>
												<tr>
													<td>Height</td>
													<td>:</td>
													<td><input name="height" class="easyui-textbox"> CM</td>
												</tr>
												<tr>
													<td>Description</td>
													<td>:</td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td colspan="3">
														<textarea name="description" id="idDescription"></textarea>
													</td>
												</tr>
												<tr>
													<td colspan="3" align="right"><input type="submit" value="Save"></td>
												</tr>
											</table>
										</td>
										<td width="50%" align="right">
											
										</td>
									</tr>
								
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
	<script>
		
		// $("#idDescription").tinymce();
		tinymce.init({
				selector:"textarea"
			})	
			
		$(document).ready(function() {
				
			// enable fileuploader plugin
			$('input[name="files"]').fileuploader({
				extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				changeInput: ' ',
				theme: 'thumbnails',
				limit: 5,
				enableApi: true,
				addMore: true,
				thumbnails: {
					box: '<div class="fileuploader-items">' +
							  '<ul class="fileuploader-items-list">' +
								  '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
							  '</ul>' +
						  '</div>',
					item: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
								   '<div class="progress-holder">${progressBar}</div>' +
							   '</div>' +
						   '</li>',
					item2: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
							   '</div>' +
						   '</li>',
					startImageRenderer: true,
					canvasImage: false,
					_selectors: {
						list: '.fileuploader-items-list',
						item: '.fileuploader-item',
						start: '.fileuploader-action-start',
						retry: '.fileuploader-action-retry',
						remove: '.fileuploader-action-remove'
					},
					onItemShow: function(item, listEl) {
						var plusInput = listEl.find('.fileuploader-thumbnails-input');
						
						plusInput.insertAfter(item.html);
						
						if(item.format == 'image') {
							item.html.find('.fileuploader-item-icon').hide();
						}
					}
				},
				afterRender: function(listEl, parentEl, newInputEl, inputEl) {
					var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));
				
					plusInput.on('click', function() {
						api.open();
					});
				},
			});
			
		});
	</script>
<?php
	$this->load->view('core/v_footer');
?>	
</body>
</html>