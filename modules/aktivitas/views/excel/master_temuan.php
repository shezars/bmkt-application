<html>
<body>
<p><b>MASTER TEMUAN</b></p>
<table border=1>
	<tr>
		<td><b>No</b></td>
		<td><b>Barcode</b></td>
		<td><b>Lifting Area</b></td>
		<td><b>Category</b></td>
		<td><b>Sub Category</b></td>
		<td><b>Material</b></td>
		<td><b>Sub Material</b></td>
		<td><b>Cleaning</b></td>
		<td><b>Moving</b></td>
		<td><b>Measure</b></td>
		<td><b>Final Storage</b></td>
	</tr>
	<?php
	$num=1;
	foreach($data as $row){
		?>
		<tr>
			<td><?=$num;?></td>
			<td><?=$row['barcode'];?></td>
			<td><?=$row['lifting_area_name'];?></td>
			<td><?=$row['category_name'];?></td>
			<td><?=$row['sub_category_name'];?></td>
			<td><?=$row['material_name'];?></td>
			<td><?=$row['sub_material_name'];?></td>
			<td>
				<?php
					if($row['status_cleaning'] == 0){
						echo 'Belum';
					}else{
						echo 'Sudah';
					}
				?>
			</td>
			<td>
				<?php
					if($row['status_moving'] == 0){
						echo 'Belum';
					}else{
						echo 'Sudah';
					}
				?>
			</td>
			<td>
				<?php
					if($row['status_measure'] == 0){
						echo 'Belum';
					}else{
						echo 'Sudah';
					}
				?>
			</td>
			<td>
				<?php
					if($row['status_final_storage'] == 0){
						echo 'Belum';
					}else{
						echo 'Sudah';
					}
				?>
			</td>
		</tr>
		<?php
	$num++;
	}
	?>
</table>
</body>
</html>