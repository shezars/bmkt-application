<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			
			<div title="Master Temuan" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
			
				<div class="row">
					<div class="span100persen">
						Filter PIC: <select id="idPIC">
							<option selected="true" disabled="disabled">-- Pilih PIC --</option> 
							<?php
							foreach($data_users as $row){
								?>
								<option value="<?=$row['id'];?>"><?=$row['complete_name'];?></option>
								<?php
							}
							?>
						</select>
						<a href="<?=base_url();?>aktivitas/master_temuan" class="btn btn-primary btn-xs">Show All</a>
					</div>
				</div>
				
				<div class="row">
					<div class="span100persen">
							<table id="dg" title="MASTER TEMUAN" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'barcode',width:80">Barcode</th>
										<th data-options="field:'nama_lengkap',width:80">PIC</th>
										<th data-options="field:'lifting_area_namee',width:80">Lifting Area</th>
										<th data-options="field:'category_name',width:100">Category</th>
										<th data-options="field:'sub_category_name',width:100">SubCategory</th>
										<th data-options="field:'material_name',width:100">Material</th>
										<th data-options="field:'sub_material_name',width:100">SubMaterial</th>
										<th data-options="field:'status_cleaning',width:100">Cleaning</th>
										<th data-options="field:'status_moving',width:100">Moving</th>
										<th data-options="field:'status_measure',width:100">Measure</th>
										<th data-options="field:'status_final_storage',width:100">Final Storage</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search2" plain="true" onclick="detailTemuan()">Detail</a>
								<a href="<?=base_url();?>aktivitas/data_baru" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true">Tambah</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUserr()">Ubah</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-delete" plain="true" onclick="destroySuperUser()">Super Delete</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-broom" plain="true" onclick="item_cleaning()">Cleaning</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-trucks" plain="true" onclick="item_moving()">Moving</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-tools" plain="true" onclick="item_measure()">Measure</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-warehouse3" plain="true" onclick="finalStorage()">Final Storage</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-barcode" plain="true" onclick="generateBarcode()">Barcode</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-qr-code" plain="true" onclick="generateQrcode()">QR-Code</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-excel2" plain="true" onclick="exportExcel()">Export Excel</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-pdf" plain="true" onclick="exportBulk()">Export PDF</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-picture" plain="true" onclick="importBulkImage()">Import Image</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Status Cleaning Information</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>QTY Artefact:</label>
										<input name="qty" class="easyui-numberbox" data-options="min:1" required="true">
									</div>
									<div class="fitem">
										<label>Aksi:</label>
										<select name="action" class="easyui-combobox" required="true">
											<?php
											foreach($refferensi_status_cleaning_aksi as $row){
												echo "<option value='".$row['id']."'>".$row['value']."</option>";
											}
											?>
										</select>
									</div>
									<div class="fitem">
										<label>Ubah Status:</label>
										<select name="status" class="easyui-combobox" required="true">
											<option value='1'>Sudah</option>
											<option value='0'>Belum</option>
										</select>
									</div>
									<div class="fitem">
										<label>Notes:</label>
										<input name="notes" multiline="true" class="easyui-textbox">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Status Moving Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>QTY Artefact:</label>
										<input name="qty" class="easyui-numberbox" data-options="min:1" required="true">
									</div>
									<div class="fitem">
										<label>Aksi:</label>
										<select name="action" class="easyui-combobox" required="true">
											<?php
											foreach($refferensi_status_moving_aksi as $row){
												echo "<option value='".$row['id']."'>".$row['value']."</option>";
											}
											?>
										</select>
									</div>
									<div class="fitem">
										<label>Ubah Status:</label>
										<select name="status" class="easyui-combobox" required="true">
											<option value='1'>Sudah</option>
											<option value='0'>Belum</option>
										</select>
									</div>
									<div class="fitem">
										<label>Notes:</label>
										<input name="notes" multiline="true" class="easyui-textbox">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg22" class="easyui-dialog" style="width:400px;height:350px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">User Information</div>
								<form id="fm22" method="post" novalidate>
									<table>
										<tr>
											<td>Lifting Area:</td>
											<td>
											<select name="lifting_area" class="easyui-combobox"  id="idLiftingArea" required="true" data-options="onSelect:function(rec){generateBarcode(rec,'lifting_area');}">
												<?php
												foreach($lifting_area as $row){
													echo '<option value="'.$row['id'].'-'.$row['other'].'">'.$row['value'].'</option>';
												}
												?>
											</select>	
											</td>
										</tr>
										<tr>
											<td>Category:</td>
											<td>
											<select name="category" class="easyui-combobox"  id="idCategory" required="true" data-options="onSelect:function(rec){changeCategory(rec);generateBarcode(rec,'category');}">
												<?php
												foreach($category as $row){
													echo '<option value="'.$row['id'].'-'.$row['other'].'">'.$row['value'].'</option>';
												}
												?>
											</select>
											</td>
										</tr>
										<tr>
											<td>SubCategory:</td>
											<td>
												<input name="subcategory" id="cc2" class="easyui-combobox" data-options="onSelect:function(rec){generateBarcode(rec,'subcategory');}">
											</td>
										</tr>
										<tr>
											<td>Material:</td>
											<td>
												<select name="material" class="easyui-combobox"  id="idMaterial" required="true" data-options="onSelect:function(rec){changeMaterial(rec);}">
												<?php
												foreach($material as $row){
													echo '<option value="'.$row['other'].'">'.$row['value'].'</option>';
												}
												?>
											</select>
											</td>
										</tr>
										<tr>
											<td>SubMaterial:</td>
											<td>
												<input name="submaterial" id="cc3" class="easyui-combobox">
											</td>
										</tr>
										<tr>
											<td>Barcode:</td>
											<td><input name="barcode" id="idBarcode" class="f1 easyui-textbox"></input></td>
											<input type="hidden" name="newNumber" id="idNewNumber">
										</tr>
										<tr>
											<td></td>
											<td style="padding-top:10px;text-align:right;"><input type="submit" value="Submit"></input></td>
										</tr>
									</table>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm22','dlg22')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg22').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg23" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons23">
								<div class="ftitle">Move Storage Information</div>
								<form id="fm23" method="post" novalidate>
									<div class="fitem">
											<label>Choose Warehouse:</label>
											<select class="easyui-combobox" name="box_warehouse" id="id_coin_type2" style="width:200px;" data-options="onSelect:function(rec){changeWarehouse(rec);}">
												<option value="-">-</option>
												<?php
												foreach($data_warehouse as $row){
													?>
													<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
													<?php
												}
												?>
											</select>
										</div>
										<div class="fitem">
											<label>Choose Rack:</label>
											<input name="box_rack" id="id_coin_type22" class="easyui-combobox" data-options="onSelect:function(rec){changeRack(rec);}">
											
											<!--
											<input name="subcategory" id="cc2" class="easyui-combobox" data-options="onSelect:function(rec){generateBarcode(rec,'subcategory');}">
											-->
										</div>
										<div class="fitem">
											<label>Choose Box:</label>
											<input id="id_coin_type23" class="easyui-combobox" name="box">
										</div>
								</form>
							</div>
							<div id="dlg-buttons23">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm23','dlg23')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg23').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg24" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Export Bulk Code</div>
								<form id="fm24" method="post" novalidate>
									
									<label><span id="txtQtyItemExport"></span></label>
									
									<div class="fitem">
										<label>Jenis Code:</label>
										<select name="jenis" id="jenisCode" class="easyui-combobox" required="true">
											<option value='1'>Barcode</option>
											<option value='2'>QR-Code</option>
										</select>
									</div>
									
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="exportBulk_Act()" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg24').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg25" class="easyui-dialog" style="width:650px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Import Image</div>
								<form id="fm25" method="post" novalidate>
									
									<label><span id="txtQtyItemImage"></span></label><br/>
									<!-- <label>Method Type : </label>
									<select name="methodtype" id="methodtype" class="easyui-combobox" required="true" style="width:100px;">
										<option value='1'>All</option>
										<option value='2'>Single</option>
									</select><br/>
									-->
									<label>Image Number : </label>
									<select name="imagenumber" id="imagenumber" class="easyui-combobox" required="true" style="width:50px;">
										<option value='1'>1</option>
										<option value='2'>2</option>
										<option value='3'>3</option>
										<option value='4'>4</option>
										<option value='5'>5</option>
									</select>
									<div class="fitem">
										<!-- <input type="file" name="files"> -->
										<!-- <div id="uploader"></div> -->
										<!-- <div id="fine-uploader-manual-trigger"></div> -->
										
										<div id="fine-uploader-gallery"></div>
									</div>
									
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="exportBulk_Act()" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg26').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg26" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Super Delete</div>
								<form id="fm26" method="post" novalidate>
									
									<label><span id="txtQtySuperDelete"></span></label>
									
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="superDelete_Act()" style="width:190px">Lanjutkan Hapus</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg26').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>

	<script type="text/javascript">
	
		function changeWarehouse(rec){
			
			var value = rec.value;
			var url = '<?=base_url();?>aktivitas/getRackFromWarehouse/'+value;
			
			$('#id_coin_type22').combobox({
				url:url,
				valueField:'id',
				textField:'value'
			});
		}
		
		function changeRack(rec){
			var value = rec.id;
			var url = '<?=base_url();?>aktivitas/getBoxFromWarehouse/'+value;
			$('#id_coin_type23').combobox({
				url:url,
				valueField:'id',
				textField:'value'
			});
		}
		
        $(function(){
			var checkPICFilter = "<?=$this->uri->segment(3);?>";
			
			var url = '<?=base_url();?>aktivitas/data_master_temuan';
			
			if(checkPICFilter != null){
				url = '<?=base_url();?>aktivitas/data_master_temuan/'+checkPICFilter;
			}
			 <!-- <th data-options="field:'ck',checkbox:true"></th> -->
            var dg = $('#dg').datagrid({
                url: url,
				checkOnSelect:false,
				selectOnCheck:false,
                pagination: true,
				singleSelect:true,
                remoteFilter: true,
				idField:'barcode',
                rownumbers: true,
				columns:[[
					{field:'ck',checkbox:'true'},
					{field:'barcode',width:80,title:'Barcode'},
					{field:'nama_lengkap',width:80,title:'PIC'},
					{field:'lifting_area_namee',width:80,title:'Lifting Area'},
					{field:'category_name',width:100,title:'Category'},
					{field:'sub_category_name',width:100,title:'SubCategory'},
					{field:'material_name',width:100,title:'Material'},
					{field:'sub_material_name',width:100,title:'SubMaterial'},
					{field:'status_cleaning',width:100,title:'Cleaning',
						formatter: function(value,row,index){
							if(value==0){
								return 'Belum';
							}else{
								return 'Sudah';
							}
						}
					},
					{field:'status_moving',width:100,title:'Moving',
						formatter: function(value,row,index){
							if(value==0){
								return 'Belum';
							}else{
								return 'Sudah';
							}
						}
					},
					{field:'status_measure',width:100,title:'Measure',
						formatter: function(value,row,index){
							if(value==0){
								return 'Belum';
							}else{
								return 'Sudah';
							}
						}
					},
					{field:'status_final_storage',width:100,title:'Final Storage',
						formatter: function(value,row,index){
							if(value==0){
								return 'Belum';
							}else{
								return 'Sudah';
							}
						}
					}
				]],
				onUncheckAll:function(){
					var state = $(this).data('datagrid');
					state.checkedRows = [];
					// state.selectedRows = [];
				},
				onCheckAll: function(dataa) {
					var state = $(this).data('datagrid');
					$.post(url).done(function(data){
						var obj = jQuery.parseJSON(data);
						state.checkedRows = obj['rows_total'];
						// state.selectedRows = obj['rows_total'];
					})
				}
            });
            dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Refferensi Baru');
			$('#fm').form('clear');
			url = '<?=base_url();?>refferensi/tambah_refferensi';
			aksi 	= 1;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Ubah Status Data');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>aktivitas/ubah_data_baru/'+row.id;
				aksi 	= 2;
			}
		}
		function detailTemuan(){
			var row = $('#dg').datagrid('getSelected');
			console.log(row);
			if (row){
				window.location.href = '<?=base_url();?>aktivitas/detail_temuan/'+row.category_name+'?id='+row.id;
			}
		}
		function item_cleaning(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Cleaning');
				// $('#fm2').form('load',row);
				url = '<?=base_url();?>aktivitas/item_cleaning/'+row.id;
				aksi 	= 10;
			}
		}
		function item_moving(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Moving');
				url = '<?=base_url();?>aktivitas/item_moving/'+row.id;
				aksi 	= 11;
			}
		}
		function item_measure(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				url = '<?=base_url();?>aktivitas/item_measure/'+row.category_name+'?id='+row.id;
				window.location = url;
			}
		}
		function finalStorage(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg23').dialog('open').dialog('setTitle','Ubah Status Data');
				// $('#fm23').form('load',row);
				url = '<?=base_url();?>aktivitas/item_storage/'+row.id;
				aksi 	= 22;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					console.log(result);
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==10){
							pesan = "Berhasil Update Status Cleaning";
						}else if(aksi==11){
							pesan = "Berhasil Update Status Moving";
						}else if(aksi==22){
							pesan = "Berhasil Update Storage";
						}else{
							pesan = "Berhasil Ubah Status Laporan";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this finding item?',function(r){
					if (r){
						$.post('<?=base_url();?>aktivitas/hapus_data_baru/',{id:row.id,category:row.category},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Temuan'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
		function destroySuperUser(){
			
			var status	= $('#dg').datagrid('getChecked');
			
			if(status.length==0){
				alert("Pilih data terlebih dahulu!");
			}else{
				$("#txtQtySuperDelete").text("Apakah anda yakin untuk menghapus "+status.length+" data?");
				$('#dlg26').dialog('open').dialog('setTitle','Super Delete');
				$('#fm26').form('clear');
			}
		}
		function generateBarcode(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				window.location.href="<?=base_url();?>aktivitas/downloadBarcode/"+row.barcode;
			}
		}
		function generateQrcode(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				window.location.href="<?=base_url();?>aktivitas/downloadQrcode/"+row.barcode;
			}
		}
		function exportExcel(){
			window.location = "<?=base_url();?>aktivitas/export_master_temuan";
		}
		
		$("#idPIC").change(function(){
			var id = this.value;
			window.location.href = '<?=base_url();?>aktivitas/master_temuan/'+id;
		})
		
		function exportBulk(){
			var status	= $('#dg').datagrid('getChecked');
			var jenis 	= $("#jenisCode").val();
			
			if(status.length==0){
				alert("Pilih data terlebih dahulu!");
			}else{
				$("#txtQtyItemExport").text("QTY Item: "+status.length);
				$('#dlg24').dialog('open').dialog('setTitle','Export Bulk Code');
				$('#fm24').form('clear');
			}
		}
		
		function exportBulk_Act(){
			var status	= $('#dg').datagrid('getChecked');
			var jenis 	= $("#jenisCode").val();
			url = '<?=base_url();?>aktivitas/export_bulk';				
			$.post(url,{data:status,jenis:jenis}).done(function(data){
				// console.log(data);
				window.location.href = "<?=base_url();?>/aktivitas/downloadExportBulk/"+data;
			})
		}
		
		function superDelete_Act(){
			var data	= $('#dg').datagrid('getChecked');
			url = '<?=base_url();?>aktivitas/hapus_super_data_baru';				
			$.post(url,{data:data}).done(function(data){
				var obj = jQuery.parseJSON(data);
				$.messager.show({	// show message
									title: 'Notifikasi',
									msg: "Berhasil Hapus"+obj['qty']+" data"
								});
				$('#dlg26').dialog('close');				
				$('#dg').datagrid('reload');	// reload the user data
			})
		}
		
		function importBulkImage(){
			var status		= $('#dg').datagrid('getChecked');
			var jenis 		= $("#jenisCode").val();
			var methodtype 	= $("#methodtype").val();
			var imagenumber = $("#imagenumber").val();
			var namafileori	= new Array();
			var namafileori2= new Array("asdf","asdf");
			// console.log(imagenumber);
			
			if(status.length==0){
				alert("Pilih data terlebih dahulu!");
			}else{
				
				$("#txtQtyItemImage").text("QTY Item: "+status.length);
				$('#dlg25').dialog('open').dialog('setTitle','Import Bulk Image');
				$('#fm25').form('clear');
				
				var defaultParams = {
					data:JSON.stringify(status)
				}
				
				var galleryUploader = new qq.FineUploader({
					element: document.getElementById("fine-uploader-gallery"),
					template: 'qq-template-gallery',
					request: {
						endpoint: '<?=base_url();?>assets/fine-uploader/php-traditional-server-master/endpoint.php',
					},
					thumbnails: {
						placeholders: {
							waitingPath: '<?=base_url();?>assets/fine-uploader/placeholders/waiting-generic.png',
							notAvailablePath: '<?=base_url();?>assets/fine-uploader/placeholders/not_available-generic.png'
						}
					},
					validation: {
						allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
					},
					debug: true,
					callbacks: {
						onSubmit: function(id,name){
							var x = confirm("Apakah anda yakin");
							if(x){
								
							}else{
								return false;
							}
						},
						onUpload: function (id, fileName) {
							
							imagenumber = $("#imagenumber").val();
							
							namafileori.push = fileName;
							var newParams = {
								newPar: fileName,
								imagenumber:imagenumber,
								//JSON.stringify(namafileori)//namafileori
							},
								finalParams = defaultParams;

							qq.extend(finalParams, newParams);
							this.setParams(finalParams);
						}
					}
				});
			}
		}
		
    </script>
	
<?php
$this->load->view('core/v_footer');
?>	
</body>
</html>
