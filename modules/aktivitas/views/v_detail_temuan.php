<?php
	$this->load->view('core/v_header');
?>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Detail Measure Other" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						<div class="easyui-panel" title="Measure Other" style="width:100%;padding:10px;">
							<form id="ff" action="<?=base_url();?>aktivitas/tambah_item_measure" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
								<table width="100%">
									<tr>
										<td width="20%">
										<h4>Data Temuan</h4>
											<table width="100%">
												<tr>
													<td width="20%">Barcode</td>
													<td width="1%">:</td>
													<td><?=$data['barcode'];?></td>
												</tr>
												<tr>
													<td width="20%">Lifting Area</td>
													<td width="1%">:</td>
													<td><?=$data['lifting_area_namee'];?></td>
												</tr>
												<tr>
													<td>Category</td>
													<td>:</td>
													<td><?=$data['category_name'];?></td>
												</tr>
												<tr>
													<td>Sub Category</td>
													<td>:</td>
													<td><?=$data['sub_category_name'];?></td>
												</tr>
												<tr>
													<td>Material</td>
													<td>:</td>
													<td><?=$data['material_name'];?></td>
												</tr>
												<tr>
													<td>Sub Material</td>
													<td>:</td>
													<td><?=$data['sub_material_name'];?></td>
												</tr>
											</table>
										</td>
										<td width="50%" align="right">
											<table width="100%">
												<tr>
													<td width="20%">&nbsp;</td>
													<td width="1%">&nbsp;</td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td width="20%">Cleaning</td>
													<td width="1%">:</td>
													<td><?=$data['status_cleaning']==1?'Sudah':'Belum';?></td>
												</tr>
												<tr>
													<td>Moving</td>
													<td>:</td>
													<td><?=$data['status_moving']==1?'Sudah':'Belum';?></td>
												</tr>
												<tr>
													<td>Measure</td>
													<td>:</td>
													<td><?=$data['status_measure']==1?'Sudah':'Belum';?></td>
												</tr>
												<tr>
													<td>Storage</td>
													<td>:</td>
													<td><?=$data['status_final_storage']==1?'Sudah':'Belum';?></td>
												</tr>
											</table>
										</td>
									</tr>
								
								</table>
								
								
								<hr/>
								<h4>Detail Temuan</h4>
								<?php
								if(empty($dataMeasure)){
									echo 'Belum Pengukuran';
								}else{
									if($category_name == 'coin'){
										
									}else if($category_name == 'bottle'){
										
									}else{
										?>
										<table width="100%">
											<tr>
												<td width="20%">
													<table width="100%">
														<tr>
															<td width="20%">Diameter Top</td>
															<td width="1%">:</td>
															<td><?=$dataMeasure['diameter_top'];?></td>
														</tr>
														<tr>
															<td width="20%">Diameter Bottom</td>
															<td width="1%">:</td>
															<td><?=$dataMeasure['diameter_bottom'];?></td>
														</tr>
														<tr>
															<td>Height</td>
															<td>:</td>
															<td><?=$dataMeasure['height'];?></td>
														</tr>
														<tr>
															<td>Length</td>
															<td>:</td>
															<td><?php
																if($dataMeasure['long']=='' || $dataMeasure['long']=='-'){
																	echo '-';
																}else{
																	echo $dataMeasure['long'];
																};
																?>
															</td>
														</tr>
														<tr>
															<td>Width</td>
															<td>:</td>
															<td>
																<?php
																if($dataMeasure['width']=='' || $dataMeasure['width']=='-'){
																	echo '-';
																}else{
																	echo $dataMeasure['width'];
																};
																?>
															</td>
														</tr>
														<tr>
															<td>Description</td>
															<td>:</td>
															<td><?=$dataMeasure['description'];?></td>
														</tr>
													</table>
												</td>
												<td width="50%" align="right">
												
												</td>
											</tr>
										
										</table>
										<?php
									}
									?>
									
									<hr/>
									<h4>Foto Temuan</h4>
									
									<?php
									$folder_location = base_url().$data['folder_location'];
									if($dataMeasure['image_1']!=null){
										$image = $folder_location.'/'.$dataMeasure['image_1'];
										echo '<a href="'.$image.'" data-lightbox="image-1" data-title="Image 1">Image #1</a>';
									}
									if($dataMeasure['image_2']!=null){
										$image = $folder_location.'/'.$dataMeasure['image_2'];
										echo '<br/><a href="'.$image.'" data-lightbox="image-1" data-title="Image 2">Image #2</a>';
									}
									if($dataMeasure['image_3']!=null){
										$image = $folder_location.'/'.$dataMeasure['image_3'];
										echo '<br/><a href="'.$image.'" data-lightbox="image-1" data-title="Image 3">Image #3</a>';
									}
									if($dataMeasure['image_4']!=null){
										$image = $folder_location.'/'.$dataMeasure['image_4'];
										echo '<br/><a href="'.$image.'" data-lightbox="image-1" data-title="Image 4">Image #4</a>';
									}
									if($dataMeasure['image_5']!=null){
										$image = $folder_location.'/'.$dataMeasure['image_5'];
										echo '<br/><a href="'.$image.'" data-lightbox="image-1" data-title="Image 5">Image #5</a>';
									}
								}
								?>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
	<script>
		
		// $("#idDescription").tinymce();
		tinymce.init({
				selector:"textarea"
			})	
			
		$(document).ready(function() {
				
			// enable fileuploader plugin
			$('input[name="files"]').fileuploader({
				extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				changeInput: ' ',
				theme: 'thumbnails',
				limit: 5,
				enableApi: true,
				addMore: true,
				thumbnails: {
					box: '<div class="fileuploader-items">' +
							  '<ul class="fileuploader-items-list">' +
								  '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
							  '</ul>' +
						  '</div>',
					item: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
								   '<div class="progress-holder">${progressBar}</div>' +
							   '</div>' +
						   '</li>',
					item2: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
							   '</div>' +
						   '</li>',
					startImageRenderer: true,
					canvasImage: false,
					_selectors: {
						list: '.fileuploader-items-list',
						item: '.fileuploader-item',
						start: '.fileuploader-action-start',
						retry: '.fileuploader-action-retry',
						remove: '.fileuploader-action-remove'
					},
					onItemShow: function(item, listEl) {
						var plusInput = listEl.find('.fileuploader-thumbnails-input');
						
						plusInput.insertAfter(item.html);
						
						if(item.format == 'image') {
							item.html.find('.fileuploader-item-icon').hide();
						}
					}
				},
				afterRender: function(listEl, parentEl, newInputEl, inputEl) {
					var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));
				
					plusInput.on('click', function() {
						api.open();
					});
				},
			});
			
		});
	</script>
<?php
	$this->load->view('core/v_footer');
?>	
</body>
</html>