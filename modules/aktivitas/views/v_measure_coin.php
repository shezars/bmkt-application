<?php
	$this->load->view('core/v_header');
?>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Detail Measure Coin" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						<div class="easyui-panel" title="Measure Coin" style="width:100%;padding:10px;">
							<form id="ff" action="<?=base_url();?>aktivitas/tambah_item_measure_coin" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
								<table width="100%">
									<tr>
										<td width="60%">
											<table width="100%">
												<tr>
													<td width="20%">Barcode</td>
													<td width="1%">:</td>
													<td><?=$data['barcode'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td width="20%">Lifting Area</td>
													<td width="1%">:</td>
													<td><?=$data['lifting_area_name'];?></td>
												</tr>
												<tr>
													<td>Category</td>
													<td>:</td>
													<td><?=$data['category_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td>Sub Category</td>
													<td>:</td>
													<td><?=$data['sub_category_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td>Material</td>
													<td>:</td>
													<td><?=$data['material_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td style="padding-bottom:20px;">Sub Material</td>
													<td>:</td>
													<td><?=$data['sub_material_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_coin/diameter.png" data-lightbox="1">Diameter</a></td>
													<td>:</td>
													<td><input name="diameter" class="easyui-textbox"> Cm</td>
													
													<td>Year</td>
													<td>:</td>
													<td><input name="year" class="easyui-textbox" prompt="ex: 1970, 1960, ..."></td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_coin/thickness.png" data-lightbox="1">Thickness</a></td>
													<td>:</td>
													<td><input name="thickness" class="easyui-textbox"> Cm</td>
													
													<td>Mint Mark</td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="mint_mark" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($mint_mark as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_coin/weight.png" data-lightbox="1">Weight</a></td>
													<td>:</td>
													<td><input name="weight" class="easyui-textbox"> Kg</td>
													
													<td>Denomination</td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="denomination" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($denomination as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_coin/coin_type.png" data-lightbox="1">Coin Type</a></td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="coin_type" id="id_coin_type" style="width:200px;" onChange:function(newValue,oldValue){berubah(newValue);}>
															<option value="-">-</option>
															<option value="1">two globes and a coat of arms</option>
															<option value="2">a head / bust and a coat of arms</option>
															<?php
															/*
															foreach($data_refferensi as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['akhir'];?></option>
																<?php
															
															}*/
															?>
														</select>
													</td>
													
													<td>Which Symbol On This Coin</td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="symbol_coin" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($symbol_coin as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
												</tr>
												<tr id="coin_type_1">
													<td style="background-color:#ECECEC ;margin-left:0px;" colspan="3">
														<table width="100%" style="margin-left:0px;">
															<tr>
																<td width="20%">Text Around Top Rim</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="text_around_top_rim" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($text_aroundtoprim as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		
																		}
																		?>
																	</select>
																</td>
															</tr>
															<tr>
																<td width="20%">Kings Name</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="text_1_kings_name" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($kings_name as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		
																		}
																		?>
																	</select>
																</td>
															</tr>
															<tr>
																<td width="20%">After Name of King</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="text_2_after_kings_name" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($after_kings_name as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		
																		}
																		?>
																	</select>
																</td>
															</tr>
															<tr>
																<td width="20%">Assayer's Name In Between</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="assayers_name_in_between" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($assayers_name_between as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		
																		}
																		?>
																	</select>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="coin_type_2">
													<td style="background-color:#ECECEC ;margin-left:0px;" colspan="3">
														<table width="100%" style="margin-left:0px;">
															<tr>
																<td width="20%">Left of Year</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="text_1_left_of_year" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($left_of_year as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		}
																		?>
																	</select>
																</td>
															</tr>
															<tr>
																<td width="20%">Right of Year</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="text_2_right_of_year" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($right_of_year as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		}
																		?>
																	</select>
																</td>
															</tr>
															<tr>
																<td width="20%">Right Above to Left Below</td>
																<td width="1%">:</td>
																<td>
																	<select class="easyui-combobox" name="right_above_to_left_below" style="width:200px;">
																		<option value="-">-</option>
																		<?php
																		foreach($right_toleftbelow as $row){
																			?>
																			<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																			<?php
																		}
																		?>
																	</select>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>Symbol on Reverse</td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="symbol_reverse" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($symbol_reverse as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
													
													<td>Assayer's Initial</td>
													<td>:</td>
													<td><input name="assayers_initial" class="easyui-textbox" prompt="ex: F.M or MM"></td>
												</tr>
												<tr>
													<td>Form of Rim</td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="form_rim" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($form_rim as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															}
															?>
														</select>
													</td>
													
													<td>Form of Edge</td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="form_edge" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($form_edge as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
												</tr>
												<tr>
													<td colspan="6" align="right">
															<input type="file" name="files">
														</form>
													</td>
												</tr>
												<tr>
													<td>Description</td>
													<td>:</td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td colspan="6">
														<textarea name="description" id="idDescription"></textarea>
													</td>
												</tr>
												<tr>
													<td colspan="6" align="right"><input type="submit" value="Save"></td>
												</tr>
											</table>
										</td>
										<td width="50%" align="right">
											
										</td>
									</tr>
								
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
		.textbox{
			width:171px !important;
		}
    </style>
	<script>
	
		$('#id_coin_type').combobox({
			onSelect: function(row){
				var target = this;
				setTimeout(function(){
					if(row.value==1){
						$("#coin_type_1").show();
						$("#coin_type_2").hide();
					}else if(row.value==2){
						$("#coin_type_1").hide();
						$("#coin_type_2").show();
					}
					// $(target).combobox('setText','Hello,World');
				},0);
			}
		})
		
		
		$("#coin_type_1").hide();
		$("#coin_type_2").hide();
		
		// $("#idDescription").tinymce();
		tinymce.init({
				selector:"textarea"
			})	
			
		$(document).ready(function() {
		
			// enable fileuploader plugin
			$('input[name="files"]').fileuploader({
				extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				changeInput: ' ',
				theme: 'thumbnails',
				limit: 5,
				enableApi: true,
				addMore: true,
				thumbnails: {
					box: '<div class="fileuploader-items">' +
							  '<ul class="fileuploader-items-list">' +
								  '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
							  '</ul>' +
						  '</div>',
					item: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
								   '<div class="progress-holder">${progressBar}</div>' +
							   '</div>' +
						   '</li>',
					item2: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
							   '</div>' +
						   '</li>',
					startImageRenderer: true,
					canvasImage: false,
					_selectors: {
						list: '.fileuploader-items-list',
						item: '.fileuploader-item',
						start: '.fileuploader-action-start',
						retry: '.fileuploader-action-retry',
						remove: '.fileuploader-action-remove'
					},
					onItemShow: function(item, listEl) {
						var plusInput = listEl.find('.fileuploader-thumbnails-input');
						
						plusInput.insertAfter(item.html);
						
						if(item.format == 'image') {
							item.html.find('.fileuploader-item-icon').hide();
						}
					}
				},
				afterRender: function(listEl, parentEl, newInputEl, inputEl) {
					var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));
				
					plusInput.on('click', function() {
						api.open();
					});
				},
			});
			
		});
	</script>
<?php
	$this->load->view('core/v_footer');
?>	
</body>
</html>