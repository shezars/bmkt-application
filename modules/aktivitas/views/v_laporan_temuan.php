<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Laporan Temuan" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER LAPORAN TEMUAN" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'name',width:80">Nama Pelapor</th>
										<th data-options="field:'address',width:80">Alamat</th>
										<th data-options="field:'no_telp',width:100">No.Telp</th>
										<th data-options="field:'email',width:100">E-Mail</th>
										<th data-options="field:'location',width:100">Lokasi Temuan</th>
										<th data-options="field:'upload_date',width:100">Tgl Laporan</th>
										<th data-options="field:'attachment',width:100">Lampiran</th>
										<th data-options="field:'status',width:100">Status</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<!--
									<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">New Refferensi</a>
								-->
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah Status Laporan</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus Laporan</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:270px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Refferensi Information</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>Tag Name:</label>
										<input name="tag_name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Description:</label>
										<input name="description" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Value:</label>
										<input name="value" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Parent:</label>
										<input name="parent" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:170px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">User Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>Ubah Status:</label>
										<select name="status_laporan" class="easyui-combobox" required="true">
											<option value='1'>Valid</option>
											<option value='2'>Tidak Valid</option>
										</select>
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>aktivitas/data_master_laporan_temuan',
                pagination: true,
                remoteFilter: true,
                rownumbers: true
            });
            dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Refferensi Baru');
			$('#fm').form('clear');
			url = '<?=base_url();?>refferensi/tambah_refferensi';
			aksi 	= 1;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Ubah Status Laporan');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>aktivitas/ubah_status_laporan_temuan/'+row.id;
				aksi 	= 2;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Refferensi";
						}else{
							pesan = "Berhasil Ubah Status Laporan";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this submission report?',function(r){
					if (r){
						$.post('<?=base_url();?>aktivitas/hapus_status_laporan_temuan/',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Notifikasi'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>