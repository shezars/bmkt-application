<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Lokasi Temuan Baru" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER LOKASI TEMUAN BARU" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'nama',width:80">Nama Lokasi</th>
										<th data-options="field:'wilayah',width:80">Wilayah Lokasi</th>
										<th data-options="field:'long',width:100">Longitude</th>
										<th data-options="field:'lat',width:100">Latitude</th>
										<th data-options="field:'user_created',width:100">User Buat</th>
										<th data-options="field:'date_created',width:100">Tgl Buat</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">Tambah Lokasi Baru</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah Temuan Lokasi</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus Temuan Lokasi</a>
								
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-map" plain="true" onclick="geografis()">Geografis</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-tools" plain="true" onclick="kedalaman()">Kedalaman</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search2" plain="true" onclick="penemuan()">Penemuan</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-speedometer" plain="true" onclick="periode()">Periode</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:330px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Informasi Temuan Baru</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>Nama Lokasi:</label>
										<input name="nama" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Wilayah:</label>
										<input name="wilayah" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Longitude:</label>
										<input name="long" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Latitude:</label>
										<input name="lat" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Kode Pengangkatan:</label>
										<input name="kode" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:270px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">Informasi Temuan Baru</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>Nama Lokasi:</label>
										<input name="nama" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Wilayah:</label>
										<input name="wilayah" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Longitude:</label>
										<input name="long" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Latitude:</label>
										<input name="lat" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Kode Pengangkatan:</label>
										<input name="kode" id="idKodePengangkatanEdit" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg3" class="easyui-dialog" style="width:440px;height:300px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<form id="fm3" method="post" novalidate>
									<textarea name="geografis" id="idGeografis"></textarea>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm3','dlg3')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg3').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg4" class="easyui-dialog" style="width:440px;height:300px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<form id="fm4" method="post" novalidate>
									<textarea name="kedalaman" id="idKedalaman"></textarea>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm4','dlg4')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg4').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg5" class="easyui-dialog" style="width:440px;height:300px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<form id="fm5" method="post" novalidate>
									<textarea name="penemuan" id="idPenemuan"></textarea>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm5','dlg5')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg5').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg6" class="easyui-dialog" style="width:440px;height:300px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<form id="fm6" method="post" novalidate>
									<textarea name="periode" id="idPeriode"></textarea>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm6','dlg6')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg6').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>aktivitas/data_lokasi_temuan_baru',
                pagination: true,
                remoteFilter: true,
                rownumbers: true
            });
            dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Tambah Lokasi Temuan Baru');
			$('#fm').form('clear');
			url = '<?=base_url();?>aktivitas/tambah_lokasi_temuan_baru';
			aksi 	= 1;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Ubah Lokasi Temuab Baru');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>aktivitas/ubah_lokasi_temuan_baru/'+row.id;
				aksi 	= 2;
				
				//update nilai kode pengangkatan
				$.post("<?=base_url();?>aktivitas/get_lokasi_penemuan",{id:row.id}).done(function(data){
					var obj = jQuery.parseJSON(data);
					console.log(obj['kode_wilayah']);
					$("#idKodePengangkatanEdit").textbox({
						value:obj['kode_wilayah']
					});
				})
				
			}
		}
		function geografis(){
			var row = $('#dg').datagrid('getSelected');
			
			$.post("<?=base_url();?>aktivitas/get_lokasi_penemuan",{id:row.id}).done(function(data){
				var data = jQuery.parseJSON(data);
				tinyMCE.get('idGeografis').setContent(data['lokasi_geografis']);
			})
			
			if (row){
				$('#dlg3').dialog('open').dialog('setTitle','Informasi Lokasi Geografis');
				$('#fm3').form('load',row);
				url = '<?=base_url();?>aktivitas/lokasi_geografis/'+row.id;
				aksi 	= 3;
			}
		}
		function kedalaman(){
			var row = $('#dg').datagrid('getSelected');
			
			$.post("<?=base_url();?>aktivitas/get_lokasi_penemuan",{id:row.id}).done(function(data){
				var data = jQuery.parseJSON(data);
				tinyMCE.get('idKedalaman').setContent(data['lokasi_kedalaman']);
			})
			
			if (row){
				$('#dlg4').dialog('open').dialog('setTitle','Informasi Lokasi Kedalaman');
				$('#fm4').form('load',row);
				url = '<?=base_url();?>aktivitas/lokasi_kedalaman/'+row.id;
				aksi 	= 4;
			}
		}
		function penemuan(){
			var row = $('#dg').datagrid('getSelected');
			
			$.post("<?=base_url();?>aktivitas/get_lokasi_penemuan",{id:row.id}).done(function(data){
				var data = jQuery.parseJSON(data);
				tinyMCE.get('idPenemuan').setContent(data['penemuan']);
			})
			
			if (row){
				$('#dlg5').dialog('open').dialog('setTitle','Informasi Penemuan');
				$('#fm5').form('load',row);
				url = '<?=base_url();?>aktivitas/lokasi_penemuan/'+row.id;
				aksi 	= 5;
			}
		}
		function periode(){
			var row = $('#dg').datagrid('getSelected');
			
			$.post("<?=base_url();?>aktivitas/get_lokasi_penemuan",{id:row.id}).done(function(data){
				var data = jQuery.parseJSON(data);
				tinyMCE.get('idPeriode').setContent(data['dinasti_periode']);
			})
			
			if (row){
				$('#dlg6').dialog('open').dialog('setTitle','Informasi Dinasti Periode');
				$('#fm6').form('load',row);
				url = '<?=base_url();?>aktivitas/lokasi_periode/'+row.id;
				aksi 	= 6;
			}
		}
		function saveUser(param,param2){
			
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Lokasi Temuan Baru";
						}else if(aksi==3){
							pesan = "Berhasil Ubah Informasi Geografis";
						}else if(aksi==4){
							pesan = "Berhasil Ubah Informasi Kedalaman";
						}else if(aksi==5){
							pesan = "Berhasil Ubah Informasi Penemuan";
						}else if(aksi==6){
							pesan = "Berhasil Ubah Informasi Dinasti Periode";
						}else{
							pesan = "Berhasil Ubah Lokasi Temuan Baru";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this new location finding?',function(r){
					if (r){
						$.post('<?=base_url();?>aktivitas/hapus_lokasi_temuan_baru/',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
		tinymce.init({
			selector:"textarea",
			menubar: false,
		})	

    </script>
</body>
</html>