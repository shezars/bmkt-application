<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Pemindahan" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER PEMINDAHAN" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'pic_name',width:80">PIC Name</th>
										<th data-options="field:'pic_phone',width:80">PIC Phone</th>
										<th data-options="field:'tujuan',width:100">Destination</th>
										<th data-options="field:'description',width:200">Description</th>
										<th data-options="field:'date_created',width:80">Date Move</th>
										<th data-options="field:'date_return',width:80">Date Return</th>
										<th data-options="field:'status',width:50">Status</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-trucks" plain="true" onclick="newUser()">Pemindahan Baru</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-return" plain="true" onclick="newUser()">Pengembalian Barang</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-faq" plain="true" onclick="detailBarang()">Detail Barang</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-excel2" plain="true" onclick="exportExcel()">Export Excel</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah Pemindahan</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus Pemindahan</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Informasi Pemindahan</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>Tujuan:</label>
										<input name="tujuan" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Deskripsi:</label>
										<input name="deskripsi" class="easyui-textbox" required="true">
									</div>
									<div class="ftitle">Informasi PIC</div>
									<div class="fitem">
										<label>Nama Lengkap:</label>
										<input name="pic_nama_lengkap" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Handphone:</label>
										<input name="pic_handphone" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Tgl Kembali:</label>
										<input name="tgl_kembali" type="text" class="easyui-datebox" required="required">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:170px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">User Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>Ubah Status:</label>
										<select name="status_laporan" class="easyui-combobox" required="true">
											<option value='1'>Valid</option>
											<option value='2'>Tidak Valid</option>
										</select>
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>aktivitas/data_master_pemindahan',
                pagination: true,
                remoteFilter: true,
                rownumbers: true,
				columns:[[
					{field:'pic_name',width:80,title:'PIC Name'},
					{field:'pic_phone',width:80,title:'PIC Phone'},
					{field:'tujuan',width:80,title:'Tujuan'},
					{field:'description',width:80,title:'Deskripsi'},
					{field:'date_created',width:80,title:'Tgl Keluar'},
					{field:'date_return',width:80,title:'Tgl Kembali'},
					{field:'status',width:100,title:'Status',
						formatter: function(value,row,index){
							if(value==0){
								return 'Keluar';
							}else{
								return 'Kembali';
							}
						}
					}
				]]
            });
            // dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Detail Informasi');
			$('#fm').form('clear');
			url = '<?=base_url();?>aktivitas/tambah_pemindahan_baru';
			aksi 	= 1;
		}
		function detailBarang(){
			var row = $('#dg').datagrid('getSelected');
			window.location = "<?=base_url();?>aktivitas/detail_pemindahan/"+row.id;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Ubah Status Laporan');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>aktivitas/ubah_status_laporan_temuan/'+row.id;
				aksi 	= 2;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Pemindahan Baru";
						}else{
							pesan = "Berhasil Ubah Pemindahan";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
					
				}
			});
		}
		function exportExcel(){
			window.location = "<?=base_url();?>aktivitas/export_master_pemindahan";
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this submission report?',function(r){
					if (r){
						$.post('<?=base_url();?>aktivitas/hapus_status_laporan_temuan/',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Notifikasi'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>