<?php
	$this->load->view('core/v_header');
?>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Detail Measure Bottle" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						<div class="easyui-panel" title="Measure Bottle" style="width:100%;padding:10px;">
							<form id="ff" action="<?=base_url();?>aktivitas/tambah_item_measure_bottle" method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
								<table width="100%">
									<tr>
										<td width="60%">
											<table width="100%">
												<tr>
													<td width="20%">Barcode</td>
													<td width="1%">:</td>
													<td><?=$data['barcode'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td width="20%">Lifting Area</td>
													<td width="1%">:</td>
													<td><?=$data['lifting_area_name'];?></td>
												</tr>
												<tr>
													<td>Category</td>
													<td>:</td>
													<td><?=$data['category_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td>Sub Category</td>
													<td>:</td>
													<td><?=$data['sub_category_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td>Material</td>
													<td>:</td>
													<td><?=$data['material_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td style="padding-bottom:20px;">Sub Material</td>
													<td>:</td>
													<td><?=$data['sub_material_name'];?></td>
													<td colspan="3"></td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_bottle/bottle_shape.png" data-lightbox="1">Bottle Shape</a></td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="bottle_shape" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($bottle_shape as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
													
													<td><a href="<?=base_url();?>assets/images/measure_bottle/height.png" data-lightbox="2">Height</a></td>
													<td>:</td>
													<td><input name="height" class="easyui-textbox"> Cm</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_bottle/bottle_lips.png" data-lightbox="3">Bottle Lips</a></td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="bottle_lips" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($bottle_lips as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
													
													<td><a href="<?=base_url();?>assets/images/measure_bottle/lip_diameter.png" data-lightbox="4">Lip Diameter</a></td>
													<td>:</td>
													<td><input name="lip_diameter" class="easyui-textbox"> Cm</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_bottle/mouth_diameter.png" data-lightbox="5">Mouth Diameter</a></td>
													<td>:</td>
													<td><input name="mouth_diameter" class="easyui-textbox"> Cm</td>
													
													<td><a href="<?=base_url();?>assets/images/measure_bottle/lip_thickness.png" data-lightbox="6">Lip Thickness</a></td>
													<td>:</td>
													<td><input name="lip_thickness" class="easyui-textbox"> Cm</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_bottle/lip_height.png" data-lightbox="7">Lip Height</a></td>
													<td>:</td>
													<td><input name="lip_height" class="easyui-textbox"> Cm</td>
													
													<td><a href="<?=base_url();?>assets/images/measure_bottle/neck_smallest_diameter.png" data-lightbox="8">Neck Smallest Diameter</a></td>
													<td>:</td>
													<td><input name="neck_smallest_diameter" class="easyui-textbox"> Cm</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_coin/body_artifact_measured.png" data-lightbox="9">Is Neck/Body Artifact Measured</a></td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="measured_artifact" id="id_measured_artifact" style="width:200px;">
															<option value="-">-</option>
															<option value="1">Yes</option>
															<option value="2">No</option>
														</select>
													</td>
												</tr>
												<tr id="coin_type_1">
													<td style="background-color:#ECECEC ;margin-left:0px;" colspan="3">
														<table width="100%" style="margin-left:0px;">
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/neck_height.png" data-lightbox="10">Neck Height</a></td>
																<td>:</td>
																<td><input name="neck_height" class="easyui-textbox"> Cm</td>
															</tr>
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/body_height.png" data-lightbox="11">Body Height</a></td>
																<td>:</td>
																<td><input name="body_height" class="easyui-textbox"> Cm</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_coin/artefact_shape.png" data-lightbox="12">Artefact Shape</a></td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="artefact_shape" id="id_artefact_shape" style="width:200px;">
															<option value="-">-</option>
															<option value="1">Cylinder</option>
															<option value="2">Case</option>
														</select>
													</td>
													
													<td><a href="<?=base_url();?>assets/images/measure_bottle/pontil_scars_type.png" data-lightbox="13">Choose Pontil Scars</a></td>
													<td>:</td>
													<td>
														<select class="easyui-combobox" name="pontil_scars" style="width:200px;">
															<option value="-">-</option>
															<?php
															foreach($pontil_scars as $row){
																?>
																<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
																<?php
															
															}
															?>
														</select>
													</td>
												</tr>
												<tr id="artefact_shape_1">
													<td style="background-color:#ECECEC ;margin-left:0px;" colspan="3">
														<table width="100%" style="margin-left:0px;">
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/body_diameter.png" data-lightbox="14">Body Diameter</a></td>
																<td>:</td>
																<td><input name="body_diameter" class="easyui-textbox"> Cm</td>
															</tr>
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/base_diameter.png" data-lightbox="15">Base Diameter</a></td>
																<td>:</td>
																<td><input name="base_diameter" class="easyui-textbox"> Cm</td>
															</tr>
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/base_inside_diameter.png" data-lightbox="16">Base Inside Diameter</a></td>
																<td>:</td>
																<td><input name="base_inside_diameter" class="easyui-textbox"> Cm</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="artefact_shape_2">
													<td style="background-color:#ECECEC ;margin-left:0px;" colspan="3">
														<table width="100%" style="margin-left:0px;">
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/four_largest_sides.png" data-lightbox="17">Four Largest Sides</a></td>
																<td>:</td>
																<td>
																	a <input name="four_largest_sides_a" class="easyui-textbox"> Cm<br/>
																	b <input name="four_largest_sides_b" class="easyui-textbox"> Cm<br/>
																	c <input name="four_largest_sides_c" class="easyui-textbox"> Cm<br/>
																	d <input name="four_largest_sides_d" class="easyui-textbox"> Cm
																</td>
															</tr>
															<tr>
																<td><a href="<?=base_url();?>assets/images/measure_bottle/four_smallest_sides.png" data-lightbox="18">Four Smallest Sides</a></td>
																<td>:</td>
																<td>
																	a <input name="four_smallest_sides_a" class="easyui-textbox"> Cm<br/>
																	b <input name="four_smallest_sides_b" class="easyui-textbox"> Cm<br/>
																	c <input name="four_smallest_sides_c" class="easyui-textbox"> Cm<br/>
																	d <input name="four_smallest_sides_d" class="easyui-textbox"> Cm
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td><a href="<?=base_url();?>assets/images/measure_bottle/pontil_scars_diameter.png" data-lightbox="19">Pontil Scar Diameter</a></td>
													<td>:</td>
													<td><input name="pontil_scar_diameter" class="easyui-textbox"> Cm</td>
													
													<td><a href="<?=base_url();?>assets/images/measure_bottle/pontil_scars_depth.png" data-lightbox="20">Pontil Scar Depth</a></td>
													<td>:</td>
													<td><input name="pontil_scar_depth" class="easyui-textbox"> Cm</td>
												</tr>
												<tr>
													<td colspan="6" align="right">
															<input type="file" name="files">
														</form>
													</td>
												</tr>
												<tr>
													<td>Description</td>
													<td>:</td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td colspan="6">
														<textarea name="description" id="idDescription"></textarea>
													</td>
												</tr>
												<tr>
													<td colspan="6" align="right"><input type="submit" value="Save"></td>
												</tr>
											</table>
										</td>
										<td width="50%" align="right">
											
										</td>
									</tr>
								
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
		.textbox{
			width:171px !important;
		}
    </style>
	<script>
	
		$('#id_measured_artifact').combobox({
			onSelect: function(row){
				var target = this;
				setTimeout(function(){
					if(row.value==1){
						$("#coin_type_1").show();
						$("#coin_type_2").hide();
					}
					else if(row.value==2){
						$("#coin_type_1").hide();
						$("#coin_type_2").hide();
					}
				},0);
			}
		})
		
		$('#id_artefact_shape').combobox({
			onSelect: function(row){
				var target = this;
				setTimeout(function(){
					if(row.value==1){
						$("#artefact_shape_1").show();
						$("#artefact_shape_2").hide();
					}
					else if(row.value==2){
						$("#artefact_shape_1").hide();
						$("#artefact_shape_2").show();
					}
				},0);
			}
		})
		
		
		$("#coin_type_1").hide();
		$("#coin_type_2").hide();
		
		$("#artefact_shape_1").hide();
		$("#artefact_shape_2").hide();
		
		// $("#idDescription").tinymce();
		tinymce.init({
				selector:"textarea"
			})	
			
		$(document).ready(function() {
		
			// enable fileuploader plugin
			$('input[name="files"]').fileuploader({
				extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				changeInput: ' ',
				theme: 'thumbnails',
				limit: 5,
				enableApi: true,
				addMore: true,
				thumbnails: {
					box: '<div class="fileuploader-items">' +
							  '<ul class="fileuploader-items-list">' +
								  '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
							  '</ul>' +
						  '</div>',
					item: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
								   '<div class="progress-holder">${progressBar}</div>' +
							   '</div>' +
						   '</li>',
					item2: '<li class="fileuploader-item">' +
							   '<div class="fileuploader-item-inner">' +
								   '<div class="thumbnail-holder">${image}</div>' +
								   '<div class="actions-holder">' +
									   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
									   '<span class="fileuploader-action-popup"></span>' +
								   '</div>' +
							   '</div>' +
						   '</li>',
					startImageRenderer: true,
					canvasImage: false,
					_selectors: {
						list: '.fileuploader-items-list',
						item: '.fileuploader-item',
						start: '.fileuploader-action-start',
						retry: '.fileuploader-action-retry',
						remove: '.fileuploader-action-remove'
					},
					onItemShow: function(item, listEl) {
						var plusInput = listEl.find('.fileuploader-thumbnails-input');
						
						plusInput.insertAfter(item.html);
						
						if(item.format == 'image') {
							item.html.find('.fileuploader-item-icon').hide();
						}
					}
				},
				afterRender: function(listEl, parentEl, newInputEl, inputEl) {
					var plusInput = listEl.find('.fileuploader-thumbnails-input'),
						api = $.fileuploader.getInstance(inputEl.get(0));
				
					plusInput.on('click', function() {
						api.open();
					});
				},
			});
			
		});
	</script>
<?php
	$this->load->view('core/v_footer');
?>	
</body>
</html>