<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Import Data Baru" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						<div class="easyui-panel" title="Import Data Baru" style="width:100%;padding:10px;">
							<form id="uploadFile" method="post" enctype="multipart/form-data">
								<label>Upload File Here</label>
								<input class="easyui-filebox" id="chooseFile" name="userfile">
								<a id="btn_add" href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true">Add File</a>
							</form>
							<br/>
							<div id="progressFile" class="easyui-progressbar" style="width:100%;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width:800px;height:auto;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">TOTAL: 4 DATA | <a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-upload" plain="true" onclick="masukkanTempData()">Masukkan Data</a></div>
		<label>Lifting Area: </label><select class="easyui-combobox"  id="idLiftingAreaImport">
			<?php
			foreach($lifting_area as $row){
				echo '<option value="'.$row['id'].'-'.$row['kode_wilayah'].'">'.$row['nama'].'</option>';
			}
			?>
		</select><br/>
		<table id="dg" title="Summary Info" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
			<thead>
				<tr>
					<th data-options="field:'category',width:80">Category</th>
					<th data-options="field:'subcategory',width:80">Sub Category</th>
					<th data-options="field:'material',width:100">Material</th>
					<th data-options="field:'submaterial',width:100">Sub Material</th>
				</tr>
			</thead>
		</table>
		
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
	<script>
		$("#progressFile").hide();
		$('#uploadFile').form({
			url:'<?=base_url();?>aktivitas/import_tambah_data_baru',
			ajax:true,
			iframe:false,
			onProgress: function(percent){
				$('#progressFile').progressbar('setValue', percent);    
			},
			success: function(data){
					var obj = jQuery.parseJSON(data);
					if(obj.status){
						alert("Import Succeess");
						temp_data_import();
					}else{
						$.messager.alert('Error',obj.errorMsg);
					}
			},
			onLoadError: function(){
					// apres l'envoi du fichier en cas d'erreur
			}
		});
	
		$('#btn_add').bind('click', function(){
			if($('#chooseFile').filebox('getValue')!="") {
				$.messager.confirm('Confirm','Are you sure you want to import file?',function(r){
					if (r){
						$("#progressFile").show();
						$('#uploadFile').submit();
					}
				});
			}else{
				alert("Choose the file first");
			}
		});
		
		function temp_data_import(){
			$('#dlg').dialog('open').dialog('setTitle','Summary File');
			
			var dg = $('#dg').datagrid({
                url: '<?=base_url();?>aktivitas/get_data_import_temp',
                pagination: true,
                remoteFilter: true,
                rownumbers: true,
				// pageSize: 5,
				// pageList:[5,10,20,30,40,20]
            });
            dg.datagrid('enableFilter');
		}
		
		function masukkanTempData(){
			$.messager.confirm('Confirm','Yakin memasukkan semua data ini?',function(r){
				if (r){
					var lifting_area = $("#idLiftingAreaImport").val();
					$.post("<?=base_url();?>aktivitas/proses_masukan_data_import",{lifting:lifting_area})
					.success(function(data){
						$.messager.alert({
							title: 'Informasi',
							msg: 'Data Baru Berhasil Masuk Kedalam System',
							fn: function(){
								location.reload();
							}
						});
					})
				}
			});
		}
		
		var temp2 	= new Array();
		var xyz 	= null;
		var digit 	= null;
		var other_temp = null;
		
		newNumber();
		function newNumber(){
			$.get("<?=base_url();?>aktivitas/newNumber",function(result){
				xyz = result;
			})
		}
		
		getDigit();
		function getDigit(){
			$.get("<?=base_url();?>aktivitas/getDigit",function(result){
				var obj = jQuery.parseJSON(result);
				digit = obj['value'];
			})
		}
		
		function generateBarcode(sel,param){
			
			var formatJoin;
		
			var getFormat = $.get("<?=base_url();?>aktivitas/getBarcodeFormat",function(result){
				var obj = jQuery.parseJSON(result);
				var split = obj['value'].split("-");
				var originalValue = $("#idBarcode").val();
				
				if(param=='subcategory'){
					temp = sel.other;
				}else{
					temp = sel.value.split('-');
					temp = temp[1];
				}
				
				for(x=0;x<split.length;x++){
					if(split[x] == param){
						
						var index = split.indexOf(param);
						if (index !== -1) {
							temp2[x] = temp;
						}else{
							temp2.push(temp);
						}
					}
				}
				
				formatJoin = temp2.join('')+lpad(xyz,parseInt(digit));
				$("#idNewNumber").val(formatJoin);
				
				$("#idBarcode").textbox({
					disabled	:true,
					value		: formatJoin
				});
				
			})
		}
		
		function lpad(value, padding) {
			var zeroes = new Array(padding+1).join("0");
			return (zeroes + value).slice(-padding);
		}
		
		function changeCategory(rec){
			var value = rec.value.split("-");
			var temp='';
			var url = '<?=base_url();?>aktivitas/getSubCategory/'+value[1];
			
			$('#cc2').combobox({
				url:url,
				valueField:'id',
				textField:'value'
			});
		}
		
		function changeMaterial(rec){
			var temp='';
			var url = '<?=base_url();?>aktivitas/getSubMaterial/'+rec.value;
			
			$('#cc3').combobox({
				url:url,
				valueField:'other',
				textField:'value'
			});
		}
	</script>
<?php
	$this->load->view('core/v_footer');
?>	
</body>
</html>