<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
		
		.btnModif {
		  background: #3498db;
		  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
		  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
		  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
		  background-image: -o-linear-gradient(top, #3498db, #2980b9);
		  background-image: linear-gradient(to bottom, #3498db, #2980b9);
		  -webkit-border-radius: 7;
		  -moz-border-radius: 7;
		  border-radius: 7px;
		  font-family: Arial;
		  color: #ffffff;
		  font-size: 12px;
		  padding: 6px 16px 6px 16px;
		  text-decoration: none;
		}

		.btnModif:hover {
		  background: #3cb0fd;
		  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
		  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
		  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
		  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
		  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
		  text-decoration: none;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Informasi Detail Barang Pemindahan" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
							<div style="padding-bottom:10px;">
								<table width="100%">
									<tr>
										<td width="10%">Tujuan</td>
										<td width="1%">:</td>
										<td><?=$getDetailPemindahan['tujuan'];?></td>
									</tr>
									<tr>
										<td>Deskripsi</td>
										<td>:</td>
										<td><?=$getDetailPemindahan['description'];?></td>
									</tr>
									<tr>
										<td>Nama PIC</td>
										<td>:</td>
										<td><?=$getDetailPemindahan['pic_name'];?></td>
									</tr>
									<tr>
										<td>Handphone PIC</td>
										<td>:</td>
										<td><?=$getDetailPemindahan['pic_phone'];?></td>
									</tr>
								</table>
							</div>
						
							<table id="dg" title="DETAIL BARANG PEMINDAHAN" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'barcode',width:80">Barcode</th>
										<th data-options="field:'lifting_area_name',width:80">Lifting Area</th>
										<th data-options="field:'category_name',width:80">Category</th>
										<th data-options="field:'sub_category_name',width:100">Sub Category</th>
										<th data-options="field:'material_name',width:200">Material</th>
										<th data-options="field:'sub_material_name',width:80">Sub Material</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">Tambah Barang</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus Barang</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:750px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Daftar Barang</div>
								
								<table id="dg2" style="width:auto;padding:10px;" singleSelect="true" fitColumns="true" rownumbers="true">
									<thead>
										<tr>
											<th data-options="field:'id',width:80">#</th>
											<th data-options="field:'barcode',width:80">Barcode</th>
											<th data-options="field:'lifting_area',width:80">Lifting Area</th>
											<th data-options="field:'category',width:80">Category</th>
											<th data-options="field:'subcategory',width:80">Sub Category</th>
											<th data-options="field:'material',width:100">Material</th>
											<th data-options="field:'submaterial',width:100">Sub Material</th>
										</tr>
									</thead>
								</table>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="closedlg()" style="width:90px">Close</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:170px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">User Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>Ubah Status:</label>
										<select name="status_laporan" class="easyui-combobox" required="true">
											<option value='1'>Valid</option>
											<option value='2'>Tidak Valid</option>
										</select>
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
		var url;
		var aksi;
		var pesan;
		var id_pemindahan='<?=$id_pemindahan;?>';
		
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>aktivitas/data_master_detail_pemindahan/'+id_pemindahan,
                pagination: true,
                remoteFilter: true,
                rownumbers: true,
            });
            // dg.datagrid('enableFilter');
        });
		
		function CloseFunction(){
			alert("ea");
		}
		
		function closedlg(){
			$('#dlg').dialog('close');
			$('#dg').datagrid('reload');
		}
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Informasi Daftar Barang');
		
			var dg2 = $('#dg2').datagrid({
                url: '<?=base_url();?>aktivitas/data_master_temuan',
                pagination: true,
                remoteFilter: true,
                rownumbers: true,
				columns:[[
					{field:'id',width:80,
						formatter: function(value,row,index){
							return '<button class="btnModif" id="'+value+'" onclick="addItemPemindahan(this)">Add</button>';
						}
					},
					{field:'barcode',width:80,title:'Barcode'},
					{field:'lifting_area_name',width:80,title:'Lifting Area'},
					{field:'category_name',width:80,title:'Category'},
					{field:'sub_category_name',width:80,title:'Sub Category'},
					{field:'material_name',width:80,title:'Material'},
					{field:'sub_material_name',width:100,title:'Sub Material'}
				]]
            });
            dg2.datagrid('enableFilter',[{
				field:'id',
				type:'label'
			}]);
		}
		function detailBarang(){
			var row = $('#dg').datagrid('getSelected');
			window.location = "<?=base_url();?>aktivitas/detail_pemindahan/"+row.id;
		}
		function addItemPemindahan(param){
			$.post("<?=base_url();?>aktivitas/add_item_pemindahan",{id:param.id,id_pemindahan:id_pemindahan}).success(function(data){
				$('#dg2').datagrid('reload');
			})
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Ubah Status Laporan');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>aktivitas/ubah_status_laporan_temuan/'+row.id;
				aksi 	= 2;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Pemindahan Baru";
						}else{
							pesan = "Berhasil Ubah Pemindahan";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
					
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this submission report?',function(r){
					if (r){
						$.post('<?=base_url();?>aktivitas/hapus_status_laporan_temuan/',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Notifikasi'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>