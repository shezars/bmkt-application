<?php
class M_aktivitas extends CI_Model  {
	function laporan_temuan_baru_add($data){
		$this->db->insert('laporan_penemuan',$data);
	}
	
	function getDigit(){
		return $this->db->get_where('refferensi',array('tag_name'=>'autonum_4'))->row_array();
	}
	
	function get_lokasi_penemuan($id){
		return $this->db->get_where('lokasi_penemuan',array('id'=>$id))->row_array();
	}
	
	function getRefferensi($id,$value=null,$tag_name=null,$parent=null){
		if($value==null){
			$this->db->where('id',$id);
		}else{
			$this->db->where('value',$value);
			if($tag_name!=null){
				$this->db->where('tag_name',$tag_name);
			}
			if($parent!=null){
				$this->db->where('parent',$parent);
			}
		}
		return $this->db->get_where('refferensi')->row_array();
	}
	
	function get_users($group_id=null){
		if($group_id!=null){
			$this->db->where('c.id',$group_id);
		}
		$this->db->select('a.*');
		$this->db->join('users_groups b','b.user_id = a.id');
		$this->db->join('groups c','c.id = b.group_id');
		return $this->db->get('users a')->result_array();
	}
	
	function newNumber(){
		return $this->db->get('master_item')->num_rows();
	}
	
	function getBarcodeFormat(){
		return $this->db->get_where('refferensi',array('tag_name'=>'barcode_format'))->row_array();
	}
	
	function tambah_lokasi_temuan_baru($data){
		$this->db->insert('lokasi_penemuan',$data);
	}
	
	function tambah_data_baru($data){
		$this->db->insert('master_item',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	function tambah_data_baru_ukur_other($data){
		$this->db->insert('measure_other',$data);
	}
	
	function hapus_data_baru_ukur($category,$id){
		$this->db->where('id',$id);
		if($category == 'bottle'){
			$this->db->delete('measure_bottle');
		}else if($category == 'coin'){
			$this->db->delete('measure_coin');
		}else{
			$this->db->delete('measure_other');
		}
	}
	
	function update_temp_upload($id,$data){
		$this->db->where('id',$id);
		$this->db->update('temp_upload',$data);
	}
	
	function hapus_data_baru($id){
		$this->db->where('id',$id);
		$this->db->delete('master_item');
	}
	
	function ubah_lokasi_temuan_baru($id,$data){
		$this->db->where('id',$id);
		$this->db->update('lokasi_penemuan',$data);
	}
	
	function hapus_lokasi_temuan_baru($id){
		$this->db->where('id',$id);
		$this->db->delete('lokasi_penemuan');
	}
	
	function data_lokasi_temuan_baru(){
		$this->db->select('*');
		$data = $this->db->get('lokasi_penemuan');
		return $data->result_array();
	}
	
	function get_data_import_temp(){
		$this->db->select('*');
		$this->db->where('status','0');
		$data = $this->db->get('temp_upload');
		return $data->result_array();
	}
	
	function tambah_pemindahan_baru($data){
		$this->db->insert('pemindahan',$data);
	}
	
	function getDetailPemindahan($id){
		return $this->db->get_where('pemindahan',array('id'=>$id))->row_array();
	}
	
	function data_master_pemindahan(){
		$this->db->select('*');
		$data = $this->db->get('pemindahan');
		return $data->result_array();
	}
	
	function data_master_detail_pemindahan($id){
		$this->db->select('a.*,b.barcode,
			(SELECT c.wilayah FROM lokasi_penemuan c WHERE c.id = b.lifting_area) AS lifting_area_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = b.category) AS category_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = b.subcategory) AS sub_category_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = b.material) AS material_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = b.submaterial) AS sub_material_name
		');
		$this->db->select('a.*');
		$this->db->join('master_item b','b.id=a.id_master_item');
		$this->db->where('a.id_pemindahan',$id);
		$data = $this->db->get('pemindahan_detail a');
		return $data->result_array();
	}
	
	function data_master_laporan_temuan(){
		$this->db->select('*');
		$data = $this->db->get('laporan_penemuan');
		return $data->result_array();
	}
	function getRefferensiByTag($tag_name,$return_array=null,$parent=null){
		
		if($parent!=null){
			$this->db->where('parent',$parent);
		}
		$this->db->where('tag_name',$tag_name);
			
		if($return_array!=null){
			return $this->db->get('refferensi')->result_array();
		}else{
			return $this->db->get('refferensi')->row_array();
		}
	}
	function data_master_temuan($page,$rows,$filterParam,$where){
		$offset = ($page-1)*$rows;
		
		if($filterParam == null){
			
			$this->db->select('a.*,b.complete_name as nama_lengkap,
			(SELECT b.wilayah FROM lokasi_penemuan b WHERE b.id = a.lifting_area) AS lifting_area_name,
			(SELECT b.nama FROM lokasi_penemuan b WHERE b.id = a.lifting_area) AS lifting_area_namee,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.category) AS category_name,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.subcategory) AS sub_category_name,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.material) AS material_name,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.submaterial) AS sub_material_name
			');
			$this->db->join('users b','b.id = a.user_created','left');
			$this->db->join('users_groups c','c.user_id = b.id','left');
		}else{
			
			$this->db->select('a.*,b.complete_name as nama_lengkap,
			(SELECT b.wilayah FROM lokasi_penemuan b WHERE b.id = a.lifting_area) AS lifting_area_name,
			(SELECT b.nama FROM lokasi_penemuan b WHERE b.id = a.lifting_area) AS lifting_area_namee,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.category) AS category_name,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.subcategory) AS sub_category_name,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.material) AS material_name,
			(SELECT b.VALUE FROM refferensi b WHERE b.id = a.submaterial) AS sub_material_name
			');
			$this->db->join('users b','b.id = a.user_created','left');
			$this->db->join('users_groups c','c.user_id = b.id','left');
			$this->db->where($filterParam);
		}
		$this->db->where('status','0');
		if($where!=null){
			$this->db->where($where);
		}
		$this->db->limit($rows,$offset);
		$data = $this->db->get('master_item a');
		return $data->result_array();
	}
	
	function getRackFromWarehouse($id){
		$this->db->where('tag_name','storage_warehouse_rak');
		$this->db->where('parent',$id);
		return $this->db->get('refferensi')->result_array();
	}
	
	function getBoxFromWarehouse($id){
		$this->db->where('tag_name','storage_warehouse_box');
		$this->db->where('parent',$id);
		return $this->db->get('refferensi')->result_array();
	}
	
	function data_master_temuan_by_id($id){
		
		$this->db->select('a.*,
			(SELECT c.wilayah FROM lokasi_penemuan c WHERE c.id = a.lifting_area) AS lifting_area_name,
			(SELECT c.nama FROM lokasi_penemuan c WHERE c.id = a.lifting_area) AS lifting_area_namee,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = a.category) AS category_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = a.subcategory) AS sub_category_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = a.material) AS material_name,
			(SELECT c.VALUE FROM refferensi c WHERE c.id = a.submaterial) AS sub_material_name
		');
		$this->db->where('a.id',$id);
		$data = $this->db->get('master_item a');
		return $data->row_array();
		
	}
	
	function data_master_pengukuran_coin_by_id($id){
		return $this->db->get_where('measure_coin',array('master_item_id'=>$id))->row_array();
	}
	
	function data_master_pengukuran_bottle_by_id($id){
		return $this->db->get_where('measure_bottle',array('master_item_id'=>$id))->row_array();
	}
	
	function data_master_pengukuran_other_by_id($id){
		return $this->db->get_where('measure_other',array('master_item_id'=>$id))->row_array();
	}
	
	function all_barang_master_temuan($status=null){
		
		if($status==null){
			$this->db->where('status','0');
		}else{
			$this->db->where('status',$status);
		}
		
		return $this->db->get('master_item')->result_array();
	}
	
	function data_master_temuan_all($where=null){
		
		$this->db->select('a.*,
		(SELECT b.wilayah FROM lokasi_penemuan b WHERE b.id = a.lifting_area) AS lifting_area_name,
		(SELECT b.nama FROM lokasi_penemuan b WHERE b.id = a.lifting_area) AS lifting_area_namee,
		(SELECT b.VALUE FROM refferensi b WHERE b.id = a.category) AS category_name,
		(SELECT b.VALUE FROM refferensi b WHERE b.id = a.subcategory) AS sub_category_name,
		(SELECT b.VALUE FROM refferensi b WHERE b.id = a.material) AS material_name,
		(SELECT b.VALUE FROM refferensi b WHERE b.id = a.submaterial) AS sub_material_name
		');
		$this->db->join('users b','b.id = a.user_created','left');
		$this->db->join('users_groups c','c.user_id = b.id','left');
			
		$this->db->where('status','0');
		
		if($where!=null){
			$this->db->where($where);
		}
		$data = $this->db->get('master_item a');
		return $data->result_array();
	}
	
	function ubah_status_laporan_temuan($id,$data){
		$this->db->where('id',$id);
		$this->db->update('laporan_penemuan',$data);
	}
	
	function hapus_status_laporan_temuan($id){
		$this->db->where('id',$id);
		$this->db->delete('laporan_penemuan');
	}
	
	function lifting_area(){
		return $this->db->get('lokasi_penemuan')->result_array();
	}
	
	function getCategory(){
		return $this->db->get_where('refferensi',array('tag_name'=>'category'))->result_array();
	}
	
	function getMaterial(){
		return $this->db->get_where('refferensi',array('tag_name'=>'material'))->result_array();
	}
	
	function getSubCategory($id){
		$this->db->where('tag_name','subcategory');
		$this->db->where('parent',$id);
		return $this->db->get('refferensi')->result_array();
	}
	
	function getSubMaterial($id){
		$this->db->where('tag_name','submaterial');
		$this->db->where('parent',$id);
		return $this->db->get('refferensi')->result_array();
	}
	
	function getNamaWilayah_lokasiPenemuan($id_wilayah){
		return $this->db->get_where('lokasi_penemuan',array('id'=>$id_wilayah))->row_array();
	}
	
	function getGrafikTemuan($id_wilayah){
		if($id_wilayah==null || $id_wilayah ==''){
			$id_wilayah=1;
		}
		$this->db->select('b.id AS id_lokasi_wilayah, c.id AS id_category, c.value, (SELECT COUNT(*) FROM master_item d WHERE d.lifting_area = '.$id_wilayah.' AND d.category = c.id) AS qty');
		$this->db->join('lokasi_penemuan b','b.id = a.lifting_area');
		$this->db->join('refferensi c','c.id = a.category');
		if($id_wilayah != null){
			$this->db->where('a.lifting_area',$id_wilayah);
		}
		$this->db->group_by('c.value');
		return $this->db->get('master_item a')->result_array();
	}
	
	function getGrafikTemuanDrilldown(){
		$this->db->select('a.category AS id_category, a.subcategory AS id_subcategory, b.value, ( SELECT COUNT(*) FROM master_item c WHERE c.subcategory = a.subcategory ) AS qty');
		$this->db->join('refferensi b','b.id = a.subcategory');
		$this->db->where('b.tag_name','subcategory');
		$this->db->group_by('a.subcategory');
		return $this->db->get('master_item a')->result_array();
	}
	
	function lokasi_geografis($id,$data){
		$this->db->where('id',$id);
		$this->db->update('lokasi_penemuan',$data);
	}
	
}	
?>