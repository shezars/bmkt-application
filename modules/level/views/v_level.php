<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Data Level" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER DATA LEVEL" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'name',width:80">Name</th>
										<th data-options="field:'description',width:80">Description</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">New Level</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit Level</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Level</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-role-playing-game" plain="true" onclick="roleUser()">Role Level</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:210px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Level Information</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>Name:</label>
										<input name="name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Description:</label>
										<input name="description" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:210px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">Level Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>Name:</label>
										<input name="name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Description:</label>
										<input name="description" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<!-- ROLE LEVEL -->
							<div id="dlg3" class="easyui-dialog" style="width:620px;height:510px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">Role Level Form</div>
								<form id="fm3" method="post" novalidate>
									<div class="fitem">
										<label>Modules:</label>
										<select name="module" class="easyui-combobox" required="true">
											<?php
											foreach($data_module as $row){
												echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
											}
											?>
										</select>
										<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm3','dlg3')" style="width:90px">Save</a>
									</div>
								</form>
								<div class="ftitle">Master Role Level</div>
								<table id="dg2" title="MASTER DATA MODULES" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar2" singleSelect="true" fitColumns="true" rownumbers="true">
									<thead>
										<tr>
											<th data-options="field:'description',width:80">Level</th>
											<th data-options="field:'module_name',width:80">Modules</th>
										</tr>
									</thead>
								</table>
								<div id="toolbar2">
									<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeRoleLevel()">Remove</a>
								</div>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg3').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>level/data_level',
                pagination: true,
                remoteFilter: true,
                rownumbers: true
            });
            dg.datagrid('enableFilter');
        });
		
		function kodok(group_id){
			var dg2 = $('#dg2').datagrid({
				url: '<?=base_url();?>level/data_role_level/'+group_id,
				pagination: true,
				remoteFilter: true,
				rownumbers: true
			});
			dg2.datagrid('enableFilter');
		}
		
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Tambah Level Baru');
			$('#fm').form('clear');
			url = '<?=base_url();?>level/tambah_level';
			aksi 	= 1;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Ubah Level');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>level/ubah_level/'+row.id;
				aksi 	= 2;
			}
		}
		function roleUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg3').dialog('open').dialog('setTitle','Role Level');
				kodok(row.id);
				
				url = '<?=base_url();?>level/tambah_role_level/'+row.id;
				aksi 	= 3;
			}
		}
		function removeRoleLevel(){
			var row = $('#dg2').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to remove this module?',function(r){
					if (r){
						$.post('<?=base_url();?>level/hapus_role_level',{id:row.id_module_group},function(result){
							console.log(result);
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show notifikasi
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Module'
								});
								$('#dg2').datagrid('reload');
							}
						},'json');
					}
				});
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Level";
						}else if(aksi==1){
							pesan = "Berhasil Ubah Level";
						}else{
							pesan = "Berhasil Tambah Role Level";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
						
						if(param=='fm3'){
							$('#dg2').datagrid('reload');	// reload the user data
						}else{
							$('#'+param2).dialog('close');		// close the dialog
							$('#dg').datagrid('reload');	// reload the user data
						}		
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this level?',function(r){
					if (r){
						$.post('<?=base_url();?>level/hapus_level',{id:row.id},function(result){
							
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show notifikasi
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Level'
								});
								$('#dg').datagrid('reload');
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>