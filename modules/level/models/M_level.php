<?php
class M_level extends CI_Model  {
	
	function data_level($page,$rows,$filterParam){
		$offset = ($page-1)*$rows;
		
		if($filterParam == null){
			$sql = "SELECT a.* FROM groups a ORDER BY a.id ASC
			LIMIT $offset,$rows
			";
		}else{
			$sql = "SELECT a.* FROM groups a
			WHERE $filterParam ORDER BY a.id ASC LIMIT $offset,$rows 
			";
		}
		
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function data_role_level($id){
		$this->db->select('a.*,c.name as module_name,b.id as id_module_group');
		$this->db->from('groups a');
		$this->db->join('module_groups b','b.group_id=a.id');
		$this->db->join('module c','b.module_id=c.id');
		$this->db->where('b.group_id',$id);
		$data = $this->db->get();
		return $data->result_array();
	}
	
	function data_level_all(){
		$this->db->select('a.*');
		$this->db->from('groups a');
		$this->db->order_by('a.id','ASC');
		$data = $this->db->get();
		return $data->result_array();
	}
	
	function tambah_role_level($data){
		$this->db->insert('module_groups',$data);
	}
	
	function hapus_role_level($id){
		$this->db->where('id',$id);
		$this->db->delete('module_groups');
	}
	
	function tambah_level($data){
		$this->db->insert('groups',$data);
	}
	
	function ubah_level($id,$data){
		$this->db->where('id',$id);
		$this->db->update('groups',$data);
	}
	
	function hapus_level($id){
		$this->db->where('id',$id);
		$this->db->delete('groups');
	}
	
	
}	
?>