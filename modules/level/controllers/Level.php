<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("m_level","level");
        $this->load->model("module/m_module","module");
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$data['data_module'] = $this->module->data_module();
			$this->load->view('v_level',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function data_level(){
		
		//Filtering
		$filterParam	= null;
		if($this->input->post('filterRules')){
			if($this->input->post('filterRules') != '[]'){
				$filter			= json_decode($this->input->post('filterRules'));
				$filter_count	= count($filter);
				if($filter_count>1){
					for($x=0;$x<$filter_count;$x++){
						if($x == $filter_count-1){
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%"';
						}else{
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%" AND ';
						}
						$filterParam .= $temp;
					}
				}else{
					$filterParam = $filter[0]->field.' LIKE "%'.$filter[0]->value.'%"';
				}
			}
		}
		
		
		$page 			= $this->input->post('page') ? intval($this->input->post('page')) : 1;
		$rows 			= $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
		$dataRows 		= $this->level->data_level($page,$rows,$filterParam);
		$dataRowsAll 	= $this->level->data_level_all();
		$data["total"] 	= ''.count($dataRowsAll).'';
		$data["rows"] 	= $dataRows;
		
		// $data = $this->level->data_level($page,$rows,$filterParam);
		echo json_encode($data);
	}
	
	function data_role_level($id){
		$dataRowsAll 	= $this->level->data_role_level($id);
		echo json_encode($dataRowsAll);
	}
	
	function tambah_role_level($id){
		$module 		= $this->input->post('module');
		
		$data = array(
			'group_id'			=> $id,
			'module_id'			=> $module
		);
		
		$status = $this->level->tambah_role_level($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_role_level(){
		
		$id = $this->input->post('id');
		
		$status = $this->level->hapus_role_level($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function tambah_level(){
		$name 			= $this->input->post('name');
		$description 	= $this->input->post('description');
		
		$data = array(
			'name'				=> $name,
			'description'		=> $description
		);
		
		$status = $this->level->tambah_level($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function ubah_level($id){
		$name 			= $this->input->post('name');
		$description 	= $this->input->post('description');
		
		$data = array(
			'name'				=> $name,
			'description'		=> $description
		);
		
		$status = $this->level->ubah_level($id,$data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_level(){
		
		$id = $this->input->post('id');
		
		$status = $this->level->hapus_level($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
}
