<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Data Pengguna" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER DATA PENGGUNA" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'complete_name',width:80">Complete Name</th>
										<th data-options="field:'level',width:80">Level</th>
										<th data-options="field:'username',width:100">Username</th>
										<th data-options="field:'phone',width:100">Phone</th>
										<th data-options="field:'email',width:150">Email</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">New User</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit User</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="editPass()">Change Password</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove User</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:370px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">User Information</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>First Name:</label>
										<input name="first_name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Last Name:</label>
										<input name="last_name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Level:</label>
										<select name="level" class="easyui-combobox" required="true">
											<?php
											foreach($data_level as $row){
												echo '<option value="'.$row['id'].'">'.$row['description'].'</option>';
											}
											?>
										</select>
									</div>
									<div class="fitem">
										<label>Phone:</label>
										<input name="phone" class="easyui-textbox">
									</div>
									<div class="fitem">
										<label>Email:</label>
										<input name="email" class="easyui-textbox" validType="email" required="true">
									</div>
									<div class="fitem">
										<label>Username:</label>
										<input name="username" class="easyui-textbox" required="true">
									</div>
									<div class="fitem" id="idPass">
										<label>Password:</label>
										<input type="text" name="password" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<!-- EDIT -->
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:330px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">User Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>First Name:</label>
										<input name="first_name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Last Name:</label>
										<input name="last_name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Group:</label>
										<select name="level" class="easyui-combobox" required="true">
											<?php
											foreach($data_level as $row){
												echo '<option value="'.$row['id'].'">'.$row['description'].'</option>';
											}
											?>
										</select>
									</div>
									<div class="fitem">
										<label>Phone:</label>
										<input name="phone" class="easyui-textbox">
									</div>
									<div class="fitem">
										<label>Email:</label>
										<input name="email" class="easyui-textbox" validType="email" required="true">
									</div>
									<div class="fitem">
										<label>Username:</label>
										<input name="username" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<!-- CHANGE PASSWORD -->
							<div id="dlg3" class="easyui-dialog" style="width:400px;height:330px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">User Information</div>
								<form id="fm3" method="post" novalidate>
									<div class="fitem">
										<label>Old Password:</label>
										<input name="password_old" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>New Password:</label>
										<input name="password_new" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm3','dlg3')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg3').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>pengguna/data_pengguna',
                pagination: true,
                remoteFilter: true,
                rownumbers: true
            });
            dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Pengguna Baru');
			$('#fm').form('clear');
			url 	= '<?=base_url();?>pengguna/tambah_pengguna';
			aksi 	= 1;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Edit User');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>pengguna/ubah_pengguna/'+row.id;
				aksi 	= 2;
			}
		}
		function editPass(){
			var row = $('#dg').datagrid('getSelected');
			$('#fm3').form('clear');
			if (row){
				$('#dlg3').dialog('open').dialog('setTitle','Change Password');
				url = '<?=base_url();?>pengguna/ubah_password/'+row.username;
				aksi 	= 3;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					// console.log(result);
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Pengguna";
						}else{
							pesan = "Berhasil Ubah Pengguna";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this user?',function(r){
					if (r){
						$.post('<?=base_url();?>pengguna/hapus_pengguna/',{id:row.id},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: "Berhasil Hapus Pengguna"
								});
							} else {
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>