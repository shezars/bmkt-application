<?php
class M_pengguna extends CI_Model  {
	
	function data_pengguna($page,$rows,$filterParam){
		$offset = ($page-1)*$rows;
		
		if($filterParam == null){
			$sql = "SELECT a.*,c.description as level FROM users a 
			JOIN users_groups b ON b.user_id=a.id
			JOIN groups c ON b.group_id=c.id
			ORDER BY a.id ASC
			LIMIT $offset,$rows
			";
		}else{
			$sql = "SELECT a.*,c.description as level FROM users a 
			JOIN users_groups b ON b.user_id=a.id
			JOIN groups c ON b.group_id=c.id
			WHERE $filterParam LIMIT $offset,$rows ORDER BY a.id ASC
			";
		}
		
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	function data_pengguna_all(){
		$this->db->select('a.*,c.description as level');
		$this->db->from('users a');
		$this->db->join('users_groups b','b.user_id=a.id');
		$this->db->join('groups c','b.group_id=c.id');
		$this->db->order_by('a.id','ASC');
		$data = $this->db->get();
		return $data->result_array();
	}
	
	
}	
?>