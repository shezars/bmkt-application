<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("m_pengguna","pengguna");
        $this->load->model("level/m_level","level");
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$data['data_level'] = $this->level->data_level_all();	
			$this->load->view('v_pengguna',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function data_pengguna(){
		//Filtering
		$filterParam	= null;
		if($this->input->post('filterRules')){
			if($this->input->post('filterRules') != '[]'){
				$filter			= json_decode($this->input->post('filterRules'));
				$filter_count	= count($filter);
				if($filter_count>1){
					for($x=0;$x<$filter_count;$x++){
						if($x == $filter_count-1){
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%"';
						}else{
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%" AND ';
						}
						$filterParam .= $temp;
					}
				}else{
					$filterParam = $filter[0]->field.' LIKE "%'.$filter[0]->value.'%"';
				}
			}
		}
		
		
		$page 			= $this->input->post('page') ? intval($this->input->post('page')) : 1;
		$rows 			= $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
		$dataRows 		= $this->pengguna->data_pengguna($page,$rows,$filterParam);
		$dataRowsAll 	= $this->pengguna->data_pengguna_all();
		$data["total"] 	= ''.count($dataRowsAll).'';
		$data["rows"] 	= $dataRows;
		echo json_encode($data);
	}
	
	function tambah_pengguna(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email = $this->input->post('email');
		$additional_data = array(
								'first_name' => $this->input->post('first_name'),
								'last_name' => $this->input->post('last_name'),
								'complete_name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
								'phone' => $this->input->post('phone')
								);
		$group = array($this->input->post('level'));
		$status = $this->ion_auth->register($username, $password, $email, $additional_data, $group);
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function ubah_pengguna($id){
		$group = $this->input->post('level');
		
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'complete_name' => $this->input->post('first_name').' '.$this->input->post('last_name'),
			'phone' => $this->input->post('phone'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email')
		);
		
		$status = $this->ion_auth->update($id, $data);
		
		//remove from group
		$this->ion_auth->remove_from_group(false, $id);
		//add to new group
		$this->ion_auth->add_to_group($group, $id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function ubah_password($id){
		$password_old	= $this->input->post('password_old');
		$password_new	= $this->input->post('password_new');
		$status = $this->ion_auth->change_password($id,$password_old,$password_new);
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			$errors = $this->ion_auth->errors_array();
			echo json_encode(array('errorMsg'=>'Some errors occured: '.$errors));
		}
	}
	
	function hapus_pengguna(){
		$id = $this->input->post('id');
		$status = $this->ion_auth->delete_user($id);
		if($status >= 0){
			echo json_encode(array(
				'success' => true,
			));
		}else{
			$errors = $this->ion_auth->errors_array();
			echo json_encode(array('errorMsg'=>'Some errors occured: '.$errors));
		}
	}
	
}
