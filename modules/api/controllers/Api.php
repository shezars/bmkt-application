<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';


class Api extends REST_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("m_module","module");
    }
	
	public function index_get()
	{
		$master_item = $this->db->get('master_item')->result();
		$this->response($master_item, 200);
	}
	
	public function master_item_get()
	{
		$master_item = $this->db->get('master_item')->result();
		$master_item = array('result'=>$master_item);
		$this->response($master_item, 200);
	}
	
	public function master_item_get_where_put()
	{
		$barcode = $this->put('barcode');
//		$this->db->query("INSERT INTO module (name) VALUES('KODOK')");
		$master_item = $this->db->get_where('master_item',array("barcode"=>$barcode))->result();
		$master_item = array('result'=>$master_item);
		$this->response($master_item, 200);
	}
}
