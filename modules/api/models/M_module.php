<?php
class M_module extends CI_Model  {
	
	function data_module(){
		$this->db->select('*');
		$data = $this->db->get('module');
		return $data->result_array();
	}
	
	function tambah_module($data){
		$this->db->insert('module',$data);
	}
	
	function ubah_module($id,$data){
		$this->db->where('id',$id);
		$this->db->update('module',$data);
	}
	
	function hapus_module($id){
		$this->db->where('id',$id);
		$this->db->delete('module');
	}
	
	
}	
?>