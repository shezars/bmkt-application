<?php
class M_refferensi extends CI_Model  {
	
	function data_refferensi($page,$rows,$filterParam){
		$offset = ($page-1)*$rows;
		if($filterParam == null){
			$this->db->select('*');
		}else{
			$this->db->select('*');
			$this->db->where($filterParam);
		}
		$this->db->limit($rows,$offset);
		
		$data = $this->db->get('refferensi');
		return $data->result_array();
	}
	
	function data_refferensi_warehouse($page,$rows,$filterParam){
		$offset = ($page-1)*$rows;
		if($filterParam == null){
			$this->db->select('b.*,(SELECT COUNT(*) FROM refferensi a WHERE a.tag_name="storage_warehouse_rak" AND a.parent=b.id) as jumlah_rak');
		}else{
			$this->db->select('b.*,(SELECT COUNT(*) FROM refferensi a WHERE a.tag_name="storage_warehouse_rak" AND a.parent=b.id) as jumlah_rak');
			$this->db->where($filterParam);
		}
		$this->db->limit($rows,$offset);
		$this->db->where('b.tag_name','storage_warehouse');
		$data = $this->db->get('refferensi b');
		return $data->result_array();
	}
	
	function data_refferensi_rack($page,$rows,$filterParam,$id){
		$offset = ($page-1)*$rows;
		if($filterParam == null){
			$this->db->select('b.*,(SELECT COUNT(*) FROM refferensi a WHERE a.tag_name="storage_warehouse_box" AND a.parent=b.id) as jumlah_box');
		}else{
			$this->db->select('b.*,(SELECT COUNT(*) FROM refferensi a WHERE a.tag_name="storage_warehouse_box" AND a.parent=b.id) as jumlah_box');
			$this->db->where($filterParam);
		}
		$this->db->limit($rows,$offset);
		$this->db->where('b.tag_name','storage_warehouse_rak');
		if($id!=null){
			$this->db->where('b.parent',$id);
		}
		$data = $this->db->get('refferensi b');
		return $data->result_array();
	}
	
	function data_refferensi_box($page,$rows,$filterParam,$id){
		$offset = ($page-1)*$rows;
		if($filterParam == null){
			$this->db->select('a.*,(SELECT COUNT(*) FROM master_item b WHERE b.box_id=a.id AND b.status=0) as jumlah_barang');
		}else{
			$this->db->select('a.*,(SELECT COUNT(*) FROM master_item b WHERE b.box_id=a.id AND b.status=0) as jumlah_barang');
			$this->db->where($filterParam);
		}
		$this->db->limit($rows,$offset);
		$this->db->where('a.tag_name','storage_warehouse_box');
		if($id!=null){
			$this->db->where('a.parent',$id);
		}
		$data = $this->db->get('refferensi a');
		return $data->result_array();
	}
	
	function data_refferensi_all(){
		$this->db->select('*');
		$data = $this->db->get('refferensi');
		return $data->result_array();
	}
	
	function get_parent(){
		$this->db->select('*,CONCAT("(",tag_name,") ",value) as akhir');
		$data = $this->db->get('refferensi');
		return $data->result_array();
	}
	
	function tambah_refferensi($data){
		$this->db->insert('refferensi',$data);
	}
	
	function ubah_refferensi($id,$data){
		$this->db->where('id',$id);
		$this->db->update('refferensi',$data);
	}
	
	function hapus_refferensi($id){
		$this->db->where('id',$id);
		$this->db->delete('refferensi');
	}
	
	
}	
?>