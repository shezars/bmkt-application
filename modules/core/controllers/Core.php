<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("aktivitas/M_aktivitas","aktivitas");
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$data['dataWilayahTemuan']		= $this->aktivitas->data_lokasi_temuan_baru();
			$data['totalBarangTemuan']		= count($this->aktivitas->all_barang_master_temuan());
			$data['totalBarangPinjaman']	= count($this->aktivitas->all_barang_master_temuan(1));
			$data['totalLaporanPenemuan']	= count($this->aktivitas->data_master_laporan_temuan());
			$this->load->view('v_core',$data);
		}else{
			$this->load->view('v_login');
		}
	}
	
	function setSessionGrafikWilayah($id_wilayah=null){
		$getName = $this->aktivitas->getNamaWilayah_lokasiPenemuan($id_wilayah);
		$this->session->set_userdata('grafik_wilayah', $getName['wilayah']);
		$this->session->set_userdata('grafik_idWilayah', $id_wilayah);
	}
	
	function getGrafikTemuan($id_wilayah=null){
		if($this->session->userdata('grafik_idWilayah')){
			$id_wilayah = $this->session->userdata('grafik_idWilayah');
		}
		
		$temp['temuan'] = array();
		$temp2 = array();
		$result = $this->aktivitas->getGrafikTemuan($id_wilayah);
		foreach($result as $row){
			$data = array(
				'name'		=> $row['value'],
				'y'			=> (int)$row['qty'],
				'drilldown'	=> $row['id_category']
			);
			array_push($temp['temuan'],$data);
		}
		
		
		$temp['temuanDetail'] = $this->getGrafikTemuanDrilldown();
		
		echo json_encode($temp);
	}
	
	function getGrafikTemuanDrilldown(){
		$temp = array();
		$result = $this->aktivitas->getGrafikTemuanDrilldown();
		
		$temp2 = array();
		
		foreach($result as $index => $row){
			
			//Check Existing
			if(!empty($temp)){	
					
					if(in_array($row['id_category'],$temp2)){
						$key_string1 = array_keys($temp2, $row['id_category']);
						$temp3 = array(
							$row['value'],(int)$row['qty']
						);
						
						array_push($temp[$key_string1[0]]['data'],$temp3);
						
					}else{
						
						$data = array(
							'name'		=> 'Barang Temuan Detail '.$index,
							'id'		=> $row['id_category'],
							'data'		=> array(array($row['value'],(int)$row['qty'])
							)
						);
						array_push($temp,$data);
					}
				
			}else{
				$data = array(
					'name'		=> 'Barang Temuan Detail '.$index,
					'id'		=> $row['id_category'],
					'data'		=> array(array($row['value'],(int)$row['qty'])
					)
				);
				
				array_push($temp2,$row['id_category']);
				array_push($temp,$data);
			}
		
		}
		
		return $temp;
	}
	
	function do_login(){
		$username		= $this->input->post('username');
		$password		= $this->input->post('password');
		
		$remember = TRUE; // remember the user
		$status = $this->ion_auth->login($username, $password, $remember);
		if($status){
			$user = $this->ion_auth->user()->row();
			$group_id 	= $this->ion_auth->get_users_groups($user->id)->row()->id;
			$group_name = $this->ion_auth->get_users_groups($user->id)->row()->name;
			$this->session->set_userdata('id_level', $group_id);
			$this->session->set_userdata('level', $group_name);
			$this->session->set_userdata('isLogin', TRUE);
			echo 1;
		}else{
			echo 0;
		}
	}
	
	function getTemuanPeta(){
		echo json_encode($this->db->get('lokasi_penemuan')->result_array());
	}
	
	function getSummaryTemuanCategory(){
		$id = $this->input->post('id');
		$sql = "SELECT c.value,(SELECT COUNT(*) FROM master_item b WHERE b.category=a.category AND b.lifting_area=$id) as qty_category FROM master_item a JOIN refferensi c ON c.id=a.category WHERE a.lifting_area=$id GROUP BY a.category";
		$result = $this->db->query($sql)->result_array();
		$html = null;
		
		if(!empty($result)){
			foreach($result as $row){
				$html .= '<tr><td>'.$row['value'].'</td><td>'.$row['qty_category'].'</td></tr>';
			}
		}
		
		echo $html;
	}
	
	function getSummaryTemuanMaterial(){
		$id = $this->input->post('id');
		$sql = "SELECT c.value,(SELECT COUNT(*) FROM master_item b WHERE b.material=a.material AND b.lifting_area=$id) as qty_category FROM master_item a JOIN refferensi c ON c.id=a.material WHERE a.lifting_area=$id GROUP BY a.material";
		$result = $this->db->query($sql)->result_array();
		$html = null;
		
		if(!empty($result)){
			foreach($result as $row){
				$html .= '<tr><td>'.$row['value'].'</td><td>'.$row['qty_category'].'</td></tr>';
			}
		}
		
		echo $html;
	}
	
	function do_logout(){
		session_destroy();
		redirect(base_url());
	}
	
	function hit(){
		$id		= $this->input->post('id');
		$hit	= $this->input->post('hit');
		$data = array(
			'hit'	=> $hit
		);
		$this->db->where('id',$id);
		$this->db->update('module',$data);
	}
	
}
