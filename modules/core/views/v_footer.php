<div data-options="region:'south',border:false" style="height:40px;padding:10px;">Copyright &copy; Direktorat Jasa Kelautan, Ditjen PRL, Kementerian Kelautan dan Perikanan</div>
<script>
	
	$("#idUser").click(function(){
			$('#idUserProfile').dialog({
				title: 'User Profile',
				width: 400,
				height: 200,
				closed: false,
				cache: false,
				// href: 'get_content.php',
				modal: true
			});
		})
		
	$("#idLogout").click(function(){
		$('#idConfirmLogout').dialog({
			title: 'Konfirmasi Logout',
			width: 400,
			height: 200,
			closed: false,
			cache: false,
			// href: 'get_content.php',
			modal: true
		});
	})
	
	function ya(){
		// $.get("<?=base_url();?>core/do_logout");
		// window.location = "<?=base_url();?>";
		window.location = "<?=base_url();?>core/do_logout";
	}
	
	$(".imageLeftButton").click(function(){
		var id			= this.id;
		var joinIDHit 	= id.split("-");
		var newHit		= parseInt(joinIDHit[1])+1;
		$.post('<?=base_url();?>core/hit',{id:id,hit:newHit});
	})
</script>