<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>BMKT</title>
	<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/jquery-easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/bxslider/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/jquery-easyui/themes/icon.css">
	<!-- <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/jquery-easyui/demo.css"> -->
	<script type="text/javascript" src="<?=base_url();?>assets/jquery-easyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/jquery-easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/bxslider/jquery.bxslider.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyA-HbVe5SO626kFFK9PdhMnT4pQhF-SqZk"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	
	<!-- HIGHCHART JS -->
	<script src="<?=base_url();?>assets/highcharts/code/highcharts.js"></script>
	<script src="<?=base_url();?>assets/highcharts/code/modules/series-label.js"></script>
	<script src="<?=base_url();?>assets/highcharts/code/modules/exporting.js"></script>
	<style>
	.panel-htop{
		z-index: 9999 !important;
	}
	.kodok td {
		padding: 10px;
	}
	</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false" class="header-logo" style="height:120px;padding:10px;">
		<div class="header-logo-area">
			<img src="<?=base_url();?>assets/images/kkp-90.png">
			<?php
			$CI =& get_instance();
			$id_level 	= $this->session->userdata('id_level');
			$sql 		= "SELECT value as alamat_1,(SELECT value FROM refferensi WHERE tag_name='alamat_2') as alamat_2, 
						   (SELECT value FROM refferensi WHERE tag_name='alamat_3') as alamat_3 ,
						   (SELECT value FROM refferensi WHERE tag_name='alamat_4') as alamat_4 ,
						   (SELECT value FROM refferensi WHERE tag_name='alamat_5') as alamat_5 
						   FROM refferensi WHERE tag_name='alamat_1'";
			$data 		= $CI->db->query($sql)->result_array();
			?>
			<div>
				<h2>INDONESIA MARINE HERITAGE</h2>
				<h4 style="margin-top:0px;margin-bottom:0px;"><?=$data[0]['alamat_1'];?><br/><?=$data[0]['alamat_2'];?><br/><?=$data[0]['alamat_3'];?></h4>
				<h4 style="margin-top:0px;margin-bottom:0px;"><?=$data[0]['alamat_4'];?></h4>
				<h4 style="margin-top:0px;margin-bottom:0px;"><?=$data[0]['alamat_5'];?></h4>
			</div>
		</div>
		<div class="header-logo-area2">
			<a href="#" id="idEn_english" class="easyui-linkbutton" data-options="iconCls:'icon-united-states',size:'large'">&nbsp;</a>
			<a href="#" id="idEn_indonesia" class="easyui-linkbutton" data-options="iconCls:'icon-indonesia',size:'large'">&nbsp;</a>
			<a href="http://bmkt-kkp.com/gallery" target="_BLANK" class="easyui-linkbutton" data-options="iconCls:'icon-gallery',size:'large'">Marine Heritage Galeri &nbsp;</a>
			<a href="#" id="idLaporanPenemuan" class="easyui-linkbutton" data-options="iconCls:'icon-signing',size:'large'">Laporan Penemuan &nbsp;</a>
			<a href="#" id="idLogin" class="easyui-linkbutton" data-options="iconCls:'icon-login-2',size:'large'">Login &nbsp;</a>
		</div>
	</div>
	
	<div data-options="region:'west',title:' '" style="width:50%;">
	
	<div id="canvasPeta" style="width:100%;height:100%;">&nbsp;</div>
	
	</div>
	
	<div id="idFormLogin" style="max-height:200px !important">
		<div style="padding:20px">
			<form id="ff" method="post" action="<?=base_url();?>core/do_login">
				<div style="margin-bottom:20px">
					<input class="easyui-textbox" name="username" style="width:90%" data-options="label:'Username:',required:true">
				</div>
				<div style="margin-bottom:20px">
					<input class="easyui-textbox" type="password" name="password" style="width:90%" data-options="label:'Password:',required:true">
				</div>
			</form>
			<div style="text-align:center;padding:5px 0">
				<button class="easyui-linkbutton" onclick="submitForm()" style="width:80px">Submit</button>
				<button class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</button>
			</div>			
		</div>
	</div>
	
	<div id="idFormLaporanPenemuan" style="max-height:550px !important;">
		<div style="padding:20px">
			<form id="ff2" method="post" action="<?=base_url();?>aktivitas/laporan_temuan_baru_add" enctype="multipart/form-data">
				<div style="margin-bottom:20px">
					<input class="easyui-textbox" name="name" style="width:90%" data-options="label:'Nama:',required:true">
				</div>
				<div style="margin-bottom:20px">
					<input class="easyui-textbox" name="alamat" style="width:90%" data-options="label:'Alamat:',required:true">
				</div>
				<div style="margin-bottom:20px">
					<input class="easyui-textbox" name="no_hp" style="width:90%" data-options="label:'No.Telepon',required:true">
				</div>
				<div style="margin-bottom:20px">
					<input class="easyui-textbox" name="email" style="width:90%" data-options="label:'E-Mail',required:true">
				</div>
				<div style="margin-bottom:20px;">
					<input class="easyui-textbox" name="lokasi" style="width:90%" data-options="label:'Lokasi',required:true">
				</div>
				<div style="margin-bottom:20px;padding-top:10px;">
					<input class="easyui-file" type="file" name="userfile" style="width:90%" data-options="label:'Lampiran',required:true">
					<br/><font color="#blue;">*WAJIB unggah lampiran yg dapat di unduh disini</font> (<a href="<?=base_url();?>assets/Form Laporan Penemuan BMKT.docx" target="_BLANK">Unduh Format Lampiran</a>)
				</div>
				<div style="margin-bottom:20px;padding-top:15px;">
					<div class="g-recaptcha" data-sitekey="6Lc6dE4UAAAAAGaWBrY7Zg8JPxZMJrX3diEB7Zh2"></div>
				</div>
			</form>
			<div style="text-align:center;padding:5px 0">
				<button class="easyui-linkbutton" onclick="submitForm2()" style="width:80px">Submit</button>
				<button class="easyui-linkbutton" onclick="clearForm2()" style="width:80px">Clear</button>
			</div>			
		</div>
	</div>

	<div data-options="region:'center',title:'&nbsp;'" style="overflow: hidden;">
		<div id="idDetailMap" style="padding:30px;">
			<h2 style="text-decoration: underline;float:left;">DETAIL TEMUAN</h2>
			<button style="float:right;background: none;" id="btnCloseDetailTemuan">CLOSE</button>
			<table width="100%">
				<tr>
					<td width="15%">Wilayah</td>
					<td width="1%">:</td>
					<td><div id="txtWilayah">.....</div></td>
				</tr>
				<tr>
					<td>Longitude</td>
					<td>:</td>
					<td><div id="txtLong">.....</div></td>
				</tr>
				<tr>
					<td>Latitude</td>
					<td>:</td>
					<td><div id="txtLat">.....</div></td>
				</tr>
				<tr>
					<td>Geografis</td>
					<td>:</td>
					<td><div id="txtGeog">.....</div></td>
				</tr>
				<tr>
					<td>Kedalaman</td>
					<td>:</td>
					<td><div id="txtDepth">.....</div></td>
				</tr>
				<tr>
					<td>Penemuan</td>
					<td>:</td>
					<td><div id="txtPenemuan">.....</div></td>
				</tr>
				<tr>
					<td>Dinasty / Periode</td>
					<td>:</td>
					<td><div id="txtDinper">.....</div></td>
				</tr>
			</table>
			<br/>
			
			<table border="1" width="50%" style="border-collapse:collapse;float:left;" class="kodok">
				<thead style="background-color:#F3F3F3;">
					<tr>
						<td>Category</td>
						<td>Qty Category</td>
					</tr>
				</thead>
				<tbody id="tbodyCategory">
					<tr>
						<td>........................</td>
						<td>........................</td>
					</tr>
				</tbody>
			</table>
			
			<table border="1" width="50%" style="border-collapse:collapse;" class="kodok">
				<thead style="background-color:#F3F3F3;">
					<tr>
						<td>Material</td>
						<td>Qty Material</td>
					</tr>
				</thead>
				<tbody id="tbodyMaterial">
					<tr>
						<td>........................</td>
						<td>........................</td>
					</tr>
				</tbody>
			</table>
			
		</div>
		<div id="idSlider">
			<ul class="bxslider">
			  <?php
			  $CI =& get_instance();
			  $CI->load->database();
			  $query="SELECT * FROM slider";
			  $slider = $CI->db->query($query)->result_array();
			  if(!empty($slider)){
				foreach($slider as $row){
				?>
					<li style="width:200px;"><img src="<?=base_url();?>uploads/slider/<?=$row['url'];?>" /></li>
				<?php
				}  
			  }else{
				echo '<li style="width:200px;"><img src="<?=base_url();?>assets/images/slider/slider2.jpeg" /></li>';
			  }
			  ?>
			</ul>
		</div>
	</div>


	<div data-options="region:'south',border:false" style="height:35px;padding:10px;">Copyright &copy; Direktorat Jasa Kelautan, Ditjen PRL, Kementerian Kelautan dan Perikanan</div>
	
	
	<script>
		
		$("#idDetailMap").hide();
		
		$("#btnCloseDetailTemuan").click(function(){
			$("#idSlider").show();
			$("#idDetailMap").hide();
		});
		
		$("#idLogin").click(function(){
			$('#idFormLogin').dialog({
				title: 'Login',
				width: 400,
				height: 270,
				closed: false,
				cache: false,
				// href: 'get_content.php',
				modal: true
			});
		})
		
		$("#idLaporanPenemuan").click(function(){
			$('#idFormLaporanPenemuan').dialog({
				title: 'Laporan Penemuan',
				width: 500,
				height: 520,
				closed: false,
				cache: false,
				// href: 'get_content.php',
				modal: true
			});
		})
		
		function ya(){
			$.get("<?=base_url();?>core/do_logout");
			location.reload();
		}
		
		function submitForm(){
			$('#ff').form({
				url:'<?=base_url();?>core/do_login',
				onSubmit: function(){
					// do some check
					// return false to prevent submit;
				},
				success:function(data){
					if(data==1){
						alert("Login Berhasil");
						location.reload();
					}else{
						alert("Username atau Password Tidak Sesuai");
					}
				}
			});
			$('#ff').submit();
		}
		function clearForm(){
			$('#ff').form('clear');
		}
		
		function submitForm2(){
			$('#ff2').form({
				url:'<?=base_url();?>aktivitas/laporan_temuan_baru_add',
				onSubmit: function(){
					// do some check
					// return false to prevent submit;
				},
				success:function(result){
					var result = eval('('+result+')');
					
					if (result.status == 0){
						$.messager.show({
							title: 'Notifikasi',
							msg: result.messages
						});
					} else {
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: "Berhasil Kirim Laporan Penemuan"
								});
								
						setTimeout(function(){ location.reload(); }, 2000);
					}
				}
			});
			$('#ff2').submit();
		}
		function clearForm2(){
			$('#ff2').form('clear');
		}
		
		var customLabel = {
			kapal: {
			  label: 'T'
			},
			bar: {
			  label: 'B'
			}
		  };
		var peta;
		// var koorAwal = new google.maps.LatLng(-1.0344303,111.3719731);
		var koorAwal = new google.maps.LatLng(-2.3933468, 112.858076);
		var settingpeta = {
							zoom: 5,
							// zoom: 12,
							center: koorAwal,
							mapTypeId: google.maps.MapTypeId.ROADMAP 
							};
		peta = new google.maps.Map(document.getElementById("canvasPeta"),settingpeta);
		
		var infoWindow = new google.maps.InfoWindow;
		
		// Change this depending on the name of your PHP or XML file
          downloadUrl('<?=base_url();?>core/getTemuanPeta', function(data) {
			  
			var obj = jQuery.parseJSON(data.responseText);
			for(x=0;x<obj.length;x++){
				  var id 		= obj[x]['id'];
				  var name 		= obj[x]['nama'];
				  var longi 	= obj[x]['long'];
				  var lati 		= obj[x]['lat'];
				  var geog 		= obj[x]['lokasi_geografis'];
				  var depth 	= obj[x]['lokasi_kedalaman'];
				  var penem 	= obj[x]['penemuan'];
				  var dinper 	= obj[x]['dinasti_periode'];
				  var address = obj[x]['wilayah'];
				  var type = 'kapal';
				  var point = new google.maps.LatLng(
					  parseFloat(obj[x]['lat']),
					  parseFloat(obj[x]['long'])
					);
				  var infowincontent = document.createElement('div');
				  var strong = document.createElement('strong');
				  strong.textContent = name
				  infowincontent.appendChild(strong);
				  infowincontent.appendChild(document.createElement('br'));

				  var text = document.createElement('text');
				  text.textContent = address
				  // console.log(text);
				  infowincontent.appendChild(text);
				  var icon = customLabel[type] || {};
				  
				  var marker = new google.maps.Marker({
					map: peta,
					position: point,
					label: icon.label
				  });
				  var infowindow = new google.maps.InfoWindow()
				  
				  google.maps.event.addListener(marker,'click', (function(marker,infowincontent,infowindow,address,longi,lati,geog,depth,penem,dinper,id){ 
					return function() {
						
					   $("#idSlider").hide();
					   $("#idDetailMap").show();	
					   
					   $("#txtWilayah").html(address);
					   $("#txtLong").html(longi);
					   $("#txtLat").html(lati);
					   $("#txtGeog").html(geog);
					   $("#txtDepth").html(depth);
					   $("#txtPenemuan").html(penem);
					   $("#txtDinper").html(dinper);
					   
					   //Category
					   $.post("<?=base_url();?>core/getSummaryTemuanCategory",{id:id}).success(function(data){
						   $("#tbodyCategory").html(data);
					   })
					   
					   //Material
					   $.post("<?=base_url();?>core/getSummaryTemuanMaterial",{id:id}).success(function(data){
						   $("#tbodyMaterial").html(data);
					   })
					   
					   infowindow.setContent(infowincontent);
					   infowindow.open(peta,marker);
					};
				  })(marker,infowincontent,infowindow,address,longi,lati,geog,depth,penem,dinper,id)); 
				  
				  // marker.addListener('click', function() {
					// $("#txtWilayah").html("INDONESIA");
					// $(".bxslider").hide();
					// console.log(infowincontent);
					// infoWindow.setContent(infowincontent);
					// infoWindow.open(peta, marker);
				  // });
			}
			
          });
		  
		  function downloadUrl(url, callback) {
				var request = window.ActiveXObject ?
					new ActiveXObject('Microsoft.XMLHTTP') :
					new XMLHttpRequest;

				request.onreadystatechange = function() {
				  if (request.readyState == 4) {
					request.onreadystatechange = doNothing;
					callback(request, request.status);
				  }
				};

				request.open('GET', url, true);
				request.send(null);
			  }
			  
			  function doNothing() {}
		
	</script>
	<script>
		$(document).ready(function(){
			$('.bxslider').bxSlider({
				mode: 'fade',
				captions: true,
				// slideWidth: 600,
				auto: true,
				autoControls: true,
				stopAutoOnClick: true,
				pager: true
			});
		});
	</script>

</body>
</html>