<?php
	$this->load->view('v_header');
?>
	
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Dashboard" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="container" style="padding-left:0px;">
				<div class="row" style="padding-left:0px;">
					<div class="col-md-3 col-xs-12">
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-ship fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge"><?=$totalBarangTemuan;?></div>
												<div>Total Barang Temuan</div>
											</div>
										</div>
									</div>
									<a href="<?=base_url();?>aktivitas/master_temuan">
										<div class="panel-footer">
											<span class="pull-left">View Details</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-green">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-truck fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge"><?=$totalBarangPinjaman;?></div>
												<div>Total Barang Dipinjamkan</div>
											</div>
										</div>
									</div>
									<a href="<?=base_url();?>aktivitas/pemindahan">
										<div class="panel-footer">
											<span class="pull-left">View Details</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-red">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
												<i class="fa fa-envelope fa-5x"></i>
											</div>
											<div class="col-xs-9 text-right">
												<div class="huge"><?=$totalLaporanPenemuan;?></div>
												<div>Laporan Temuan</div>
											</div>
										</div>
									</div>
									<a href="<?=base_url();?>aktivitas/master_laporan_temuan">
										<div class="panel-footer">
											<span class="pull-left">View Details</span>
											<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
											<div class="clearfix"></div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-9 col-xs-12" style="text-align:center;">
						<div id="grafik_temp"></div>
						<select id="wilayahTemuan">
							<option>--pilih wilayah--</option>
							<?php
							foreach($dataWilayahTemuan as $row){
								?>
								<option value="<?=$row['id'];?>"><?=$row['wilayah'];?></option>
								<?php
							}
							?>
						</select>
					</div>
					
					
				</div>
				</div>
			</div>
		</div>
	</div>
	
<?php
	$this->load->view('v_footer');
?>	

	<script type="text/javascript">
	
		grafik();
		
		$("#wilayahTemuan").change(function(){
			var id = this.value;
			$.get("<?=base_url();?>core/setSessionGrafikWilayah/"+id).success(function(){
				location.reload();
			})
		})	
		
		function grafik(){
			$.getJSON( "<?=base_url();?>core/getGrafikTemuan", function(data) {
			// console.log(contoh2);
			// console.log(data['temuanDetail']);
			// var contohlagi = [{"name":"bottle","y":1,"drilldown":"46"},{"name":"bottle2","y":2,"drilldown":"47"}];		
			
					// Create the chart
					Highcharts.chart('grafik_temp', {
						chart: {
							type: 'column'
						},
						title: {
							text: 'Grafik Temuan BMKT Wilayah <?=$this->session->userdata('grafik_wilayah');?>'
						},
						subtitle: {
							text: 'Klik Kategori Untuk Melihat SubKategori'
						},
						xAxis: {
							type: 'category'
						},
						yAxis: {
							title: {
								text: 'QTY'
							}

						},
						legend: {
							enabled: false
						},
						plotOptions: {
							series: {
								borderWidth: 0,
								dataLabels: {
									enabled: true,
									// format: '{point.y:.1f}'
								}
							}
						},

						tooltip: {
							headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
							pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
						},

						"series": [
							{
								"name": "Barang Temuan",
								"colorByPoint": true,
								"data": data['temuan']
							}
						],
						"drilldown": {
							"series": 
								data['temuanDetail']	
						}
					});
			});
		}
		
	</script>
</body>
</html>