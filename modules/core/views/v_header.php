<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>BMKT</title>
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
	<link href="<?=base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url();?>assets/css/sb-admin-2.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/jquery-easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/jquery-easyui/themes/icon.css">
	<!-- <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/jquery-easyui/demo.css"> -->
	<script type="text/javascript" src="<?=base_url();?>assets/jquery-easyui/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>assets/jquery-easyui/jquery.easyui.min.js"></script>
	
	<!-- HIGHCHART JS -->
	<!--
	<script src="<?=base_url();?>assets/highcharts/code/highcharts.js"></script>
	<script src="<?=base_url();?>assets/highcharts/code/modules/series-label.js"></script>
	-->
	
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="<?=base_url();?>assets/highcharts/code/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/drilldown.js"></script>
	
	<?php
	if($this->uri->segment(1)=='pengguna' || $this->uri->segment(1)=='level' || $this->uri->segment(1)=='module' || $this->uri->segment(1)=='refferensi' || $this->uri->segment(1)=='aktivitas' || $this->uri->segment(1)=='storage'){
		echo '<script type="text/javascript" src="'.base_url().'assets/jquery-easyui/plugins/datagrid-filter/datagrid-filter.js"></script>';
		echo '<script type="text/javascript" src="'.base_url().'assets/jquery-easyui/datagrid-detailview.js"></script>';
	}
	if($this->uri->segment(1)=='aktivitas'){
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/lightbox2/dist/css/lightbox.min.css">';
		echo '<script type="text/javascript" src="'.base_url().'assets/lightbox2/dist/js/lightbox.min.js"></script>';
		
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/fileuploader/jquery.fileuploader.css">';
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/fileuploader/jquery.fileuploader-theme-thumbnails.css">';
		echo '<script type="text/javascript" src="'.base_url().'assets/fileuploader/jquery.fileuploader.js"></script>';
		echo '<script type="text/javascript" src="'.base_url().'assets/tinymce/js/tinymce/tinymce.min.js"></script>';
		
		//Fineuploader
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/fine-uploader/fine-uploader-new.css">';
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/fine-uploader/fine-uploader-gallery.min.css">';
		echo '<script src="'.base_url().'assets/fine-uploader/fine-uploader.js"></script>';
	}
	?>
	<script type="text/template" id="qq-template-gallery">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Upload a file</div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <div class="qq-thumbnail-wrapper">
                        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                    </div>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                        <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                        Retry
                    </button>

                    <div class="qq-file-info">
                        <div class="qq-file-name">
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                        </div>
                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                            <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                            <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                            <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                        </button>
                    </div>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
	<style>
        #trigger-upload {
            color: white;
            background-color: #00ABC7;
            font-size: 14px;
            padding: 7px 20px;
            background-image: none;
        }

        #fine-uploader-manual-trigger .qq-upload-button {
            margin-right: 15px;
        }

        #fine-uploader-manual-trigger .buttons {
            width: 36%;
        }

        #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
            width: 60%;
        }
    </style>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		.imageLeftButton1{
			padding-left:5px;
		}
	</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false" class="header-logo" style="height:130px;padding:10px">
		<div class="header-logo-area">
			<img src="<?=base_url();?>assets/images/kkp-90.png">
			<?php
			if($this->session->userdata('isLogin')){
				$CI =& get_instance();
				$id_level 	= $this->session->userdata('id_level');
				$sql 		= "SELECT value as alamat_1,(SELECT value FROM refferensi WHERE tag_name='alamat_2') as alamat_2, 
							   (SELECT value FROM refferensi WHERE tag_name='alamat_3') as alamat_3 ,
							   (SELECT value FROM refferensi WHERE tag_name='alamat_4') as alamat_4 ,
							   (SELECT value FROM refferensi WHERE tag_name='alamat_5') as alamat_5 
							   FROM refferensi WHERE tag_name='alamat_1'";
				$data 		= $CI->db->query($sql)->result_array();
				?>
				<div>
					<h3 style="margin-top:0px;margin-bottom:0px;">INDONESIA MARINE HERITAGE</h3>
					<h5 style="margin-top:0px;margin-bottom:0px;"><?=$data[0]['alamat_1'];?><br/><?=$data[0]['alamat_2'];?><br/><?=$data[0]['alamat_3'];?></h5>
					<h5 style="margin-top:0px;margin-bottom:0px;"><?=$data[0]['alamat_4'];?></h5>
					<h5 style="margin-top:0px;margin-bottom:0px;"><?=$data[0]['alamat_5'];?></h5>
				</div>
				<?php
			}else{
				?>
				<div>
					<h2>INDONESIA MARINE HERITAGE</h2>
					<h4>Gedung Mina Bahari I, Jalan Medan Merdeka Timur No.16<br/>RT.7/RW.1 Gambir, Kota Jakarta Pusat <br/>Daerah Khusus Ibukota Jakarta 10110</h4>
					<h4>Telp. (021) 3519070</h4>
				</div>
				<?php
			}
			?>
		</div>
		<div class="header-logo-area2">
			<a href="#" id="idUser" class="easyui-linkbutton" data-options="iconCls:'icon-boy-large',size:'large'">&nbsp;</a>
			<a href="#" id="idLogout" class="easyui-linkbutton" data-options="iconCls:'icon-exit-large',size:'large'">&nbsp;</a>
		</div>
	</div>
	<div data-options="region:'west',split:true,title:'Navigation'" style="width:250px;">
		<div class="easyui-accordion" style="width:100%;height:350px;" data-options="selected:1">
			<?php
				$CI =& get_instance();
				$id_level 	= $this->session->userdata('id_level');
				$sql 		= "SELECT a.* FROM module a INNER JOIN module_groups b ON b.module_id=a.id WHERE b.group_id=$id_level order by a.hit desc limit 4";
				$data 		= $CI->db->query($sql)->result_array();
				$temp_4 	= null;
				foreach($data as $row){
					$url 			= $row['url'];
					$url 			= base_url().$row['url'];
					$icon 			= $row['icon'];
					$icon_image 	= $row['icon_image'];
					$url_image 		= base_url().'assets/jquery-easyui/themes/icons/'.$icon_image;
					$name 			= $row['name'];
					$joinIDHIT		= $row['id'].'-'.$row['hit'];;
					
					$temp_4 .= '<img class="imageLeftButton1" src="'.$url_image.'"><a href="'.$url.'" id="'.$joinIDHIT.'" class="easyui-linkbutton" data-options="plain:true">'.$name.'</a><br/>';
				}
			?>
			<div title="General" data-options="iconCls:'icon-general-1'" style="overflow:auto;padding:10px;">
				<a href="<?=base_url();?>" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-speedometer'">Dashboard</a><br/>
				<?= $temp_4; ?>
				<!--
				<a href="<?=base_url();?>" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-speedometer'">Dashboard</a><br/>
				<a href="<?=base_url();?>aktivitas/data_baru" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-box3'">Data Baru</a><br/>
				<a href="<?=base_url();?>aktivitas/pemindahan" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-box4'">Pemindahan</a><br/>
				<a href="<?=base_url();?>aktivitas/master_temuan" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-warehouse'">Master Temuan</a>
				<a href="<?=base_url();?>aktivitas/lokasi_temuan_baru" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-placeholder'">Lokasi Temuan Baru</a><br/>
				<a href="<?=base_url();?>aktivitas/master_laporan_temuan" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-folder'">Master Laporan Temuan</a>
				-->
			</div>
			
			<?php
				$sql2 		= "SELECT a.* FROM module a INNER JOIN module_groups b ON b.module_id=a.id WHERE b.group_id=$id_level";
				$data2 		= $CI->db->query($sql2)->result_array();
				$temp_1 	= null;
				$temp_2 	= null;
				$temp_3 	= null;
				
				foreach($data2 as $row){
					$url 			= $row['url'];
					$url 			= base_url().$row['url'];
					$icon 			= $row['icon'];
					$icon_image 	= $row['icon_image'];
					$url_image 		= base_url().'assets/jquery-easyui/themes/icons/'.$icon_image;
					$name 			= $row['name'];
					$joinIDHIT		= $row['id'].'-'.$row['hit'];;
					
					if($row['type'] == '1'){
						//AKTIVITAS
						$temp_1 .= '<img class="imageLeftButton1" src="'.$url_image.'"><a href="'.$url.'" id="'.$joinIDHIT.'" class="easyui-linkbutton imageLeftButton" data-options="plain:true">'.$name.'</a><br/>';
					}else if($row['type'] == '3'){
						//PENGATURAN
						$temp_3 .= '<img class="imageLeftButton1" src="'.$url_image.'"><a href="'.$url.'" id="'.$joinIDHIT.'" class="easyui-linkbutton imageLeftButton" data-options="plain:true">'.$name.'</a><br/>';
					}else{
						//LAPORAN
						
					}
				}
				
			?>
			
			<?php
			//LAPORAN
			if($temp_1 != null){
				?>
				<div title="Aktivitas" data-options="iconCls:'icon-box'" style="padding:10px;">
					<?= $temp_1 ;?>
					<!--
					<a href="<?=base_url();?>aktivitas/data_baru" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-box3'">Data Baru</a><br/>
					<a href="<?=base_url();?>aktivitas/import_data_baru" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-excel'">Import Data Baru</a><br/>
					<a href="<?=base_url();?>aktivitas/pemindahan" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-box4'">Pemindahan</a><br/>
					<a href="<?=base_url();?>aktivitas/lokasi_temuan_baru" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-placeholder'">Lokasi Temuan Baru</a><br/>
					<a href="<?=base_url();?>aktivitas/master_temuan" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-warehouse'">Master Temuan</a><br/>
					<a href="<?=base_url();?>storage" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-stock'">Storage</a><br/>
					<a href="<?=base_url();?>aktivitas/master_laporan_temuan" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-folder'">Master Laporan Temuan</a>
					-->
				</div>
				<?php
			}
			?>
			
			<?php
			//LAPORAN
			if($temp_2 != null){
				?>
				<div title="Laporan" data-options="iconCls:'icon-clipboard2'" style="padding:10px;">
					<!--
					<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-notepad'">Laporan Barang Masuk</a>
					<a href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-notepad'">Laporan Barang Keluar</a>
					-->
				</div>
				<?php
			}
			?>
			
			<?php
			//PENGATURAN
			if($temp_3 != null){
				?>
				<div title="Pengaturan" data-options="iconCls:'icon-push-pin'" style="padding:10px;">
					<?= $temp_3 ;?>
					<!--
					<a href="<?=base_url();?>pengguna" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-team'">Pengguna</a><br/>
					<a href="<?=base_url();?>refferensi" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-archive'">Referensi</a><br/>
					<a href="<?=base_url();?>module" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-office-material'">Module</a><br/>
					<a href="<?=base_url();?>level" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-users-level'">Level</a><br/>
					<a href="<?=base_url();?>slider" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-switch'">Slider</a>
					-->
				</div>
				<?php
			}
			?>
		</div>
	</div>
	
	<div id="idConfirmLogout">
		<div style="padding:10px;" align="center"><h4>Apakah anda yakin keluar dari sistem?</h4><br/>
			<a href="#" class="easyui-linkbutton c1" onclick="ya()" style="width:120px">Ya</a>
			<a href="#" class="easyui-linkbutton c5" onclick="tidak()" style="width:120px">Tidak</a>
		</div>
	</div>
	
	<div id="idUserProfile"><div style="padding:10px;">USER PROFILE HERE</div></div>