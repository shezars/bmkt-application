<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("m_module","module");
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$this->load->view('v_module');
		}else{
			redirect(base_url());
		}
	}
	
	function data_module(){
		$data = $this->module->data_module();
		echo json_encode($data);
	}
	
	function tambah_module(){
		$name 			= $this->input->post('name');
		$description 	= $this->input->post('description');
		$url 			= $this->input->post('url');
		$position 		= $this->input->post('position');
		
		$data = array(
			'name'				=> $name,
			'description'		=> $description,
			'url'				=> $url,
			'position'			=> $position,
		);
		
		$status = $this->module->tambah_module($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function ubah_module($id){
		$name 			= $this->input->post('name');
		$description 	= $this->input->post('description');
		$url 			= $this->input->post('url');
		$position 		= $this->input->post('position');
		
		$data = array(
			'name'				=> $name,
			'description'		=> $description,
			'url'				=> $url,
			'position'			=> $position,
		);
		
		$status = $this->module->ubah_module($id,$data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_module(){
		
		$id = $this->input->post('id');
		
		$status = $this->module->hapus_module($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
}
