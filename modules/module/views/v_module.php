<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Data Module" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER DATA MODULE" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'name',width:80">Name</th>
										<th data-options="field:'description',width:80">Description</th>
										<th data-options="field:'url',width:100">URL</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">New Module</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit Module</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Module</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:270px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Module Information</div>
								<form id="fm" method="post" novalidate>
									<div class="fitem">
										<label>Name:</label>
										<input name="name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Description:</label>
										<input name="description" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>URL:</label>
										<input name="url" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Position:</label>
										<input type="number" name="position" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:330px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">Module Information</div>
								<form id="fm2" method="post" novalidate>
									<div class="fitem">
										<label>Name:</label>
										<input name="name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Description:</label>
										<input name="description" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>URL:</label>
										<input name="url" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Position:</label>
										<input type="number" name="position" class="easyui-textbox" required="true">
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>module/data_module',
                pagination: true,
                remoteFilter: true,
                rownumbers: true
            });
            dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Module Baru');
			$('#fm').form('clear');
			url = '<?=base_url();?>module/tambah_module';
			aksi 	= 1;
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Edit User');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>module/ubah_module/'+row.id;
				aksi 	= 1;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Module";
						}else{
							pesan = "Berhasil Ubah Module";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this module?',function(r){
					if (r){
						$.post('<?=base_url();?>module/hapus_module',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Module'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>