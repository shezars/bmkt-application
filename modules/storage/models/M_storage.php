<?php
class M_storage extends CI_Model  {
	
	function data_refferensi($page,$rows,$filterParam){
		$offset = ($page-1)*$rows;
		if($filterParam == null){
			$this->db->select('*');
		}else{
			$this->db->select('*');
			$this->db->where($filterParam);
		}
		$this->db->limit($rows,$offset);
		
		$data = $this->db->get('refferensi');
		return $data->result_array();
	}
	
	function data_refferensi_all(){
		$this->db->select('*');
		$data = $this->db->get('refferensi');
		return $data->result_array();
	}
	
	function get_parent(){
		$this->db->select('*,CONCAT("(",tag_name,") ",value) as akhir');
		$data = $this->db->get('refferensi');
		return $data->result_array();
	}
	
	function tambah_refferensi($data){
		$this->db->insert('refferensi',$data);
	}
	
	function ubah_refferensi($id,$data){
		$this->db->where('id',$id);
		$this->db->update('refferensi',$data);
	}
	
	function hapus_refferensi($id){
		$this->db->where('id',$id);
		$this->db->delete('refferensi');
	}
	
	
}	
?>