<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Data Storage" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
					
							<table id="dg" toolbar="#toolbar"></table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">Tambah Storage</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Ubah Storage</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Hapus Storage</a>
							</div>
							
							<div id="dlg" class="easyui-dialog" style="width:600px;height:auto;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<table id="dg2"></table>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-no" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Close</a>
							</div>
							
							<!-- TAMBAH STORAGE -->
							<div id="dlg10" class="easyui-dialog" style="width:400px;height:290px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons10">
								<div class="ftitle">Informasi Storage</div>
								<form id="fm10" method="post" novalidate>
									<div class="fitem">
										<label>Storage Type:</label>
										<select class="easyui-combobox" name="storage_type" id="id_coin_type" style="width:200px;">
											<option value="-">-</option>
											<option value="1">Warehouse</option>
											<option value="2">Rack</option>
											<option value="3">Box</option>
										</select>
									</div>
									
									<div id="area_warehouse">
										<div class="fitem">
											<label>Warehouse Name:</label>
											<input name="warehouse_name" class="easyui-textbox">
										</div>
										<div class="fitem">
											<label>Description:</label>
											<input name="warehouse_description" class="easyui-textbox">
										</div>
									</div>

									<div id="area_rack">
										<div class="fitem">
											<label>Choose Warehouse:</label>
											<select class="easyui-combobox" name="rack_warehouse" id="id_coin_type" style="width:200px;">
												<option value="-">-</option>
												<?php
												foreach($data_warehouse as $row){
													?>
													<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
													<?php
												}
												?>
											</select>
										</div>
										<div class="fitem">
											<label>Rack Name:</label>
											<input name="rack_name" class="easyui-textbox">
										</div>
										<div class="fitem">
											<label>Description:</label>
											<input name="rack_description" class="easyui-textbox">
										</div>
									</div>

									<div id="area_box">
										<div class="fitem">
											<label>Choose Warehouse:</label>
											<select class="easyui-combobox" name="box_warehouse" id="id_coin_type2" style="width:200px;">
												<option value="-">-</option>
												<?php
												foreach($data_warehouse as $row){
													?>
													<option value="<?=$row['id'];?>"><?=$row['value'];?></option>
													<?php
												}
												?>
											</select>
										</div>
										<div class="fitem">
											<label>Choose Rack:</label>
											<input id="id_coin_type22" class="easyui-combobox" name="box_rack"
											data-options="valueField:'id',textField:'value'">
										</div>
										<div class="fitem">
											<label>Box Name:</label>
											<input name="box_name" class="easyui-textbox">
										</div>
										<div class="fitem">
											<label>Description:</label>
											<input name="box_description" class="easyui-textbox">
										</div>
									</div>

								</form>
							</div>
							<div id="dlg-buttons10">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm10','dlg10')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg10').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
		var main_url		= '<?=base_url();?>refferensi/data_refferensi_warehouse';
		var detail_box_url	= '<?=base_url();?>aktivitas/data_master_temuan/box_id/';
		var sub_url;
		
		$("#area_warehouse").hide();
		$("#area_rack").hide();
		$("#area_box").hide();
		
		var conf = {
            options:{
                fitColumns:true,
				url: main_url,
				rownumbers: true,
                columns:[[
                    {field:'id',title:'ID',width:30},
                    {field:'value',title:'Nama Gudang',width:200},
                    {field:'description',title:'Deskripsi',width:200},
                    {field:'jumlah_rak',title:'Jumlah Rak',width:200}
                ]],
				onExpandRow: function(index,row){
					sub_url = '<?=base_url();?>refferensi/data_refferensi_rack/'+row.id;
				}
            },
            subgrid:{
                options:{
                    fitColumns:true,
					rownumbers: true,
					foreignField:function(prow){
						return {
							parent: prow.id
						}
					},
					url: '<?=base_url();?>refferensi/data_refferensi_rack/',
                    columns:[[
                        {field:'id',title:'ID',width:30},
                        {field:'value',title:'Nama Rak',width:200},
                        {field:'description',title:'Deskripsi',width:200},
                        {field:'jumlah_box',title:'Jumlah Box',width:200}
                    ]]
                },
                subgrid:{
                    options:{
                        fitColumns:true,
						rownumbers: true,
                        foreignField:function(prow){
							return {
								parent: prow.id
							}
						},
						url: '<?=base_url();?>refferensi/data_refferensi_box/',
                        columns:[[
                            {field:'id',title:'ID',width:30,
								formatter: function(value,row,index){
									return '<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="dataBox('+value+')">'+value+'</a>';
								}
							},
                            {field:'value',title:'Nama Box',width:200,align:'right'},
                            {field:'description',title:'Deskripsi',width:200},
                            {field:'jumlah_barang',title:'Jumlah Barang',width:200}
                        ]],
                        data:[
                            {price:923,quantity:2312,discount:0.2}
                        ]
                    }
                }
            }
        };
		
	
        $(function(){
			
			var dg = $('#dg').datagrid({
                title:'Master Data Storage',
                width:'100%',
                height:500
            }).datagrid('subgrid', conf);
			
			var dg2 = $('#dg2').datagrid({
                url: detail_box_url,
                pagination: true,
                remoteFilter: true,
                rownumbers: true,
				columns:[[
					{field:'barcode',title:'Barcode',width:100},
                    {field:'lifting_area_name',title:'Lifting Area',width:200},
                    {field:'category_name',title:'Category',width:200},
                    {field:'sub_category_name',title:'Sub Category',width:200},
                    {field:'material_name',title:'Material',width:200},
                    {field:'sub_material_name',title:'Sub Material',width:200}
				]]
            });
			
        });
		
		var url;
		var aksi;
		var pesan;
					
		function dataBox(box_id){
			$('#dg2').datagrid('load', detail_box_url+box_id);
			$('#dlg').dialog('open').dialog('setTitle','Detail Box');
		}
		
		function newUser(){
			$('#dlg10').dialog('open').dialog('setTitle','Tambah Storage');
			$('#fm').form('clear');
			url = '<?=base_url();?>storage/tambah_storage';
			aksi 	= 1;
		}
		
		$('#id_coin_type').combobox({
			onSelect: function(row){
				var target = this;
				setTimeout(function(){
					if(row.value==1){
						$("#area_warehouse").show();
						$("#area_rack").hide();
						$("#area_box").hide();
					}else if(row.value==2){
						$("#area_warehouse").hide();
						$("#area_rack").show();
						$("#area_box").hide();
					}else if(row.value==3){
						$("#area_warehouse").hide();
						$("#area_rack").hide();
						$("#area_box").show();
					}
				},0);
			}
		})
					
		
		$('#id_coin_type2').combobox({
			onSelect: function(row){
				if(row.value!='-'){
					var url = '<?=base_url();?>storage/get_rack/'+row.value;
					$('#id_coin_type22').combobox({url:url,valueField:'id',textField:'value'});
				}
			}
		})
		
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Edit Referensi');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>refferensi/ubah_refferensi/'+row.id;
				aksi 	= 2;
			}
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Storage";
						}else{
							pesan = "Berhasil Ubah Refferensi";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this refference?',function(r){
					if (r){
						$.post('<?=base_url();?>refferensi/hapus_refferensi',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Notifikasi'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>