<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storage extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("m_storage","refferensi");
        $this->load->model("aktivitas/m_aktivitas","aktivitas");
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$data['data_refferensi'] 	= $this->refferensi->get_parent();
			$data['data_warehouse'] 	= $this->aktivitas->getRefferensiByTag('storage_warehouse',1);
			$this->load->view('v_storage',$data);
		}else{
			redirect(base_url());
		}
	}
	
	function get_rack($warehouse=null){
		echo json_encode($this->aktivitas->getRefferensiByTag('storage_warehouse_rak',1,$warehouse));
	}
	
	function tambah_storage(){
		$user		= $this->session->userdata('user_id');
		
		$storage_type			= $this->input->post('storage_type');
		
		$warehouse_name			= $this->input->post('warehouse_name');
		$warehouse_description	= $this->input->post('warehouse_description');
		
		$rack_warehouse			= $this->input->post('rack_warehouse');
		$rack_name				= $this->input->post('rack_name');
		$rack_description		= $this->input->post('rack_description');
		
		$box_warehouse			= $this->input->post('rack_description');
		$box_rack				= $this->input->post('box_rack');
		$box_name				= $this->input->post('box_name');
		$box_description		= $this->input->post('box_description');
		
		$data = null;
		
		if($storage_type=='1'){
			$data = array(
				'tag_name'		=> 'storage_warehouse',
				'value'			=> $warehouse_name,
				'other'			=> '-',
				'description'	=> $warehouse_description,
				'user_created'	=> $user,
				'date_created'	=> date('Y-m-d H:m:s')
			);
		}else if($storage_type=='2'){
			$data = array(
				'tag_name'		=> 'storage_warehouse_rak',
				'value'			=> $rack_name,
				'other'			=> '-',
				'parent'		=> $rack_warehouse,
				'description'	=> $rack_description,
				'user_created'	=> $user,
				'date_created'	=> date('Y-m-d H:m:s')
			);
		}else{
			$data = array(
				'tag_name'		=> 'storage_warehouse_box',
				'value'			=> $box_name,
				'other'			=> '-',
				'parent'		=> $box_rack,
				'description'	=> $box_description,
				'user_created'	=> $user,
				'date_created'	=> date('Y-m-d H:m:s')
			);
		}
		
		$status = $this->refferensi->tambah_refferensi($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
		
	}
	
	function data_refferensi(){
		//Filtering
		$filterParam	= null;
		if($this->input->post('filterRules')){
			if($this->input->post('filterRules') != '[]'){
				$filter			= json_decode($this->input->post('filterRules'));
				$filter_count	= count($filter);
				if($filter_count>1){
					for($x=0;$x<$filter_count;$x++){
						if($x == $filter_count-1){
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%"';
						}else{
							$temp = $filter[$x]->field.' LIKE "%'.$filter[$x]->value.'%" AND ';
						}
						$filterParam .= $temp;
					}
				}else{
					$filterParam = $filter[0]->field.' LIKE "%'.$filter[0]->value.'%"';
				}
			}
		}
		
		
		$page 			= $this->input->post('page') ? intval($this->input->post('page')) : 1;
		$rows 			= $this->input->post('rows') ? intval($this->input->post('rows')) : 10;
		$dataRows 		= $this->refferensi->data_refferensi($page,$rows,$filterParam);
		$dataRowsAll 	= $this->refferensi->data_refferensi_all();
		$data["total"] 	= ''.count($dataRowsAll).'';
		$data["rows"] 	= $dataRows;
		
		// $data = $this->refferensi->data_refferensi();
		echo json_encode($data);
	}
	
	function get_parent(){
		$data = $this->refferensi->get_parent();
		echo json_encode($data);
	}
	
	function tambah_refferensi(){
		$tag_name 		= $this->input->post('tag_name');
		$description 	= $this->input->post('description');
		$value 			= $this->input->post('value');
		$other 			= $this->input->post('other');
		$parent 		= $this->input->post('parent');
		
		$data = array(
			'tag_name'			=> $tag_name,
			'description'		=> $description,
			'value'				=> $value,
			'other'				=> $other,
			'parent'			=> $parent,
		);
		
		$status = $this->refferensi->tambah_refferensi($data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function ubah_refferensi($id){
		$tag_name 		= $this->input->post('tag_name');
		$description 	= $this->input->post('description');
		$value 			= $this->input->post('value');
		$other 			= $this->input->post('other');
		$parent 		= $this->input->post('parent');
		
		$data = array(
			'tag_name'			=> $tag_name,
			'description'		=> $description,
			'value'				=> $value,
			'other'				=> $other,
			'parent'			=> $parent,
		);
		
		$status = $this->refferensi->ubah_refferensi($id,$data);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => $status,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
	function hapus_refferensi(){
		
		$id = $this->input->post('id');
		
		$status = $this->refferensi->hapus_refferensi($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
}
