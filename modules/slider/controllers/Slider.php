<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model("m_slider","slider");
    }
	
	public function index()
	{
		if($this->session->userdata('isLogin')){
			$this->load->view('v_slider');
		}else{
			redirect(base_url());
		}
	}
	
	function data_slider(){
		$data = $this->slider->data_slider();
		echo json_encode($data);
	}
	
	function tambah_slider(){
		
		$config['upload_path']          = './uploads/slider/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$this->load->library('upload', $config);
		
		$upload_date 	= $this->input->post('upload_date');
		$name 			= $this->input->post('name');
		$position 		= $this->input->post('position');
		
		$data = array(
			'upload_date'	=> $upload_date,
			'name'			=> $name,
			'position'		=> $position
		);
		
		
		if ( ! $this->upload->do_upload('userfile'))
		{
			echo json_encode(array('errorMsg'=>$this->upload->display_errors()));
		}
		else
		{
			$data_upload = $this->upload->data();
				
			$data = array(
				'upload_date'	=> $upload_date,
				'name'			=> $name,
				'position'		=> $position,
				'url'			=> $data_upload['file_name'],
				'user_created'	=> $this->session->userdata('user_id')
			);	
			
			$result = $this->slider->tambah_slider($data);	
			echo json_encode(array(
				'status' => 1,
				'messages' => $result
			));
		}
		
	}
	
	function ubah_slider($id){
		
		$config['upload_path']          = './uploads/slider/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$this->load->library('upload', $config);
		
		$upload_date 	= $this->input->post('upload_date');
		$name 			= $this->input->post('name');
		$position 		= $this->input->post('position');
		
		$data = array(
			'upload_date'	=> $upload_date,
			'name'			=> $name,
			'position'		=> $position
		);
		
		
		if ( ! $this->upload->do_upload('userfile'))
		{
			
			$data = array(
				'name'			=> $name,
				'position'		=> $position,
				'date_modified'	=> $upload_date,
				'user_modified'	=> $this->session->userdata('user_id')
			);	
			
			$result = $this->slider->ubah_slider($id,$data);	
			echo json_encode(array(
				'status' => 1,
				'messages' => $result
			));
		}
		else
		{
			$data_upload = $this->upload->data();
				
			$data = array(
				'name'			=> $name,
				'position'		=> $position,
				'url'			=> $data_upload['file_name'],
				'date_modified'	=> $upload_date,
				'user_modified'	=> $this->session->userdata('user_id')
			);	
			
			$result = $this->slider->ubah_slider($id,$data);	
			echo json_encode(array(
				'status' => 1,
				'messages' => $result
			));
		}
	}
	
	function hapus_slider(){
		
		$id = $this->input->post('id');
		
		$status = $this->slider->hapus_slider($id);
		
		if($status >= 0){
			echo json_encode(array(
				'status' => 1,
			));
		}else{
			echo json_encode(array('errorMsg'=>'Some errors occured.'));
		}
	}
	
}
