<?php
class M_slider extends CI_Model  {
	
	function data_slider(){
		$this->db->select('*');
		$data = $this->db->get('slider');
		return $data->result_array();
	}
	
	function tambah_slider($data){
		$this->db->insert('slider',$data);
	}
	
	function ubah_slider($id,$data){
		$this->db->where('id',$id);
		$this->db->update('slider',$data);
	}
	
	function hapus_slider($id){
		$this->db->where('id',$id);
		$this->db->delete('slider');
	}
	
	
}	
?>