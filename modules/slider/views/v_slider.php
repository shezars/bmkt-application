<?php
	$this->load->view('core/v_header');
?>
	<style type="text/css">
		#fm{
			margin:0;
			padding:10px 30px;
		}
		.ftitle{
			font-size:14px;
			font-weight:bold;
			padding:5px 0;
			margin-bottom:10px;
			border-bottom:1px solid #ccc;
		}
		.fitem{
			margin-bottom:5px;
		}
		.fitem label{
			display:inline-block;
			width:80px;
		}
		.fitem input{
			width:160px;
		}
	</style>
	<div data-options="region:'center',title:'Main Content'">
		<div class="easyui-tabs" style="width:100%;height:100%">
			<div title="Master Data Slider" data-options="plain:true,iconCls:'icon-speedometer'" style="padding:10px">
				<div class="row">
					<div class="span100persen">
						
						
							<table id="dg" title="MASTER DATA SLIDER" style="width:100%;padding:10px;width:100%;" toolbar="#toolbar" singleSelect="true" fitColumns="true" rownumbers="true">
								<thead>
									<tr>
										<th data-options="field:'name',width:80">Slider Name</th>
										<th data-options="field:'upload_date',width:80">Upload Date</th>
										<th data-options="field:'url',width:90">URL</th>
										<th data-options="field:'position',width:80">Position</th>
									</tr>
								</thead>
							</table>
							<div id="toolbar">
								<a href="javascript:void(0)" class="easyui-linkbutton newUser" iconCls="icon-add" plain="true" onclick="newUser()">New Slider</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit Slider</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Remove Slider</a>
							</div>
						
							<div id="dlg" class="easyui-dialog" style="width:400px;height:270px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons">
								<div class="ftitle">Slider Information</div>
								<form id="fm" method="post" novalidate enctype="multipart/form-data">
									<div class="fitem">
										<label>Upload Date:</label>
										<input id="idUploadDate" class="easyui-textbox">
										<input type="hidden" id="idUploadDateReal" name="upload_date" >
									</div>
									<div class="fitem">
										<label>Slider Name:</label>
										<input name="name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Position:</label>
										<input type="number" name="position" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>File:</label>
										<input class="easyui-filebox" name="userfile">
									</div>
								</form>
							</div>
							<div id="dlg-buttons">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm','dlg')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
							</div>
							
							<div id="dlg2" class="easyui-dialog" style="width:400px;height:330px;padding:10px 20px"
									closed="true" buttons="#dlg-buttons2">
								<div class="ftitle">Module Information</div>
								<form id="fm2" method="post" novalidate enctype="multipart/form-data">
									<div class="fitem">
										<label>Upload Date:</label>
										<input id="idUploadDateChange" class="easyui-textbox" disabled>
										<input type="hidden" id="idUploadDateReal" name="upload_date" >
									</div>
									<div class="fitem">
										<label>Slider Name:</label>
										<input name="name" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>Position:</label>
										<input type="number" name="position" class="easyui-textbox" required="true">
									</div>
									<div class="fitem">
										<label>File:</label>
										<input class="easyui-filebox" name="userfile">
									</div>
								</form>
							</div>
							<div id="dlg-buttons2">
								<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveUser('fm2','dlg2')" style="width:90px">Save</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg2').dialog('close')" style="width:90px">Cancel</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<style scoped>
        .f1{
            width:200px;
        }
    </style>
<?php
	$this->load->view('core/v_footer');
?>	

	<script type="text/javascript">
        $(function(){
            var dg = $('#dg').datagrid({
                url: '<?=base_url();?>slider/data_slider',
                pagination: true,
                remoteFilter: true,
                rownumbers: true
            });
            dg.datagrid('enableFilter');
        });
		
		var url;
		var aksi;
		var pesan;
		
		function newUser(){
			$('#dlg').dialog('open').dialog('setTitle','Slider Baru');
			$('#fm').form('clear');
			url = '<?=base_url();?>slider/tambah_slider';
			aksi 	= 1;
			
			$('#idUploadDate').textbox({
				value:'<?=date("Y-m-d");?>',
				disabled: true
			})
			
			
			
			$('#idUploadDateReal').val('<?=date("Y-m-d");?>');
			
		}
		function editUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg2').dialog('open').dialog('setTitle','Edit User');
				$('#fm2').form('load',row);
				url = '<?=base_url();?>slider/ubah_slider/'+row.id;
				aksi 	= 1;
			}
			
			$('#idUploadDateChange').textbox({
				value:'<?=date("Y-m-d");?>',
				disabled: true
			})
		}
		function saveUser(param,param2){
			$('#'+param).form('submit',{
				url: url,
				onSubmit: function(){
					return $(this).form('validate');
				},
				success: function(result){
					var result = eval('('+result+')');
					if (result.errorMsg){
						$.messager.show({
							title: 'Error',
							msg: result.errorMsg
						});
					} else {
						
						if(aksi==1){
							pesan = "Berhasil Tambah Slider";
						}else{
							pesan = "Berhasil Ubah Slider";
						}
						
						$.messager.show({	// show message
									title: 'Notifikasi',
									msg: pesan
								});
								
						$('#'+param2).dialog('close');		// close the dialog
						$('#dg').datagrid('reload');	// reload the user data
					}
				}
			});
		}
		function destroyUser(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirm','Are you sure you want to destroy this slider?',function(r){
					if (r){
						$.post('<?=base_url();?>slider/hapus_slider',{id:row.id},function(result){
							if (result.errorMsg){
								$.messager.show({	// show error message
									title: 'Error',
									msg: result.errorMsg
								});
							} else {
								$.messager.show({	// show error message
									title: 'Notifikasi',
									msg: 'Berhasil Hapus Slider'
								});
								$('#dg').datagrid('reload');	// reload the user data
							}
						},'json');
					}
				});
			}
		}
    </script>
</body>
</html>